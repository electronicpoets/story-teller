var keystone = require('keystone'),
      Story = keystone.list('Story'),
      Users = keystone.list('User');
      
    
var update = function(story, user, callback){
    if(!story) {
        return callback();
    }
      var spec = {};
      spec.title = story.title;
      spec.description = story.description;
      spec.objective = story.objective;
      spec.players = [];
      spec.storyteller = user._id;
      spec.createdAt = new Date();
      spec.createdBy = user._id;
      spec.description = story.description;
      spec.isPublished = true;
      spec.isFinished = true;
      spec.isOpen = true;
      // spec.pages = story.pages;

      var pages = story.pages;
      removeOthers(undefined, function(){
          
        new Story.model(spec).save(function(err, s){
             console.log("Created new story "+s.id);
             console.log("Updating indexes of "+pages.length+" pages");
             updatePages(pages, s, 0, function(){
                  story.pages = pages;
                  story.save(function(err, s){
                      if(err) {
                          console.log(err);
                      } else {
                          console.log("Saved new Stories array to story");
                          console.log(" -- "+s.pages.length+" pages");
                          done();
                      }

                  });
             });
        });
        
      })
}    

var updatePages = function(pages, story, index, callback){
    if(index<pages.length) {
        var p = pages[index];
        console.log("Updating page "+index++);
        p.story = story;
        p.title = ""+index;
       // p.save(function(err) {
            updatePages(pages, story,index, callback);
       // });
    } else {
        callback();
    }
};

var removeOthers = function(keep, callback){
    //var id = keep._id;
    //console.log("Removing stories other than "+keep._id);
    
    Story.model.find({title:"Streak Free"}).exec(function(err, stories) {
        for(var i=0;i<stories.length;i++)  {
            var remove = stories[i];
            console.log(" -> Removing "+remove.id);
            remove.remove();
        }
        //callback();
        done();
     });
    
};
    
exports = module.exports = function(done) {
    console.log("Searching for Joe...");




    Users.model.findOne({email:"joe@electronicpoets.co.uk"}).exec(function(err, user){
    //Users.model.findOne({email:"joe@electronicpoets.co.uk"}).exec(function(err, user){
        if(user) {
                console.log("Searching for Streak Free");
                console.log(user._id);
                
                var step = function(list){
                    var story = list.shift();
                    if(story){
                        fixer(story, function(){
                            step(list);
                        })
                    }else {
                        done();
                    }
                };
                
                var fixer = function(story, callback){
//                       console.log("Fixing story "+story.id);
//                       story.players=[];
//                       story.storyteller=[user._id];
//                       story.createdBy=user._id;
//                       story.isOpen=false;
                       story.save(function(err){
                            if(!err) {
                                console.log(story.id+" hotfixed!");
                                 callback();
                            } else {
                                console.log(err);
                            }
                       });
                };
                
                Story.model.findOne({id:"streak-free-1"}).populate("pages").exec(function(err, stories) {
                    update(stories, user, done);
//                    console.log(" Found "+stories.length+" matching stories");
//                    for(var i=0;i<stories.length;i++) {
//                       story = stories[i];
////                       story.players=[];
////                       story.storyteller=[user._id];
////                       story.createdBy=user._id;
////                       story.isOpen=false;
//                        console.log(story.id);
//                        console.log(story.players);
//                        console.log(story._doc.players);
//                    }
//                    step(stories);
                });
        }
        //throw {};
    });
};