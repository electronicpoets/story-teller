/**
 * This file contains the common middleware used by your routes.
 * 
 * Extend or replace these functions as your application requires.
 * 
 * This structure is not enforced, and just a starting point. If
 * you have more middleware you may want to group it as separate
 * modules in your project's /lib directory.
 */

var _ = require('underscore'),
    querystring = require('querystring'),
    keystone = require('keystone'),
    async  = require('async');


var isDebug=keystone.get("st mode")!="web";
//var isDebug=false;
//var log = GLOBAL.console.log;
//var console = {
//    log: function(){
//        if(isDebug) {
//            GLOBAL.console.log.apply(this, arguments);
//        }
//    }
//};
var winston = require("../utils/winstonst");

/**
 Initialises the standard view locals
 
 The included layout depends on the navLinks array to generate
 the navigation in the header, you may wish to change this array
 or replace it with your own templates / logic.
 */

exports.initLocals = function(req, res, next) {

    var locals = res.locals;

    locals.navLinks = [
        {label: 'Home', key: 'home', href: '/'},
        //{ label: 'Blog',		key: 'blog',		href: '/blog' },
        //{ label: 'Gallery',		key: 'gallery',		href: '/gallery' },
        {label: 'Stories', key: 'stories', href: '/stories'},
        {label: 'Contact', key: 'contact', href: '/contact'}
    ];

    locals.user = req.user;

    next();

};


/**
 Fetches and clears the flashMessages before a view is rendered
 */

exports.flashMessages = function(req, res, next) {

    var flashMessages = {
        info: req.flash('info'),
        success: req.flash('success'),
        warning: req.flash('warning'),
        error: req.flash('error')
    };

    res.locals.messages = _.any(flashMessages, function(msgs) {
        return msgs.length
    }) ? flashMessages : false;

    next();

};


/**
 Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {

    if (!req.user) {
        req.flash('error', 'Please sign in to access this page.');
        res.redirect('/keystone/signin');
    } else {
        next();
    }

}



/*
 * JC add logic for updating stories
 */
var pages = keystone.list('Page').model,
        stories = keystone.list('Story').model,
        content = keystone.list('Content').model,
        links = keystone.list('Link').model,
        util = require('../utils/db-util');

// Note: stoy must be a populated stories object...
exports.updateStory = function(storyId, deltas, callback) {
    winston.info("Middleware receiving deltas for " + storyId);
    //console.log(deltas);

    stories.findById(storyId).exec(function(err, story) {
        var changedIds = {};
        // for each piece of content, create it in the DB

        var pageIds = Object.keys(deltas);
        pageIds = pageIds.sort(function(a, b) {
            var aId = a[0];
            var bId = b[0];
            if (aId == "@" && bId == "@") {
                return -1;
            }
            else if (aId == "@") {
                return -1;
            } else {
                return 1;
            }
        })
        var pageCache = {};
        var stepOn = function() {
            async.nextTick(function(){
                var pageId = pageIds.shift();
                if (pageId) {
                    var delta = deltas[pageId];
                    if (pageId[0] == "@") {
                        // Create the page with a minimal spec, then populate its details later
                        util.createPage(story, {title:delta.title, status:delta.status}, changedIds, function(p) {
                            //console.log("Minimally created page "+pageId+" ("+p._id+")");
                            delta._id = changedIds[pageId] = p._id;
                            pageIds.push(p._id);
                            pageCache[p._id] = p; // optimisation
                            deltas[p._id] = delta;
                            stepOn();
                        });
                    } else {
                        if (pageCache[pageId]) {
                           // console.log("Beginning deferred population of "+pageId);
                            //console.log(delta);
                            exports.updatePage(changedIds, pageCache[pageId], delta, stepOn);
                           // stepOn();
                       } else {
                            pages.findById(pageId).exec(function(err, p) {
                                if (!err) {
                                    exports.updatePage(changedIds, p, delta, stepOn);
                                }
                                //stepOn();
                            });
                        }
                    }
                } else {
                    // when we're all done, return any newly assigned Ids
                    story.updateIds(changedIds);
                    story.onUpdated();
                    winston.info("Middleware finished updating deltas for " + storyId);
                    callback(changedIds);
                }
            });
        };
        stepOn();
    });
};

var getDb = function(type) {
    if (type == "link" || type == "next") {
        return  links;
    } else if (type == "content" || type == "contents") {
        return content;
    } else {
        return pages;
    }
}

var updateModel = function(type, deltas, callback) {
    var db = getDb(type);

    db.findById(deltas.id).exec(function(err, model) {
        if (model) {
            //console.log("Updating " + type + " " + model.id);
            //console.log(deltas)
            // The delta should only send changes, so we can just dumbly write like this
            for (var p in deltas) {
                if (p && p[0] != "_" && p != "id") {
                    //console.log("Updating property: " + p + " (" + deltas[p] + ")");
                    if (p == "size") {
                        if (!model.size) {
                            model.size = {};
                        }
                        // Surely I don't have to micromanage this much? Why won't my save work?
                        if (deltas[p].width) {
                            model.size.width = Math.floor(deltas[p].width);
                        }
                        if (deltas[p].height) {
                            model.size.height = Math.floor(deltas[p].height);
                        }
                    } else {
                        // convention for an auto-setter
                        var key = "set_"+p;
                        if(model[key]) {
                            model[key](deltas[p]);
                        }
                        else {
                            model[p] = deltas[p];
                        }
                    }
                }
            }
            model.save(function(err, m){
                if(err) {
                   winston.error(err);
                }
                if(callback) {
                    callback(m);
                }
            });
        } else if(callback) {
            winston.err(" ** Couldn't find model "+deltas.id);
            callback();
        }
    });
};


var addToQueue=function(queue, func, args){
    queue.push([args, func]);
};

var processQueue=function(queue, callback, count){
    count=count||0;
    //if(queue.length>0) {
    if(queue[count]) {
        async.nextTick(function(){
            winston.info("Processing queue item ("+(count+1)+" of "+queue.length+")");
            //var next = queue.shift();
            var next = queue[count++];
            var args = next[0];
            args.push(function(){
                processQueue(queue, callback, count);
            });
            next[1].apply(undefined, args);
        })
    } else {
        winston.info("** process queue finished! ***");
        callback();
    }
};

exports.updatePage = function(changedIds, page, deltas, callback) {
    // console.log("* Updating page " + page._id);
    // console.log(deltas);
    if (page) {
        var q=[];
        if (deltas.destroyed) {
            //exports.removeModel(story, "page", deltas);
            //page.destroy();
            
        }
        for (var prop in deltas) {
            var nested = deltas[prop];
            if (prop == "previous") {
                page.previous = changedIds[nested] ? changedIds[nested] : nested;
            }
            else if (nested && nested.concat && nested.splice) {
                for (var i = 0; i < nested.length; i++) {
                    var delta = nested[i];
                    if (delta.destroyed) {
                        exports.removeModel(page, prop, delta);
                    } else {
                        var cid = delta.id;
                        // if this id has alreayd been changed, use the new value now
                        if (changedIds[cid]) {
                            cid = delta.id = changedIds[cid];
                        }

                        if (prop == "contents") {
                            // synchronously add this to a processing queue, to be dealt with later
                            addToQueue(q, exports.updateContents, [changedIds, page, delta, true]);
                            //exports.updateContents(changedIds, page, delta, true);
                        } else if (prop == "next") {
                            addToQueue(q, exports.updateLink, [changedIds, page, delta, true]);
                            //exports.updateLink(changedIds, page, delta);
                        }
                    }
                }
            } else {
               // console.log(" -> page " + page._id + " updating " + prop);
                var key = "set_"+prop;
                if(page[key]) {
                    page[key](deltas[prop]);
                }
                else {
                    page[prop] = deltas[prop];
                }
            }
        }
        
        processQueue(q, function(){
            var oid=page.id;
            page.save(function(err, page, deltas){
                if(err) {
                    winston.error(err);
                }
                if(callback) {
                    callback(page);
                }
            });
        });
    
    }
};

// update a piece of content for a particular page
// return the id of the content (if changed);
exports.updateContents = function(changedIds, page, model, suppressSave, callback) {
    var id = model.id;
    if (!id || id[0] == "@") {
        var c = util.createContent(page, model, function(c){
            changedIds[id] = c._id;
            if(!suppressSave) {
                page.save(function(err, c){
                    if(err) {
                        winston.error("Error saving page: "+id, err);
                    }
                    if(callback) {
                        callback(c);
                    }
                });
            }
            else if(callback) {
                callback(c);
            }
        });
    } else {
        updateModel("content", model);
        callback();
    }
};

exports.updateLink = function(changedIds, page, model, suppressSave, callback) {
    var id = model.id;

    if (model.target && changedIds[model.target]) {
        //console.log("Updating link target id:");
        model.target = changedIds[model.target];
    }

    if (!id || id[0] == "@") {
        //var l = util.createLink(page, model.target, true, model.title, model.description);
        model.story=page.story;
        if(model.title) { // only save a new link if it has a title.
            var l = util.createLink(page, true, model, function(l){
                //winston.info("Created new Link " + l._id);
                changedIds[id] = l._id;
                if(suppressSave) {
                    if(callback) {
                        callback(l);
                    }
                } else {
                    page.save(function(err){
                        winston.error("Error saving link "+l._id, err, new Error());
                        if(callback) {
                            callback(l);
                        }
                    });
                }
            });
            return l._id;
        } else {
            callback();
        }
      } else {
        updateModel("link", model, callback);
    }
};

exports.removeModel = function(page, type, model) {
    // iterate over the page's content array
    var id = typeof model == "string" ? model : model.id;

    //console.log("Removing " + type + " " + id + " from page " + page._id);
    var db = getDb(type);
    db.findById(id).exec(function(err, content) {
        if (content) {
            //console.log("Found content!");
            content.remove();
            //page.save();
        } else {
            //console.log("Couldn't find content =/");
        }
    });
};
