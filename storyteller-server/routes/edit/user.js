/*
 * Create a new story instance for the requesting player
 */

var keystone = require('keystone'),
    async = require('async'),
    _ = require('underscore'),
    User = keystone.list('User');
    
    
    
 var updatePassword = function(user, delta, accept, reject){
     if(delta.pw1||delta.pw2) {
        //console.log(" -> Updating user password:")
        if(delta.pw1==delta.pw2) {
            var pw = delta.pw;
            // check the pw against the database
            //console.log(user.password);
             user._.password.compare(pw, function(err, result){
                if(result) {
                   //console.log("Writing password...");
                    user.password=delta.pw1;
                    //console.log(user);
                    accept(user);
                    
                } else {
                   reject("Error: Incorrect Password");
                }
            })
        } else {
            //console.log(" -> Passwords don't match!");
            reject("Error: passwords don't match!");
        }
    } else {
        accept(user);
    }
 };
 
 var updateUser = function(user, delta, accept, reject){
    //console.log("* Updating user settings: ");
    //console.log(delta);
    for(var p in delta) {
        var value = delta[p];
        if(p=="name") {
            var c = {};
            //console.log("writing '"+p+"'");
            for(var q in value) {
                //console.log(" - writing '"+q+"' as "+value[q]);
                c[q] = value[q];
                //console.log(c[q]);
            }
            // jiggery pokery to force the save, not impressed by this
            value = c; 
        } else if(p=="pw1"||p=="pw2"||p=="pw") {
             continue;
        }
        user[p]=value;
    }
    accept(user);
 }

exports = module.exports = function(req, res) {
    var user = req.user;
    var data = req.body.data;
    
    var onError=function(message){
        res.send(500, { message: message});
    };
    var onAccept=function(user){
        user.save(function(err, user){
            if(err) {
                onError(err);
            } else {
                //console.log("User settings saved!");
                res.send(200);                
            }
        })
    };
    
   // User.model.findById(user._id).exec(function(err, user ){
        updatePassword(user, data, function(user){
            updateUser(user,data, onAccept, onError);
        }, onError);
    //});
   
};
