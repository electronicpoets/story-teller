/* 
 * Script to handle all edits to a story.
 * 
 * A post to story/id will include a number of deltas,
 * containing pages (new & edited) and a number of 
 * content settings for each
 * 
 * Note: should we return new IDs for each page and content?
 *       its the DB that needs to assign an ID really, not the client...
 */

var keystone = require('keystone'),
	async = require('async'),
	util = require('../../utils/db-util');

// JC use KS API to read from the model
var stories = keystone.list('Story').model,
	pages = keystone.list('Page').model,
	content = keystone.list('Content').model;
	

// update a piece of content for a particular page
// return the id of the content (if changed);
var updateContents = function(page, model){
	var id = model.id;
	if(!id || id[0]=="@") {
		var c = util.createContent(page, model.type, model.content, model.postition, model.size);
		console.log("Created new Content "+content._id);
		console.log(c);
		page.save();
		return c._id;
	} else {
		content.findOne({ _id: model.id }).exec(function(err, content){
			console.log("Updating content "+content.id);
			// The delta should only send changes, so we can just dumbly write like this
			for(var p in model) {
				console.log("Reading "+p+"...");
				if (p && p[0]!="_" && p!="id") {
					//console.log("Updating property: "+p+" ("+model[p]+")");
					content[p]=model[p];
				}
			}
			content.save();
		});
		//return model.id;
	}
};

var removeContents = function(page, model){
	// iterate over the page's content array
	
	console.log("Removing content"+model._id+" from page "+page._id);
	content.findOne({ _id: model.id }).exec(function(err, content){
		content.remove();
		// will this also update the page... ?
	});
};

exports = module.exports = function(req, res) {



	var idMap = {};
	// first, identify the story that's been affected
	stories.findOne({ id: req.params.story}).populate("pages").exec(function(err, story){
		// now load the modified pages
		var deltas = req.body.data;
		console.log(deltas);
		
		var changedIds={};
		// for each piece of content, create it in the DB
		for(var pid in deltas) {
			var handler = function(err, page){
				var deltas = this.deltas.content;
				if(err) {
					// if there's no page, create it and re-invoke this handler
				} else {
					console.log("Found page "+page.id+", adding/updating "+deltas.length+" pieces of content");
					var d = deltas;
					while(d[0]) {
						var delta = d.shift();
						if (delta.destroyed) {
							removeContents(page, delta);
						} else {
							var cid = delta.id;
							var newId = updateContents(page, delta);
							if (newId) {
								changedIds[cid] = newId;
							}
						}
					}
				}
				// when we're all done, send any changed ids back to the client
				console.log(changedIds);
				res.set("content-type", "application/json");
				res.send(changedIds);
			}
			pages.findOne({ _id: pid }).exec(handler.bind({deltas: deltas[pid]}));
		}
		
	});

}
