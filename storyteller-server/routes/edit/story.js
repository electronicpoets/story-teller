/* 
 * Script to handle all edits to a story.
 * 
 * A post to story/id will include a number of deltas,
 * containing pages (new & edited) and a number of 
 * content settings for each
 * 
 * Note: should we return new IDs for each page and content?
 *       its the DB that needs to assign an ID really, not the client...
 */

var keystone = require('keystone'),
	async = require('async'),
	util = require('../../utils/db-util'),
	middleware = require('../middleware');

// JC use KS API to read from the model
var stories = keystone.list('Story').model;

exports = module.exports = function(req, res) {
	stories.findOne({ _id: req.params.id}).populate("pages").exec(function(err, story){
		//console.log("Recieved update request for: "+story.id);
		//console.log(req.body.data);
		middleware.updateStory(story, req.body.data, function(newIds){
			//console.log(newIds);
			res.set("content-type", "application/json");
			res.send(newIds);	
		});
	});

}
