var keystone = require('keystone'),
        async = require('async'),
        stories = keystone.list('Story').model,
        pages = keystone.list('Page').model,
        winston = require("../../utils/winstonst");

exports = module.exports = function(req, res) {
    res.set("content-type", "application/json");

    // Get one single story (by id)
    if (req.params.story) {
        winston.info("Getting story: '" + req.params.story + "'");

        stories.findOne({id: req.params.story}).populate("pages chat storyteller").exec(function(err, story) {
            // if the "all" flag is passed, we want to populate each page's content, too
            if (Object.prototype.hasOwnProperty.call(req.query, 'all')) {
                // JC this works - in that it populates the pages and returns them as "data".
                //    But it doesn't write the result back to the story object, so that story remains unpopulated.

                //// Can I populate multiple paths in one go?
                //pages.populate(story.pages, { path: "contents", select: "_id type content position size" }, function(err, data){
                //		//console.log(data);
                //		//res.send(story);
                //		pages.populate(story.pages, { path: "next"/*, select: "_id type content position size"*/ }, function(err, data){
                //			res.send(story);
                //		})
                //})

                // Can I populate multiple paths in one go? yes...
                var fieldsToPopulate = [{path: "contents", select: "_id type content position size style"},
                    {path: "next"}];
                winston.info("Populating pages of "+story.id);
                pages.populate(story.pages, fieldsToPopulate, function(err, data) {
                    winston.info("... story "+story.id+" populated!");    
                    res.send(story);
                });

            } else {
                res.send(story);
            }
        });
    }
    // Or, get all stories
    else {
        stories.count({}, function(err, count) {
            var result = {
                count: count,
                results: []
            };

            stories.find({}, undefined, undefined, function(err, data) {
                result.results = data;
                res.send(result);
            });

        });
    }
    //}
}
