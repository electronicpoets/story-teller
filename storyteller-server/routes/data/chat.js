var keystone = require('keystone'),
	async = require('async'),
        chat = keystone.list('Chat').model;

exports = module.exports = function(req, res) {
    res.set("content-type", "application/json");

    // Get one single Chat (by id)
    var id = req.params.id;
    if(id) {
        chat.findById(id).populate("messages").exec(function(err, data) {
            if(err) {
                console.log(err);
                res.send({});	
            }
            res.send(data);	
        });
    } else {
         res.send({});	
    }
}
