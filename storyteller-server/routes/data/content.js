var keystone = require('keystone'),
        async = require('async');

exports = module.exports = function(req, res) {

    // JC use KS API to read from the model
    // Note that I should be able to make this a generic handler... just need to parameterise the content type
    var contents = keystone.list('Content').model;

    // JC use the Express API to return data directly
    res.set("content-type", "application/json");

    // Get one single Page (by id)
    if (req.params.page) {
        contents.findOne({_id: req.params.id}).exec(function(err, data) {
            if (err) {
                console.log(err);
            }
            res.send(data);
        });
    }
    // Or, get all pages
    else {
        contents.count({}, function(err, count) {
            var result = {
                count: count,
                results: []
            };

            contents.find({}, undefined, undefined, function(err, data) {
                result.results = data;
                res.send(result);
            });

        });
    }

}
