var keystone = require('keystone');
var images = keystone.list('Image').model;

// just a temporary resource for loading background images
exports = module.exports = function(req, res) {

    images.find({isBackground: true}, 'name key ratio').exec(function(err, bgs) {
        //console.log("Found "+bgs.length+" background images!");
        //console.log(bgs);
        if (err) {
            console.log(err);
        }
        res.set("Content-Type", "application/json");
        res.send(bgs);
    });
};
