var keystone = require('keystone'),
        async = require('async'),
        http = require('http'),
        _ = require("underscore");

var request = require('request').defaults({ encoding: null });
var processImage = function(image, callback){
    async.nextTick(function(){
        // convert the image in base64
        if(!image.bin) {
            var url = image.picture.url;
            //console.log(":: requesting image data from "+url);
            request.get(url, function (error, response, body) {
                var img;
                if (!error && response.statusCode == 200) {
                    img = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
                    //console.log(data);
                }

                image.binaryCache = img;
                image.save();
                // todo: write the binary back to the database
                //console.log(" :- returning image data from "+url);
                callback({bin:img, url:image.picture.url});
            });

        } else {
            //console.log(":: Returning image");
            callback({bin:image.bin, url: image.picture.url});
            //console.log(":- image returned!");
        }
        
    });
}

exports = module.exports = function(req, res) {

    // JC use KS API to read from the model
    // Note that I should be able to make this a generic handler... just need to parameterise the content type
    var images = keystone.list('Image').model;

    // JC use the Express API to return data directly
    res.set("content-type", "application/json");

    // Get one single Page (by id)
    var id = req.params.id
    if (id) {
        images.findOne({key: id}).exec(function(err, image) {
//             res.send({url: image.picture.url});
            if(image) {
                processImage(image, function(result){
                     res.send(result);
                });
            } else {
                console.log("WARNING: could not load image "+id);
                console.log(err);
            }
        });
    }
    // Or, get all pages
    else {
        images.count({}, function(err, count) {
            var result = {
                count: count,
                results: []
            };

            images.find({}, "key name description creator ratio inserts").populate("creator", "name").exec(function(err, data) {
                // strip _id keys from the model.
//                for(var i=0;i<data.length;i++) {
//                    var x = data[i];
//                    if(x.creator) {
//                       x.creator = _.omit(x.creator, "_id");
//                    }
//                    data[i]=_.omit(x, "_id");
//                }
                result.results = data;
                res.send(result);
            });

        });
    }

}
