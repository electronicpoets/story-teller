/* 
 * Create an image
 * 
 * This must create a database entry, and trigger the creation of a corresponding
 * Cloudinary image
 * 
 */


var cloudinary = require('cloudinary'),
    keystone = require('keystone'),
    Image = keystone.list('Image').model;

/**
 * Upload Cloudinary Image
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
var uploadToCloudinary = function(file, callback){
    cloudinary.uploader.upload(file, function(result) { 
        if (result.error) {
            callback(result.error);
            console.log("Error sending file to Cloudinary:");
            console.log(result.error);
        } else {
            // NB -result has a URL property (and doubtless others)
           callback(result);
        }
    });
};

exports = module.exports = function(req, res) {
    var data = req.body;
        user = req.user,
        binary = data.imgData,
        title=data.title;

    if(user) {
        var createImage = function(){
            // create a new Imge object
            // include created timestamp, 
            // user name (as createdBy)
            // and other data
            console.log("Cloudinary Image created successfully!");

        };

        if(binary){
            uploadToCloudinary(binary, function(cloudData){
                console.log("Creating image...");

                var image = new Image({
                    name: data.title,
                    isBackground: data.isBackground,
                    description: data.description,
                    createdAt: new Date(),
                    creator: user,
                    picture: cloudData
                    // how do I add the cloudinary stuff?
                });

                image.save(function(err, img){
                   if(!err) {
                        res.send(200, {key: img.key, ratio:img.ratio});
                   } else {
                       console.log("Error saving image!");
                       console.log(err);
                   }
                });

            });
        } else {
            console.log("Error :: no file detected!");
        }
   }
};