/*
 * Create a new story instance for the requesting player
 */

var keystone = require('keystone'),
	async = require('async'),
    User = keystone.list('User');

exports = module.exports = function(req, res) {

  var mode = keystone.get("st mode");
  if(mode=="web") {
    exports.registerPublic(req, res);
  } else {
    exports.registerDemo(req, res);
  }
}

exports.registerDemo = function(req, res){
    var userData = req.body.data;
   // create the new user
   userData.email=userData.name.first+"@keystonejs.com"; // hack an e-mail address, might make life easier
   exports.createUser(userData, function(){
      res.send(200);
   }, function(err) {
        res.set("Content-Type", "aplication/json");
        res.send(500, { error: err });
   });
}

exports.createUser = function(data, onSuccess, onFail){
    // make sure the first name is unique...
    User.model.find({name: { user: data.name.user }}).exec(function(err, user) {
        if(user && user.length > 0) {
            if(onFail) {
                onFail("That username is already taken");
            }
        } else {
            if(!data.password) {
                data.password="storyteller";
            }
            if(!data.email) {
                data.email=data.name.first+"@keystone.js";
            }
            var user = new User.model(data);
            user.save(function(){
                onSuccess();            
            });
           }
    });
};

// New for 21Sept14 - if the codeword "STORYVERSE" is uploaded,
//                    override the mailchimp check. Give people opportunity to sign-up
exports.registerPublic = function(req, res){
   var userData = req.body.data;
   var email = userData.email;
   var name;
   if(userData.name) {
        name = userData.name.user||userData.name.first;
    }
    
    var reg=function(){
        exports.createUser({name: { first: name}, email: email}, function(){
            res.send(200);    
         }, function(error) {
             res.set('Content-Type', 'application/json');
             res.send(400, { error: error });
         });
    };
    
   // check that we don't have a user with that email address
   User.model.find({email:email}).exec(function(err, usr) {
       if(usr && usr.length>0) {
           
           console.log("Error :: user already signed up!");
           console.log(usr);
           res.set('Content-Type', 'application/json');
           res.send(400, { error: "That e-mail address is already registered" });
       } else {
            if(userData.code) {
                if(userData.code.toUpperCase()=="STORYVERSE") {
                    reg();
                } else {
                    res.set('Content-Type', 'application/json');
                    res.send(400, { error: "Sorry, that's not a valid sign-up code (if you're on the mailing list, you don't need a code!)" });
                }
            } else {
                // scraped from Mailchimp
                var alphaTestersListId = "c98779a924";

                // Take 2, with the MailChimp wrapper...
                var MailChimpAPI = require('mailchimp').MailChimpAPI;

                // TODO - load from startup settings
                var apiKey = keystone.get("mailchimp key");

                try { 
                    var api = new MailChimpAPI(apiKey, { version : '2.0' });

                    api.call("lists", "member-info", { id: alphaTestersListId, emails: [{email:email}]}, function (error, data) {
                     // console.log(data);
                      if (error) {
                         // console.log(error.message);
                      } else {
                        if(data.success_count==1) {
                           //console.log("User.js :: Registering user: "+name+" ("+email+")");
                            reg();
                        } else {
                           //console.log("User.js :: Failed to register user: "+email);
                           res.send(400, { error: "That doesn't appear to be a valid, pre-authorised e-mail address."});
                        }
                      }
                  });

                } catch (error) {
                    console.log(error.message);
                }
            }
       }
   });

}