/*
 * Create a new story instance for the requesting player
 */

var keystone = require('keystone'),
        async = require('async'),
        Story = keystone.list('Story'),
        utils = require('../../utils/db-util'),
        analytics = require('../../utils/analytics');

var isWeb=keystone.get("st mode")=="web";

exports = module.exports = function(req, res) {
    var user = req.user;
    var storyName = req.params.name;
    var story;

    var onComplete=function(story) {
        // sid, visitor, event, data
        //analytics.event(req.sessionID, , "story.created", { id: story.id, title: story.title, type: story.type});
        if(isWeb) {
            analytics.event(req.sessionID, "story", "created", story.id);
        }
        console.log("Created story "+story.id)
        user.tickStats("storyCreated");
        story.storyteller.push(user._id);
        story.save(function(err){
             if(err) {
                    console.log(err);
                }
            user.stories.push(story._id);
            user.save(function(err){
                if(err) {
                    console.log(err);
                }
                if(storyName) {
                    res.redirect("/story/" + story.id);
                }    else {
                    res.set("Content-Type", "application/json");
                    res.send(200, { path: "/story/" + story.id});
                } 
            });
        });
    };

    if (storyName) {
        // use the utils to create a new story instance of this name
        utils.createStory(storyName, onComplete);
    }
    else {
        // load the story json data
        var spec = req.body;
        spec.createdAt = new Date();
        // use it to create a new story instance      
        story = new Story.model(spec);
        utils.createPage(story, {shared: true}, {}, function(){
            onComplete(story);
        });
    }
};
