var keystone = require('keystone'),
        images = keystone.list('Image').model;

// remove an image from the database
// this is actually difficult to do without dependency tracking: how do we know its safe
// to remove an image?
// best we canreally do, for now, is flag it as removed so that we don't kill other stories
// Or we just delete it, and funk it...
exports = module.exports = function(req, res) {
    // Get one single story (by id)
    if (req.params.id) {
        images.findOne({key: req.params.id}).exec(function(err, img) {
            if (img) {
                img.remove();
            }
        });
    }
}
