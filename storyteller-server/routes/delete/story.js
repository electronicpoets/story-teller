var keystone = require('keystone'),
        stories = keystone.list('Story').model;

exports = module.exports = function(req, res) {
    // Get one single story (by id)
    if (req.params.story) {
        stories.findOne({id: req.params.story}).populate("players storyteller").exec(function(err, story) {
            if (story) {
                var id = story._id.toString();
                //console.log("Removing story " + story.title + " (" + id + ")");
                var players = story.players.concat(story.storyteller || []);
                for (var i = 0; i < players.length; i++) {
                    // load the user, remove the story from its stories array
                    var stories = players[i].stories;
                    for (var j = 0; j < stories.length; j++) {
                        if (stories[j].toString() == id) {
                           // console.log("Removing player " + players[i].name.first + " from story")
                            stories.splice(j, 1);
                            j--;
                        }
                    }
                    players[i].save();
                }
                story.remove();
            }
        });
    }
}
