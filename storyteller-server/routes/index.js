/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var _ = require('underscore'),
    keystone = require('keystone'),
    middleware = require('./middleware'),
    importRoutes = keystone.importer(__dirname),
    analytics = require('../utils/analytics');

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	front: importRoutes('./views/front'),	// front/home page UIs
	web: importRoutes('./views/web'),		// online version UIs
        guide: importRoutes('./views/guide'),
	data: importRoutes('./data'),
	edit: importRoutes('./edit'),
	create: importRoutes('./create'),
	delete: importRoutes('./delete')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Handle 403 errors (doesn't work)
	keystone.set('403', function(req, res) {
	    res.render('../../templates/views/errors/403.jade');
	});
	
	// little auth function to make sure that only authorised users can get data
	// if keystoneOnly is true, then only admin accounts get do the get
	var auth = function(keystoneOnly, authedOnly){
            // wrap the handler in a closure with the args provided
            return function(req, res, next){
                var user = req.user;
                if (!user 
                        || (keystoneOnly &&  !user.canAccessKeystone)
                        || (authedOnly && !user.isPreviewAuthed)) {
                    res.status(403);

                    // OK, so need to do this without explicitly leaning on the URL. The 403 page is pretty generic.
                    res.render('../../templates/views/errors/403.jade');
                    //res.render('403');
                } else {
                    // otherwise step on...
                    next();
                }
            }
	};
	
	// Views
	//app.get('/', routes.views.index);
	var mode = keystone.get("st mode");
        
        // custom wrapped page handlers, for analytics
        var wrap = function(){
            var path = arguments[0];
            var handler = arguments[arguments.length-1];
            var wrapper=function(req) {
                // extract the session id
                var sid=req.sessionID;
                analytics.page(sid, path);
                return handler.apply(this, arguments);
            };
            var args = [];
            for(var i=0;i<arguments.length-1;i++) {
                args.push(arguments[i]);
            }
            args.push(wrapper);
            return args;
        };
        
        var get = function(){
            var args = wrap.apply(app, arguments);
            app.get.apply(app, args);
        };
        
        var post = function(){
            var args = wrap.apply(app, arguments);
            app.post.apply(app, args);
        };
        
        var del = function(){
            var args = wrap.apply(app, arguments);
            app.delete.apply(app, args);
        };

	get('/open', auth(), routes.front.openstories);
	get('/pub', routes.front.pubstories);
	get('/create', auth(), routes.front.create);
	get('/about', routes.web.ep);
	get('/storyteller', routes.web.st);
	get('/streakfree', auth(false, true), routes.web.streakfree);

	// Conditional routing depending on the mode the server is in	
	if(mode=="web") {
            get('/alpha', auth(false, true), routes.web.alpha);
            get('/mystories', routes.front.mystories);
            get('/', routes.web.index);
            get('/register', routes.web.register);
            
            // the 'guide' section
            get('/features', auth(false, true), routes.guide.features);
            get('/howto', auth(false, true), routes.guide.instructions);
            get('/issues', auth(false, true), routes.guide.issues);
            get('/about_alpha', auth(false, true), routes.guide.about);
            get('/whatis',auth(false, true),  routes.guide.whatis);
            get('/signuphelp', routes.guide.help);
            
            post('/auth', routes.web.auth);
            post('/betasignup', routes.web.betasignup);
	} else {
            get('/', routes.front.mystories);
            get('/register', routes.views.register);
            //get('/user', routes.front.mystories);
            
            get('/blog/:category?', routes.views.blog);
            get('/blog/post/:post', routes.views.post);
            get('/gallery', routes.views.gallery);
	}
        get('/user', function(req, resp) {
            if(req.user) {
               routes.web.user(req, resp);
            } else {
                routes.views.login(req,resp);
            }
        });
	get('/login', routes.views.login);
	
	// JC map my own views
	get('/create/*', auth());
	get('/create/story/:name', routes.create.story);
	post('/create/story', routes.create.story);
	post('/create/story/:name', routes.create.story);
	post('/create/image',routes.create.image);
	post('/create/user', routes.create.user);
        post('/edit/user', routes.edit.user);
	
	get('/stories',  auth(), routes.views.stories);
	get('/story/:id', auth(), routes.views.story);
	post('/story/:id',  auth(),routes.edit.story);
	del('/story/:story',  auth(), routes.delete.story);
	
	// JC all data/ APIs MUST be an admin account, or no dice.
	//   (Just noticed the commented out line below which does EXACTLY this.)
	app.get('/data/stories*', auth(true));
	
	// Now mapp individual URLS. Must be a clever way to automate this...
	app.get('/data/stories', routes.data.story);
	app.get('/data/stories/:story', routes.data.story);
	
        app.get('/data/backgrounds', auth(false, true), routes.data.backgrounds);
	app.get('/data/pages*', auth(true));
	app.get('/data/pages', routes.data.page);
	app.get('/data/pages/:page', routes.data.page);
        app.get('/data/chat/:id', routes.data.chat);
        app.get('/data/messages/:id', routes.data.messages);
	
	// Note: data/images must be freely available. At least for the time being.
	app.get('/data/images', routes.data.image);
	app.get('/data/images/:id', routes.data.image);
        post('/image/:id',  auth(),routes.edit.image);
        del('/image/:id',  auth(), routes.delete.image);
        
	app.get('/data/content*', auth(true));
	app.get('/data/content', routes.data.content);
	app.get('/data/content/:id', routes.data.content);
	
	app.all('/contact', routes.views.contact);
	
	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
}
