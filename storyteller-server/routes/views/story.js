var keystone = require('keystone'),
	async = require('async');

var stories = keystone.list("Story").model,
    middleware = require('../middleware'),
    session = require("../../utils/storySession");

var winston = require('../../utils/winstonst');


// extract the list of players to 
var _extractPlayers = function(story, user, isStoryteller){

	var players = story.players;
	
	var sts = story.storyteller;
	var state = story.state||{};

	var playerList={};
	var userId = user._id.toString();

	// build a list of all the players in the game
        var storytellers = story.storyteller||[];
        var sids = {};
        for(var j=0;j<storytellers.length;j++) {
            sids[storytellers[j]._id.toString()] = storytellers[j];
        }
        var players = storytellers.concat(story.players);
	for (var i=0;i<players.length;i++) {
            //playerData = middleware.packagePlayer(player);
            
            var p = players[i];
            var pid = p._id.toString();

            //if(pid == userId || isStoryteller ) {
                var playerData = {
                    name: p.getName()
                };

                if (sids[pid]) {
                    playerData.isStoryteller = true;
                }
               
                // if there is state for this player, transfer to the data
                // TODO this sends al state to all players... we only really want to do this on spectate
                //         To be fair, I should know in advance when I'm loading a spectate URL 
                //if(pid == userId || isStoryteller ) {        
                    if(state[pid]) {
                        playerData.state=state[pid];
                    }
                //}
                
                // check whether the player is online
                // Does this mean we have to ask the session... ?
                // can I just call session.getPlayerList? Factor out this code?
                if(pid==userId || session.isPlayerOnline(story._id.toString(), pid)) {
                    playerData.isOnline=true;
                }
                
                if(pid==userId) {
                    playerData.isPrimary=true;
                    if(user.canAccessKeystone) {
                        playerData.isAdmin=true;
                    }
                }

                // add the player to the player list
                playerList[pid] = playerData;
            //}
	}
	
	if (!playerList[userId]) {
                if(players.length<story.maxPlayers) {
                    console.log(" ** Registerering "+user._id+" to story");
                    story.players.push(user._id);
                    story.save();
                    user.stories.push(story._id);
                    user.save();
                    playerList[userId] = {
                            name: user.getName(),
                            id: user._id,
                            isStoryteller: isStoryteller,
                            isPrimary: true
                    };

                    session.onPlayerJoinedStory(story._id, user._id);
                } else {
                    // flag the player as a spectator...
                     playerList[userId] = {
                            name: user.getName(),
                            id: user._id,
                            isSpectator: true,
                            isPrimary: true
                    };
                }
	}

	return playerList;

};

// check whether a player is allowed in a non-popullated story
function isAllowed(story, user) {
    var ids = [story.storyteller].concat(story.players);
    var id = user._id.toString();
    while(ids[0]) {
        if(ids.shift()==id) return true;
    }
    
    return story.isOpen || story.isPublished;
}

exports = module.exports = function(req, res) {
	
    var view = new keystone.View(req, res),
            locals = res.locals;

    locals.version=keystone.get("st version");

    // Init locals
    locals.data = {
        story: undefined,
        chat: undefined,
        isStoryteller: false
    };
	
    var user = req.user;
    //var userId = user._id.toString();
    //console.log(user);
    // Load a list of stories
    view.on('init', function(next) {
		
        var stories = keystone.list('Story').model;
        if (req.params.id) {
            //console.log("Getting story: '"+req.params.id+"'");
            stories.findOne({ id: req.params.id})./*populate("pages players storyteller").*/exec(function(err, story) {
                if(!story) {
                    console.log(err);
                    // return 404
                    res.send(404, "Error:  story couldn't be loaded");
                    return;
                } else if(!isAllowed(story, user)) {
                    winston.error(" User "+user.getName()+" Not authorised for story");
                    res.send(403, "Error: you do not have permission to see this story");
                    return;
                } else {
                    var populate="pages chat";
                    if(story.players && story.players.length>0) {
                        populate+=" players";
                    }
                    if(story.storyteller && story.storyteller.length>0) {
                        populate+=" storyteller";
                    }

                    stories.populate(story, populate, function(err, story) {
                        if(err) {
                            console.log(err);
                            next();
                            return;
                        }

                        if(story.chat) {
                            locals.data.chat = story.chat._id.toString();
                        }

                        var isStoryteller;
                        var sts = story.storyteller;
                        if(user && user._id) {
                            var uid = user._id.toString();
                            for (var i=0;i<sts.length;i++) {
                                var stid = sts[i];
                                if (stid._id.toString()==uid) {
                                    //console.log("* storyteller detected *");
                                    isStoryteller=true;
                                    break;
                                }
                            }
                        }
                        // short-term hack: allow keystone admins full storyteller access
                        if (/*user.canAccessKeystone ||*/ isStoryteller) {
                            isStoryteller = locals.data.isStoryteller=true;
                            //console.log("Prepping page for Storyteller")
                        }

                        //console.log("Found story: '"+story.id+"'!");
                        if (story.type=="LiveStory") {
                            locals.liveStory=true;

                            var ps = locals.data.players = _extractPlayers(story, user, isStoryteller);
                            for(var p in ps) {
                                if(ps[p].isSpectator) {
                                    winston.info("Prepping page for spectator...")
                                    locals.isSpectator=true; 
                                    break;
                                }
                            }
                        } else if(user) {
                            var uid = user._id.toString();
                            locals.data.players = {};
                            var pData = {
                                name: user.getName()
                            };
                            var st = story.storyteller ? story.storyteller[0] : {};
                            if(st._id == uid) {
                                console.log("Found Storyteller!");
                                pData.isStoryteller = true;
                            }
                            locals.data.players[uid]=pData;
                        }

                        var pages = keystone.list('Page').model;
                        var fieldsToPopulate = [{ path: "contents" },
                                        { path: "next" }];
                                        //{ path: "next", select: "_id target title subtitle freestyle prompt" }];
                        pages.populate(story.pages, fieldsToPopulate, function(){
                            locals.data.story=story;
                            next();
                        });
                    });
                }
            });
        } 
    });

    // Render the view
    view.render('story'); 
	
};
