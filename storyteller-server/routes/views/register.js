var keystone = require('keystone'),
    common = require('./front/common.js');

exports = module.exports = function(req, res) {
	
	var locals = common.initLocals(res);
		view = new keystone.View(req, res);
		
    common.setLinks(locals, req, common.keys.REGISTER);

    locals.csrf_token_key = keystone.security.csrf.TOKEN_KEY;
    locals.csrf_token_value = keystone.security.csrf.getToken(req, res);

    view.render('register');
}
