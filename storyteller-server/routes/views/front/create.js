var keystone = require('keystone'),
    common = require('./common.js');

exports = module.exports = function(req, res) {
	
	// if the user is already logged in, go to root?
	var locals = res.locals,
		view = new keystone.View(req, res);

	var locals = common.initLocals(res);
	
    if(keystone.get("st mode")=="web") {
        common.setLinks(locals, req, common.keys.ALPHA);
        common.setLinks(locals, req, common.keys.CREATE, "st");
    } else {
        common.setLinks(locals, req, common.keys.CREATE);
    }

	view.render('front/createstory');
	
}
