var keystone = require('keystone');

 var keys = {
	PUBLISHED: "Library",
	MYSTORIES: "My Stories",
	CREATE: "Create Story",
	OPEN: "Open Stories",
	ABOUTEP: "Who We Are",
	ABOUTST: "Storyteller",
	REGISTER: "Register",
	STREAKFREE: "Streak Free",
        ALPHA: "Alpha Testing",
	USER: "User",
        INDEX: "Index",
        GUIDE: "Guide",
        FEATURES: "Features",
        INSTRUCTIONS: "Instructions",
        ISSUES: "Issues",
        ABOUT: "About",
        WHAT: "What is Storyteller?"
  };

exports = module.exports = {

  keys: keys,

  setLinks: function(locals, req, current, isSecondary) {
        var isWeb = keystone.get("st mode")=="web",
            links = [],
            user = req.user,
            isAuthed = user && user.isPreviewAuthed;

        if(isWeb && !isSecondary) {
            links.push({
                    name: keys.ABOUTEP,
                    target: "/about",
                    active: current==keys.ABOUTEP
                });

            links.push({
                name: keys.ABOUTST,
                target: "/storyteller",
                active: current==keys.ABOUTST
            });		

            // todo - ensure the user's account is auth'd
            if(isAuthed) {
                links.push({
                    name: keys.ALPHA,
                    target: "/mystories",
                    active: current==keys.ALPHA
                });	
                
                 links.push({
                    name: keys.GUIDE,
                    target: "/howto",
                    active: current==keys.GUIDE
                });
                
//                links.push({
//                    name: keys.STREAKFREE,
//                    target: "/streakfree",
//                    active: current==keys.STREAKFREE
//                });				
            }
        } else {
            if(!isWeb || isSecondary=="st") {
                var user = req.user;
                if (user) {
                    links.push({
                        name: keys.MYSTORIES,
                        target: isSecondary?"/mystories":"/",
                        active: current==keys.MYSTORIES
                    });
                }

                links.push({
                    name: keys.CREATE,
                    target: "/create",
                    active: current==keys.CREATE
                });

                links.push({
                    name: keys.OPEN,
                    target: "/open",
                    active: current==keys.OPEN
                });

                 links.push({
                    name: keys.PUBLISHED,
                    target: "/pub",
                    active: current==keys.PUBLISHED
                });
            }
             else if(isSecondary=="guide") {
                links.push({
                    name: keys.ABOUT,
                    target: "/about_alpha",
                    active: current==keys.ABOUT
                });
                
                links.push({
                    name: keys.WHAT,
                    target: "/whatis",
                    active: current==keys.WHAT
                });
                
                links.push({
                    name: keys.INSTRUCTIONS,
                    target: "/howto",
                    active: current==keys.INSTRUCTIONS
                });

                links.push({
                    name: keys.FEATURES,
                    target: "/features",
                    active: current==keys.FEATURES
                });

                 links.push({
                    name: keys.ISSUES,
                    target: "/issues",
                    active: current==keys.ISSUES
                });
            }
        
        }

        if(isSecondary) {
            locals.sublinks=links;
        }else {
            locals.links=links;
        }
    },


    initLocals: function(res){
        var locals = res.locals;
        locals.version = keystone.get("st version");
        locals.mode = keystone.get("st mode");
        return locals;
      }
  
};