var keystone = require('keystone'),
        common = require('./common.js'),
        Stories = keystone.list('Story'),
        winston = require('../../../utils/winstonst');

exports = module.exports = function(req, res) {

// if the user is already logged in, go to root?
    var view = new keystone.View(req, res);

    var locals = common.initLocals(res);

    if (keystone.get("st mode") == "web") {
        common.setLinks(locals, req, common.keys.ALPHA);
        common.setLinks(locals, req, common.keys.PUBLISHED, "st");
    } else {
        common.setLinks(locals, req, common.keys.PUBLISHED);
    }
    
    locals.stories = [];

    Stories.model.find({isPublished: true}).populate("storyteller players").exec(function(err, results) {
//    Stories.model.find({isPublished: true}).exec(function(err, results) {
        if (results) {
            winston.info("Published stories: found " + results.length + " hits");
            for (var i = 0; i < results.length; i++) {
                var s = results[i];
                var sts = s.storyteller;
                for (var j = 0; j < sts.length; j++) {
                    var storyteller = sts[j];
                    // write the storyteller name to the locals
                    s.stName=storyteller.getName();
                }
                locals.stories.push(s);
            }
        }
        else if(err) {
            winston.error("views-publish", err);
        }
        view.render('front/pubstories');
    })
}
