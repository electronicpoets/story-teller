var keystone = require('keystone'),
        common = require('./common.js'),
        Stories = keystone.list('Story');

exports = module.exports = function(req, res) {

    // if the user is already logged in, go to root?
    var view = new keystone.View(req, res);

    var locals = common.initLocals(res);

     if(keystone.get("st mode")=="web") {
        common.setLinks(locals, req, common.keys.ALPHA);
        common.setLinks(locals, req, common.keys.OPEN, "st");
    } else {
        common.setLinks(locals, req, common.keys.OPEN);
    }

    locals.stories = [];

    // query for all stories which are "incomplete", "open" and "published" (because we can have private games)
    //Stories.model.find({isOpen: true, type: "LiveStory"/*, isPublished: true*/}).populate("storyteller").exec(function(err, results) {
    Stories.model.find({isOpen: true,type: "LiveStory"}).populate("storyteller players").exec(function(err, results) {
        if (results) {
            for (var i = 0; i < results.length; i++) {
                var s = results[i];
                var sts = s.storyteller;
                for (var j = 0; j < sts.length; j++) {
                    var storyteller = sts[j];
                    // write the storyteller name to the locals
                    s.stName=storyteller.getName();
                }
                if (s.isOpen) { // why can't I do this in the DB query?
                    locals.stories.push(s);
                }

            }
        }
        view.render('front/openstories');
    })



}