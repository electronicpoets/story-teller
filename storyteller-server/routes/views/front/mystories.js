var keystone = require('keystone'),
        common = require('./common.js');

exports = module.exports = function(req, res) {

    // if the user is already logged in, go to root?
    var view = new keystone.View(req, res),
            user = req.user;

    var locals = common.initLocals(res);
     if(keystone.get("st mode")=="web") {
        common.setLinks(locals, req, common.keys.ALPHA);
        common.setLinks(locals, req, common.keys.MYSTORIES, "st");
    } else {
        common.setLinks(locals, req, common.keys.MYSTORIES);
    }

    // query for all stories which this player is active in
    // show two sections: invites and actives
    var stories = locals.stories = {
        active: [],
        invited: [],
        user: user
    };

    if (user) {
        // get a list of the user's stories
        var userId = user._id.toString();
        user.populate("stories", function(err, user) {
            // how do I populate stories.storyteller?
            var activeStories = user.stories;

            // use a callback loop to fetch each story AND populate its storyteller field
            // TODO - consider using processNextTick for this
            (function step(index) {
                if (index >= activeStories.length) {
                    //console.info(stories.active.length + " stories detected for " + user.getName());
                    view.render('front/mystories');
                } else {
                    var story = activeStories[index];
                    // populate the storyteller for this story
                    story.populate("storyteller players", function(err, story) {
                        stories.active.push(story);
                        var sts = story.storyteller;
                        for (var i = 0; i < sts.length; i++) {
                            var storyteller = sts[i];
                            // write the storyteller name to the locals
                            story.stName=storyteller.getName();
                            //locals.stName=storyteller.name.first;
                            // and check whether this player is the storyteller
                            if (storyteller._id.toString() == userId) {
                                story.isStoryteller = true;
                                break;
                            }
                        }
                        step(++index);
                    })
                }
            })(0);
        });
    } else {
        //view.render('front/pubstories');
        res.redirect('/pub');
    }

}
