var keystone = require('keystone'),
	common = require('./front/common.js');

exports = module.exports = function(req, res) {
	
    // if the user is already logged in, go to root?
    var locals = common.initLocals(res);
            view = new keystone.View(req, res);
		
    common.setLinks(locals, req, common.keys.REGISTER);

    if (!req.user) {
            locals.csrf_token_key = keystone.security.csrf.TOKEN_KEY;
            locals.csrf_token_value = keystone.security.csrf.getToken(req, res);
            locals.mode=keystone.get("st mode");
            view.render('login');
    } else {
            res.redirect('/');
    }
};
