var keystone = require('keystone'), 
	common = require('../front/common.js');


exports = module.exports = function(req, res) {
	
	var locals = common.initLocals(res),
		view = new keystone.View(req, res);
		
    common.setLinks(locals, req, common.keys.GUIDE);
    common.setLinks(locals, req, common.keys.INSTRUCTIONS, "guide");
    view.render('guide/instructions');
}
