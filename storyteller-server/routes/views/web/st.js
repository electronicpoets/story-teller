var keystone = require('keystone'), 
	common = require('../front/common.js');


exports = module.exports = function(req, res) {
    var locals = common.initLocals(res),
        view = new keystone.View(req, res);

    locals.user = req.user;
    common.setLinks(locals, req, common.keys.ABOUTST);
    view.render('web/st');
}
