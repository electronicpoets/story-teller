var keystone = require('keystone'), 
	common = require('../front/common.js');


exports = module.exports = function(req, res) {
	
    var locals = common.initLocals(res),
            view = new keystone.View(req, res);
		
    common.setLinks(locals, req, common.keys.STREAKFREE);
    
    common.setLinks(locals, req, common.keys.STREAKFREE, "st");

    view.render('web/alpha');
}
