/*
 * Create a new story instance for the requesting player
 */

var keystone = require('keystone'),
	async = require('async');

exports = module.exports = function(req, res) {
    console.log("Signing up new user to mailchimp");
   var userData = req.body.data;
   var email = userData.email;
   
  // scraped from Mailchimp
  var betaTestersListId = "aadca1f99e";

  var MailChimpAPI = require('mailchimp').MailChimpAPI;
  var apiKey = keystone.get("mailchimp key");
  try { 
      var api = new MailChimpAPI(apiKey, { version : '2.0' });
      api.call("lists", "subscribe", { id: betaTestersListId, email: { email:email }}, function (error, data) {
          res.set('Content-Type', 'application/json');
          console.log(error);
          console.log(error.code);
          if (error) {
            // make sure to end the error message down to the client
             res.send(500, { code: error.code });
        } else {
            res.send(200);
        }
    });

  } catch (error) {
      console.log(error.message);
  }
};