
// Simple script to authorise a user's account
exports = module.exports = function(req, res) {
    var data = JSON.parse(req.body.data);
    if(data.code=="STREAK FREE") {
        console.log("Authorising user account");
        var user = req.user;
        user.isPreviewAuthed = true;
        user.save(); // don't save for the moment..
        res.send(200);
    } else {
        console.log("User auth failed ("+data.code+")");
        res.send(400);
    }

}
