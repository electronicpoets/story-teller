var keystone = require('keystone'),
	async = require('async');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// Init locals
	locals.section = 'story';
	locals.filters = {
		type: req.params.type
	};
	locals.data = {
		stories: []
	};
	
	// Load a list of stories
	view.on('init', function(next) {
		
		var q = keystone.list('Story').paginate({
				page: req.query.page || 1,
 				perPage: 10,
 				maxPages: 10
			})
			//.where('state', 'published')
			.sort('-publishedDate');
			//.populate('author categories');

		// keep this code: optionally filter story by type		
		//if (locals.data.type) {
		//	q.where('type').in([locals.data.type]);
		//}
		
		q.exec(function(err, results) {
			locals.data.stories = results;
			next(err);
		});
		
	});
	
	// Render the view
	view.render('stories');
	
}
