var keystone = require('keystone'),
	users = keystone.list("User").model,

exports = module.exports = function(req, res) {
	
	var locals = res.locals,
		view = new keystone.View(req, res);
	
	// Set locals
	locals.section = 'home';
	
	locals.links = [
		{
			name: "Published Stories",
			target: "/pub"
		}
	];
	
	var user = req.user;
	if (user) {
		locals.links.splice(0,0,{
			name: "My Stories",
			target: "/my"
		},{
			name: "Create Story",
			target: "/create"
		},{
			name: "Open Stories",
			target: "/open"
		});
	}
	
	view.render('index');
}


//exports = module.exports = function(req, res) {
//	
//	var locals = res.locals,
//		view = new keystone.View(req, res);
//	
//	// Set locals
//	locals.section = 'home';
//	
//	var user = req.user;
//	if (!user) {
//		view.render('index');
//	} else {
//		user.populate("stories", function(err, user) {
//			var stories = req.user.stories;
//	
//			var linkData = {
//				trolls: {
//					name: "Trolls!"
//				},
//				rabbit: {
//					name: "Sad Rabbit",
//				},
//			};
//			
//			for(var i=0;i<stories.length;i++) {
//				var story = stories[i];
//				if (story.title=="Trolls!") {
//					linkData.trolls.target="story/"+story.id;
//				}
//				else if (story.title=="Sad Rabbit") {
//					linkData.rabbit.target="story/"+story.id;
//				}
//			}	
//			
//			if (!linkData.trolls.target) {
//				linkData.trolls.name+="*";
//				linkData.trolls.target = "create/story/trolls";
//			}
//			
//			if (!linkData.rabbit.target) {
//				linkData.rabbit.name+="*";
//				linkData.rabbit.target = "create/story/rabbit";
//			}
//			
//			//locals.links = [
//			//	linkData.trolls, linkData.rabbit
//			//];
//			
//			locals.links = [
//				{
//					name: "My Stories",
//					target: "/"
//				},{
//					name: "Create Story",
//					target: "/#create"
//				},{
//					name: "Open Stories",
//					target: "/#open"
//				}
//			]
//			
//			if (user.canAccessKeystone) {
//				locals.players=[];
//				
//				var process=function(players){
//					//console.log("Found "+players.length+" players with active stories");
//					locals.players=players;
//					view.render('index');
//				};
//				
//				// get a list of all the players with active games
//				// Note that we have to do it the hard way, for now
//				users.find({}).populate("stories").exec(function(err, results){
//					var x = [];
//					for (var i=0;i<results.length;i++) {
//						var p = results[i];
//						//console.log(p);
//						if (p.stories && p.stories.length>0) {
//							x.push(p);
//						}
//					}
//					process(x);
//				});
//			} else {
//				// Render the view
//				view.render('index');
//			}
//		});
//	}
//}
