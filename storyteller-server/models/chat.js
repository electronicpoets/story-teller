var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Chat = new keystone.List('Chat', {

});

Chat.add({
    // ordered list of messages
    messages: {  type: Types.Relationship, ref: 'Message', many: true },
    story: { type: Types.Relationship, ref: 'Story', index: true }
});

Chat.schema.methods.addMessage=function(messageData) {
    console.log(" -> Chat adding messages '"+messageData.content+"'");
    var m = new Message.model(messageData);
//    if(!this.messages){
//        this.messages = [];
//    }
    this.messages.push(m._id);
    m.save();
    this.save();
};

var Message = new keystone.List('Message', {

});

// story an index, as an integer?
Message.add({
    content: { type: String },
    timestamp: { type: Date },
    sequence: { type: Number },
    author: { type: Types.Relationship, ref: 'User' },
    chat: { type: Types.Relationship, ref: 'Chat', index: true }
});


Message.register();
Chat.register();
