var _ = require('underscore'),
	keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * Users
 * =====
 */

var User = new keystone.List('User');

User.add({
	name: { type: Types.Name, initial: true, required: true, index: true },
        moniker: {type:String, index: true},
	email: { type: Types.Email, initial: true, required: true, index: true },
	password: { type: Types.Password, initial: true, required: false },
	stories: { type: Types.Relationship, ref: "Story", many: true }
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone' },
	isPreviewAuthed: { type: Boolean, label: 'Is Authorised for Preview' }
});

User.schema.add({
    stats: {type: keystone.mongoose.Schema.Types.Mixed}
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});

// increment some stats
User.schema.methods.tickStats = function(statName) {
    var stats = this.stats;
    if(!stats) {
        stats = this.stats = {};
    }
    if(isNaN(stats[statName])) {
        stats[statName]=0;
    }
    stats[statName]++;
    this.stats=null;
    this.stats = stats;
    this.save();
    // publish an event?
};

User.schema.methods.resetStats = function(statName) {
    this.stats={};
    this.save();
};

User.schema.methods.getName = function(full) {
    var name = this.name;
    if(full) {
        return name.first+(name.last?" "+name.last:undefined);
    } else {
        return this.moniker||name.first;
    }
}


/**
 * Relationships
 */

User.relationship({ ref: 'Post', path: 'author' });
User.relationship({ ref: 'Story', path: 'player' });
User.relationship({ ref: 'Story', path: 'storyteller' });


/**
 * Registration
 */

User.defaultColumns = 'name, email, isAdmin, stories';
User.register();
