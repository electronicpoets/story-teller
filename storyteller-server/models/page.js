var keystone = require('keystone'),
        //mongoose = require('mongoose'),
        Types = keystone.Field.Types;



var Page = new keystone.List('Page', {
    map: {name: 'title'},
    autokey: {path: 'id', from: 'title', unique: true}
});


// Create content as a sub-type of page, for now. That "links" the two so that content is saved when a page is saved
var Content = new keystone.List('Content', {
});

Content.add({
    page: {type: Types.Relationship, ref: "Page"},
    type: {type: String},
    content: {type: String}, // this might be a String OR a Picture (indexed by key)
    style: {type: String},
    // object with w/h in %
    size: {
        width: {type: Number},
        height: {type: Number}
    },
    textSize: { type: String }

});

Content.schema.add({
    position: {
        horizontal: {type: keystone.mongoose.Schema.Types.Mixed},
        vertical: {type: keystone.mongoose.Schema.Types.Mixed}
    }
});

// Register the sub-type. Will this work with the KS wrappers?
if (Page.schema.children) {
    Page.schema.children.push(Content);
} else {
    Page.schema.children = [Content];
}

// A Link is also a sub-type of a Page.
var Link = new keystone.List('Link', {
});

Link.add({
    target: {type: Types.Relationship, ref: "Page"},
    // Rename to Label and Description?
    story: {type: Types.Relationship, ref: "Story"},
    title: {type: String},
    subtitle: {type: String},
    freestyle: {type: Boolean},
    prompt: {type: String},
    isValid: {type:Boolean, default: false}
    //input: { type: String }
})

Link.schema.add({
    input: {
        type: keystone.mongoose.Schema.Types.Mixed
    }
});

var SessionManager;
Link.schema.methods.set_status = function(isValid){
    if(isValid!=this.isValid) {
        this.isValid=isValid;
        if(isValid) {
            // issue notificaiton that this link is now valid.
            // May update connected clients or send e-mails
           console.log("** link "+this.title+"("+this.id+") is now valid!");
           if(!SessionManager) {
                SessionManager = require('../utils/storySession');
           }
            SessionManager.validateLink(this.story, this.id);
        }
        this.save();
    }
}
        
Link.schema.methods.set_target = function(targetPage, /*TODO*/ targetPlayer, callback){
    console.log("Link "+this.title+" ("+this._id+") setting target to "+targetPage);
    if(targetPage) {
        var handler=function(err, page){
            console.log(" -- Link "+this.title+" ("+this._id+") setting target page to: "+(page.title?page.title:page) );
            //console.log(page);
            this.target = page ? (page._id||page) : null; // ??
            if(page && page.status=="shared") {
                this.set_status(true);
            } else {
                this.set_status();
            }
            this.save(callback);
        }.bind(this);

        if(targetPage._id) {
            handler(null, targetPage);
        } else {
            Page.model.findById(targetPage).exec(handler);
        } 
    } else {
        this.target = null;
        this.set_status();
        if(callback) {
            callback();
        }
    }
};

Link.schema.methods.setFreestyle = function(playerId, input) {
    var data = this.input;
    if (!data) {
        data = {};
    }

    data[playerId] = input;
    //console.log("New link state:")
    //console.log(data);
    this.input = data;
    this.save();
};

Page.add({
    story: {type: Types.Relationship, ref: "Story"},
    title: {type: String},
    number: {type: Number},
    type: {type: String}, // requires enumerating
    status: {type: Types.Select, options: 'shared, unshared', default: 'unshared', index: true},
    // an image really, saved a String
    background: {type: String},
    fadeBg: {type: Boolean},
    // contains CONTENT type objects	
    contents: {type: Types.Relationship, ref: "Content", many: true},
    // valid next/previous links. Arrays of type LINK
    next: {type: Types.Relationship, ref: "Link", many: true},
    previous: {type: Types.Relationship, ref: "Link", many: true},
});


Page.schema.add({
    responses: {
        type: keystone.mongoose.Schema.Types.Mixed
    }
});

// when the status changes, we may need to enable any connected links
Page.schema.methods.set_status = function(status) {
    //var status = isComplete?"shared":"unshared";
    console.log("Page "+this.title+" set status: "+status);
    if(this.status!=status) {
        this.status=status;
        if(status=="shared") {
            console.log(" Page "+this.title+" updated: notifying links...");
            Link.model.find({target:this._id}, function(err, links) {
                for(var i=0; i<links.length;i++) {
                    var link = links[i];
                    link.set_status(true);
                }
            });
        }
    }
};

Page.schema.methods.setStory = function(story, onComplete) {
    var pages = story.pages;
    this.number = pages.length + 1;
    if (!this.title) {
        this.title = "Page " + this.number;
    }
    this.story=story;
    var id=this._id;
    this.save(function() {
        pages.push(id);
        story.save(onComplete);
    });
};

Page.schema.methods.saveResponse = function(playerId, linkId, message, timestamp, onComplete) {
    if(!this.responses) {
        this.responses={};
    }
    this.responses[playerId]={
        target: linkId,
        timestamp: timestamp||new Date().toString()
    };
    if(message) {
        this.responses[playerId].message=message;
    }
    this.save(onComplete);
};

Content.register();
Link.register();
Page.register();
