var keystone = require('keystone'),
        Types = keystone.Field.Types;

var Image = new keystone.List('Image', {
    autokey: {from: 'name', path: 'key'}
});

/*
 * TODO - add things like tags
 */
Image.add({
    name: {type: String, required: true, initial: true},
    creator: {type: Types.Relationship, ref: 'User', required: true, initial: true},
    createdAt: {type: Types.Date},
    picture: {type: Types.CloudinaryImage},
    description: {type: String},
    binaryCache: {type: String}, // note: if I keep this, when do I clear it... ? On edit, I guess!
    isBackground: {type: Boolean}, // tmp!
    group: {type: String}, // group images into related sets. An image might have muliple gorups
    palettes: {type: Types.Relationship, ref: 'Palette', many: true},
    inserts: {type: Number, default: 0},
    // tags
    // aspect ratio - compute this on upload
    ratio: {
        type: String,
        //watch: "picture",
        watch: true,
        value: function() {
            // load the height/width
            // calculate aspect ratio w:h
            // save to string
            var pic = this.picture,
                    w = pic.width,
                    h = pic.height;

            var ratio;
            if (w > h) {
                var percent = Math.ceil((w / h) * 100);
                ratio = (percent / 100) + ":1";
            } else {
                var percent = Math.ceil((h / w) * 100)
                ratio = "1:" + (percent / 100);
            }
            return ratio;
        }
    }
});

// increment stats
Image.schema.methods.onInsert = function() {
    this.inserts++;
    this.save();
    // publish an event?
};

Image.register();
