var keystone = require('keystone'),
        Types = keystone.Field.Types,
        Chat = keystone.list("Chat").model;

var Story = new keystone.List('Story', {
    map: {name: 'title'},
    autokey: {path: 'id', from: 'title', unique: true}
//    ,track: {
//        createdAt: true
//    }
});


Story.add({
    title: {type: String, required: true},
    description: {type: String},
    objective: {type: String},
    //playerCount: { type: String },
    type: {type: Types.Select, options: 'Story, LiveStory'},
    contentType: {type: Types.Select, options: 'Picture, Prose'},
    players: {type: Types.Relationship, ref: 'User', many: true},
    // --
    storyteller: {type: Types.Relationship, ref: 'User', many: true},
    createdBy: {type: Types.Relationship, ref: 'User', index: true},
    createdAt: {type: Types.Date},
    //createdDate: {type: Types.Date, index: true},
    updatedAt: {type: Types.Date },
    pages: {type: Types.Relationship, ref: 'Page', many: true},
    chat: {type: Types.Relationship, ref: 'Chat'},
    // a published story can be spectacted by anon users, and will be seen in the public browser
    isPublished: {type: Boolean},
    // a completed story cannot accept new players or pages. May accept edits.
    isComplete: {
        type: Boolean
    },
    // an open story accepts new players. A story is auto-closed when the player limit is reached
    isOpen: {
        type: Boolean,
        default: true,
        watch: "players storyteller", // when isComplete is set, update isOpen
        value: function() {
            var totalPlayers = this.storyteller.length + this.players.length;
            if (this.isComplete || totalPlayers >= this.maxPlayers) {
                if (this.isOpen) {
                    return false;
                }
            } else if (!this.isOpen) {
                //this.isOpen = true;
                return true;
            }
        }
    },
    maxPlayers: {
        type: Number,
        //watch: true,
        //value: function() {
        //	// if player count is equal to max players,
        //	// close the story
        //	var totalPlayers = this.storyteller.length + this.players.length;
        //	if (totalPlayers > this.maxPlayers) {
        //		console.log("Closing story "+this.id+" as max player limit is reached")
        //		this.isOpen = false;
        //		this.save();
        //	} else if(!this.isOpen) {
        //		this.isOpen = true;
        //		this.save();
        //	}
        //}
    },
    template: {type: Types.Relationship, ref: 'Story'}
});

Story.schema.add({
    state: {
        type: keystone.mongoose.Schema.Types.Mixed
    },
    listeners: {
        type: keystone.mongoose.Schema.Types.Mixed
    }
});

// have a getState function which pulls out a player's state object
Story.schema.methods.getState = function(playerId, pageId) {
    var state = this.state;
    if (!state) {
        state = this.state = {};
    }

    var player = state[playerId];
    if (!player) {
        player = state[playerId] = {};
    }

    if (pageId) {
        var page = player[pageId];
        if (!page) {
            page = player[pageId] = {};
        }
        return page;
    }
    else {
        return player;
    }
};

// and the inverse, let us set some state
Story.schema.methods.setState = function(playerId, pageId, property, value) {
    var state = this.getState(playerId, pageId);
    //console.log("> Story :: setting state "+property+" to "+value);
    state[property] = value;
    //this.save();
    this.updateState(this.state);
};

Story.schema.methods.getCurrentPage = function(playerId) {
    var state = this.getState(playerId);
    return state._current;
};

Story.schema.methods.setCurrentPage = function(playerId, pageId) {
    if (this.type == "LiveStory") {
        var state = this.getState(playerId);
        //console.log("> Story :: setting current page for "+playerId+" to "+pageId);
        state._current = pageId;
        this.updateState(this.state);
    }
};

Story.schema.methods.updateState = function(newState) {
    this.state = undefined;
    //this.save();
    this.state = newState;
    //this.onUpdated();
    this.save();
};

// TODO - make this a little more efficient...
Story.schema.methods.onUpdated = function(callback) {
    this.updatedAt = new Date();
    var cb;
    if(callback) {
        cb = function(err, self){
          callback(self);  
        };
    }
    this.save(cb);
};

// Messy: manually update any state references to changaed ids
Story.schema.methods.updateIds = function(newIds) {
    var changed;
    var st = this.state;
    for (var pid in this.state) {
        var playerState = this.state[pid];
        // first, update the current page id
        var c = playerState._current;
        //console.log("Updating player ids ("+this.name+")");
       // console.log(c);
        if (newIds[c]) {
            playerState._current = newIds[c];
            changed = true;
        }

        // then check each page id
        for (var pageId in playerState) {
            var pageState = playerState[pageId];
            if (pageId !== "_current") {
                // and for each page, any prev/next references
                var next = pageState.next;
                if (next && newIds[next]) {
                    pageState.next = newIds[next];
                    changed = true;
                }

                var prev = pageState.previous;
                if (prev && newIds[prev]) {
                    pageState.previous = newIds[prev];
                    changed = true;
                }
            }
        }
    }

    if (changed) {
        this.updateState(this.state);
    }

}

Story.schema.methods.addChatMessage = function(messageData) {
    var chat = this.chat;
    if (!chat) {
        chat = this.chat = new Chat({story: this._id});
        this.save();
    }

    if (!chat.addMessage) {
        this.populate("chat", function(err, self) {
            if (err) {
                console.log(err);
            } else {
                self.chat.addMessage(messageData);
            }
        });
    } else {
        chat.addMessage(messageData);
    }
}

Story.defaultColumns = 'title, storyteller, players, createdBy';
Story.register();