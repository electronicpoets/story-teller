var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Palette = new keystone.List('Palette', {
	autokey: { from: 'name', path: 'key' }
});

/*
 * TODO - add things like tags
 */
Palette.add({
	name: { type: String, required: true },
	drawers: {  type: Types.Relationship, ref: 'Drawer', many: true },
	players:  {  type: Types.Relationship, ref: 'Player', many: true }
});


var Drawer = new keystone.List('Drawer', {
	autokey: { from: 'name', path: 'key' }
});

Drawer.add({
	name: { type: String, required: true },
	images:  {  type: Types.Relationship, ref: 'Image', many: true }
});

Drawer.register();
Palette.register();
