window.onKeyPress=function(evt){
    var code = evt.key || evt.keyIdentifier;
    if(code=="Enter") {
        auth();
    }
};

window.togglePwForm=function(){
    $('#pwForm').toggleClass("show");
}

window.saveDetails=function(){
    var delta = {};
    
    var inputs = $('[data-delta]');
    for(var i=0;i<inputs.length;i++) {
        var input  = inputs[i];
        var orig = input.getAttribute("data-original");
        var value = input.value;
        if(value.length > 0 && orig !== value) {
            input.setAttribute("data-orig", value);
            var prop = input.getAttribute("data-delta").split(".");
            var d = delta;
            while(prop.length>0) {
                var p = prop.shift();
                if(prop.length>0) {
                    d = d[p] = d[p]||{};
                }else {
                    d[p] = value;
                }
            }
        }
    }
    
    if(Object.keys(delta).length>0) {
        
        // now check the password form...        
        if(delta.pw1 || delta.pw2) {
            if(delta.pw1 !== delta.pw2) {
                alert("passwords do not match!");
                return;
            }
        }
        
        var saveButton = document.getElementById("saveButton");
        $(saveButton).addClass("loading");
        // send up to the server
        $.post("/edit/user", {
            data: delta,
            dataType: "application/json"
        }).done(function(data){
                $("#saveMessage").removeClass("show");        
                $(saveButton).removeClass("loading");
                saveButton.textContent="Details Updated!";
                setTimeout(function() {
                    saveButton.textContent="Save";    
                }, 5000);
            }).fail(function(res){
                $("#saveMessage").addClass("show");
                $("#saveMessage").text("Your password is incorrect. Your settings have not been updated.");
            });
    }
}