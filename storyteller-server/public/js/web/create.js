
window.submitStory = function(notifyNode){
  var story = {};
  var errors = [];
  
  var name = document.getElementById("inputName");
  if (name.value) {
    story.title = name.value;
  } else {
    errors.push(["The Story has no name", name]);
  }
  
  var desc = document.getElementById("inputDesc");
  if (desc.value) {
    story.description = desc.value;
  }
  
  var type = document.getElementById("inputType");
  story.type = type.checked ? "LiveStory" : "Story";
  
  var content = document.getElementById("inputContent");
  if (content.value) {
    story.contentType = content.value;
  }
  
//  var players = document.getElementById("inputPlayer");
//  if (players && players.value) {
//    story.maxPlayers = parseInt(players.value);
//  }else {
//      story.maxPlayers=2;
//  }
    if(type.checked) {
        story.maxPlayers=2;
    } else {
        story.maxPlayers=1;
    }
  
  // if we have everything we need, submit the request to /create
  if (errors.length===0) {
    if(notifyNode) {
      notifyNode.className+=" loading";
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/create/story");
    xhr.onload = function(data) {
       var js = JSON.parse(this.responseText);
       window.location=js.path;
    };
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(story));
  } else {
    // otherwise, report the errors
    reportErrors(errors);
  }


};

window.reportErrors = function(errors){
  
};

//function auth(evt){
//  var input = $("#authCode")[0];
//  var code = input.value;
//  input.className+=" loading";
//  var x = JSON.stringify({'code': code});
//  console.log(x);
//  $.post("/auth", {
//    data: x,
//    dataType: "application/json"
//  }).done(function(data, status, xhr){
//      // print a thank you and re-fresh the nav options
//      if(xhr.status==200) {
//        window.location=window.location;
//      }
//    }).fail(function(){
//      // print an error: the code was invalid
//      var messageNode=$("#messageNode");
//      messageNode.addClass("visible");
//    });
//};