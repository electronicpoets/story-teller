function login(){
  window.location="/login";
}
function doCommand(evt, name){
    var code = evt.key || evt.keyIdentifier;
    if(code=="Enter") {
        if(window[name]) {
            window[name]();
        }
    }
};

// TODO - tidy this signature. Maybe take a credentials argument
function doSimpleLogin(userName, notifyNode, email, redirect) {
  var emailNode = document.getElementById("email");
  var userNode = document.getElementById("userName");
  var passwordNode = document.getElementById("pw");
  var pw;
  if(passwordNode) {
      pw =  passwordNode.value
  } else {
      pw = "storyteller";
  }
  if(userNode) {
    userName = userName || userNode.value;
  }
  
  if(!email) {
    if (userName) {
      email = userName+"@keystonejs.com";
    }
    else if(emailNode) {
        email = emailNode.value;
    }
  }
  
  if(email && pw) {
    var form = new FormData();
    form.append("email", email);
    form.append("password", pw);

    var csrf=document.getElementById("csrf");
    if(csrf) {
      form.append(csrf.getAttribute("csrf"), csrf.getAttribute("csrf-key"));
    }
    //name=csrf_token_key, value=csrf_token_value
    var notifyNode=notifyNode||document.getElementById("loginButton");
    if(notifyNode) {
       notifyNode.className+" loading";
    }
    var xhr = new XMLHttpRequest();
    xhr.open("post", "keystone/signin");
    xhr.onreadystatechange = function (aEvt) {
        window.location=redirect||"/";
    };
    xhr.send(form);
  } else {
    if(!email) {
        var node = userNode||emailNode;
        node.className="error";
        node.onchange=function(){
          this.className="";
        };
    } else if(!pw) {
        passwordNode.className="error";
        passwordNode.onchange=function(){
          this.className="";
        };
    }
  }
}
  
  
  //$.post("/keystone/signin", {
  //    data: {
  //      email: email,
  //      password: password
  //    },
  //    dataType: "application/json"
  //  },
  //  undefined, function(err){
  //    console.log("*** Login failed!! ***")
  //    console.log(err);
  //  });
//}

function logout(){
  window.location="/keystone/signout";
}

function betaSignup(notifyNode){
    var emailNode = document.getElementById("email");
    var errors = [];
    var email;
    if(emailNode) {
        email = emailNode.value;
        if(!email || !email.match("@")) {
          errors.push(emailNode);
    }

    if(email && errors.length==0) {
      if(notifyNode) {
         $(notifyNode).addClass("loading");
      }

      $.post("/betasignup", {
        data: { email: email },
        dataType: "application/json"
      }).done(function(data){
          $(notifyNode).removeClass("loading");
          $("#message").text("Thanks! We'll drop you a line at "+email);
        }).fail(function(resp, b, data){
            $(notifyNode).removeClass("loading");
            if(data.error) {
                $("#message").html(data.error);
            }
            else if(resp.responseJSON && resp.responseJSON.code==214) {
                $("#message").text("Looks like you're already signed up! Don't panic, we'll drop you a line once we've got some news to report.");
            }else {
                $("#message").text("Sorry, this doesn't look like a valid e-mail address. Problem? Drop us a line on info@electronicpoets.co.uk");
            }
        });
      } else {
        while(errors.length>0) {
          var erNode = errors.shift();
          erNode.className+=" error";
          erNode.onchange=function(evt){
            evt.target.className="";
          };
        }
      }
    }
}

function register(notifyNode) {
  // post a user user account
  var nameNode = document.getElementById("userName");
  var emailNode = document.getElementById("email");
  var codeNode = document.getElementById("code");
  var errors = [];
  var user = { name: {}};
  if(codeNode) {
      var code = codeNode.value;
        if(code) {
            user.code=code;
        }
  }
  if (nameNode) {
    var userName = nameNode.value;
    if(userName) {
        user.name.first=userName;
    } else {
      errors.push(userName);
    }
  }

  if(emailNode) {
      var email = emailNode.value;
      if(email && email.match("@")) {
        user.email=email;

      } else {
        errors.push(emailNode);
      }
  }

  if(errors.length==0) {
     if(notifyNode) {
         $(notifyNode).addClass("loading");
      }

    $.post("/create/user", {
      data: user,
      dataType: "application/json"
    }).done(function(data){
         doSimpleLogin(userName, undefined, email, "/user");
      }).fail(function(res){
        var evt = res.responseJSON;   
        if(notifyNode) {
            $(notifyNode).removeClass("loading");
         }
        $("#message").text(evt && evt.error?evt.error:"That doesn't appear to be a valid, pre-authorised e-mail address.");
      });
  }
  else {
    while(errors.length>0) {
      var erNode = errors.shift();
      erNode.className+=" error";
      erNode.onchange=function(evt){
        evt.target.className="";
      };
    }
  }
}

function removeStory(target){
    if(confirm("This will permanently destroy this story. Are you sure you want to proceed?")) {
        var p = target;
        while(!p.hasAttribute("data-storyid")) {
            p = p.parentNode;
        }
        var storyId=p.getAttribute("data-storyid");

        var xhr = new XMLHttpRequest();
        //xhr.open("get", "/keystone/stories?delete="+storyId);
        xhr.open("delete", "/story/"+storyId);
        xhr.send();

        // TODO - animate this nicely
        p.parentNode.removeChild(p);
    }
}


function auth(evt){
  var input = $("#authCode")[0];
  if(input) {
        var code = input.value;
        input.className+=" loading";
        var x = JSON.stringify({'code': code});
        console.log(x);
        $.post("/auth", {
          data: x,
          dataType: "application/json"
        }).done(function(data, status, xhr){
            // print a thank you and re-fresh the nav options
            if(xhr.status==200) {
              window.location=window.location;
            }
          }).fail(function(){
            // print an error: the code was invalid
            var messageNode=$("#messageNode");
            messageNode.addClass("visible");
          });
   }
};