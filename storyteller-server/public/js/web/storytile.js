// add dymanic behaviours to "storytiles"
require(["util/TimeStamp",
        "/js/lib/jquery-2.1.0/jquery.js",
        "/js/lib/underscore-1.6.0/underscore.min.js",
        "/js/lib/jquery-ui-1.11.1/jquery-ui.js"], function(ts){
    
    var createTile=function(tile){
//        // do stuff
//        console.log(tile);
        $(tile).hover(function(node){
            $(tile).addClass("focus");
        }, function(node){
            $(tile).removeClass("focus");
        })
        
        $(tile).click(function(evt){
            console.log("click!");
            console.log(tile);
            //var href = $(".title", tile)[0].getAttribute("data-href");
            var href = "/story/"+tile.getAttribute("data-storyid");
            window.open(href);
        });
    
    };
    
    //$.ready(function(){
        $(".storytile").each(function(idx,tile) {
            createTile(tile);
             $(".fi.remove", tile).click(function(evt){
                 evt.stopImmediatePropagation();
                 window.removeStory(tile);
             })
            
             $("[data-moment]", tile).each(function(idx, node) {
                var time = node.getAttribute("data-moment");
                if(time){
                    var formattedTime=time.substr(1,time.length-2);
                    ts.stamp(formattedTime, node);
                    node.removeAttribute("data-moment");
                    //node.appendChild();
                }
             })
            
        })
    //})
});
