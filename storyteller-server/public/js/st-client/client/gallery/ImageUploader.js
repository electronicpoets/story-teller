
/*
 * Widget to allow a widget to be uploaded
 */
define(["util/domUtils", 
    "client/story/widgets/Picture", 
    "util/ImageBank"
    ], function(dom, Picture, ImageBank){
    
    var Uploader = function(domNode, uploadTarget){
          var intro="Developer's Note: Like the Image Browser, this Uploading "
                    +"interface is functional, but pretty crude. I will be improving it "
                    +"later on. Bear in mind that after uploading a picture, it won't be "
                    +"visible in the Browser unless you close and re-openthis dialog (sorry!)";
        dom.create({text:intro, parent: domNode, class:"note"});
        
        this.nodes={
            parent: domNode,
            root: dom.create({class:"image-uploader", parent: domNode})
        };
        
        this._target = uploadTarget;
        this._formData={};
        this.init();
    };
    
    Uploader.prototype={
        
        _createInputPair: function(parentNode, label, dataProp, isShort, isRequired){
            var field = dom.create({class:"field", parent: parentNode});
            dom.create({type: "label", text: label, parent: field});
            var iWrapper = dom.create({class:"input-wrapper", parent: field});
            if(isShort) {
                iWrapper.className+=" short";
            }
            var attrs = {};
            if(dataProp) {
                attrs["data-prop"] = dataProp;
            }
            if(isRequired) {
                attrs.required=true;
            }
            dom.create({type: "input", class:"shadowed", parent: iWrapper, attrs: attrs});
            return field;
        },
        
        init: function(){
            var nodes = this.nodes,
                root = nodes.root;
            // OK, so we need:
            // Picture Title
            var form = dom.create({
                type:"form", 
                parent: root
            });
            
            this._createInputPair(form, "Title", "title", true, true);
             // Picture Description
            this._createInputPair(form, "Description", "description");
            
            // Picture Upload
            
            var imgField = dom.create({class:"field", parent: form});
            dom.create({type: "label", text: "Select Image", parent: imgField});
            var imgWrapper=dom.create({class:"input-wrapper preview", parent: imgField});
            var uploader = dom.create({
                type:"input", 
                class:"file-select",
                parent: imgWrapper,
                attrs: { 
                    type: "file", 
                    'data-url':"/create/image",
                    required:true
                }
            });
            // Picture Preview
            var preview = new Picture(imgWrapper);
            $(preview.domNode).addClass("preview");
            var formData=this._formData;
            var handleFileSelect = function(evt){
                var files = evt.target.files; // FileList object

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    // Only process image files.
                    if (!f.type.match('image.*')) {
                      continue;
                    }

                    var reader = new FileReader();
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                      return function(e) {
                        var imgBin = e.target.result;
                        formData.imgData=imgBin;
                        var id = "_uploader_tmp_";
                        ImageBank.set(id, imgBin );
                        preview._model = {
                            content: id,
                            ratio: "1:1", // can I calculate this on the client?
                            size: {
                                biggest: 150
                            }
                        };
                        preview.render();
                      };
                    })(f);
                    //formData.file=f;
                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
           };
           uploader.addEventListener('change', handleFileSelect, false);

            // Checkbox: is background
            var bgField = dom.create({class:"field", parent: form});
            dom.create({type: "label", text: "Make Background", parent: bgField});
            var wrapper = dom.create({class:"input-wrapper", parent: bgField});
            var input = dom.create({type:"input", class:"check", attrs:{type:"checkbox"}, parent: wrapper});
            input.onchange=function(){
                var checked = input.checked;
                formData.isBackground = checked;
            }
            // TODO: add tags
            
            // Submit button
            var buttonField = dom.create({class:"field center", parent: form});
            this.nodes.submit = dom.create({class:"page-button submit", parent: buttonField, click:this.submit.bind(this)});
            this.nodes.submitLabel = dom.create({type: "span", text:"Upload Picture", parent: this.nodes.submit});
            var throbber = dom.create({type:"span",class:"throbber", parent:this.nodes.submit});
            this.nodes.submit.insertBefore(throbber,this.nodes.submit.firstChild);
        },
        
        
        // Use toolttipster to display a dialog
        invalidate: function(node, message){
            $(node).addClass("invalid");
        },
        
        // strip any validation requirements
        validate: function(node){
            $(node).removeClass("invalid");
        },
        
        submit: function(){
            var target = this._target;
            var data=this._formData;
            // scrape data from the form
            var self=this;
            $("[data-prop]").each(function(idx, node){
                if(node.value) {
                    data[node.getAttribute("data-prop")] = node.value;
                     self.validate(node);
                } else if(node.hasAttribute("required")) {
                    // invalidate the node
                    self.invalidate(node);
                }
            });
            
            if(data.imgData && data.title) {
                $(this.nodes.submit).addClass("loading");
                var submitLabel = this.nodes.submitLabel;
                submitLabel.textContent="Loading...";
                $.post("/create/image", data, function(resp, status){
                    // I can set the image into the local cache now...
                    if(resp && resp.key){
                        ImageBank.set(resp.key, data.imgData);
                    }
                    $(self.nodes.submit).removeClass("loading");
                    submitLabel.textContent="OK!";
                    setTimeout(function(){
                        submitLabel.textContent="Upload Picture";
                    },2500);
                });
            }
        }
        
    };
    
    return Uploader;
    
});