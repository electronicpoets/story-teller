/* 
 * A dialog which enables 
 *      a) browsing the galelry
 *      b) uploading new files
 */

define(["client/edit/Dialog", "client/gallery/ImageBrowser", "client/gallery/ImageUploader", "util/domUtils"], 
    function(Dialog, Browser, Uploader, dom){
    
    var Gallery = function(model, events){
        this.events=events||{};
        this.nodes={};
        this._title="Picture Gallery";
        this._showCancel=true;
        
        this.dProps = {
            modal: true,
            resizable: false,
            moveable: false,
            dialogClass: "gallery"
        };
    };

    Gallery.prototype=new Dialog();

    Gallery.prototype._populate = function(parentNode){
        var tabs = this.nodes.tabContainer = dom.create({parent: parentNode});
        
        this.nodes.headers = dom.create({class:"headers", parent: tabs});
        var wrapper = dom.create({type:"ul", class:"header-wrapper", parent: tabs});
        wrapper.innerHTML="<li><a href=\"#gallery-browse\">Browse</a></li>"
                         +"<li><a href=\"#gallery-upload\">Upload</a></li>";
        
        var body = this.nodes.body = dom.create({class:"body", parent: tabs});
        var b = this.nodes.browser = dom.create({class:"container", id: "gallery-browse", parent: body});
        var u = this.nodes.upload = dom.create({class:"container", id: "gallery-upload", parent: body});
        
        $(tabs).tabs();
        this.browser = new Browser(b, "/data/images");
        // hook to double-click event
        this.browser.onDoubleClick=function(model){
            this.accept(model);
        }.bind(this);
        
        this.uploader = new Uploader(u, "/data/images");
    };
    
            
    Gallery.prototype.accept = function(selected) {
        var openIdx = $(this.nodes.tabContainer).tabs("option", "active");
        if(openIdx===0) {
            // only do this if the Browser is visible
            selected = selected||this.browser.getSelected();
            if(this.events.accept) {
                this.events.accept(selected);
            }
        }
        $(this.nodes.dom).dialog("close");
    };
            
    // TODO - on accept, return the selected model
    // double-click is accept and close
    
    return Gallery;
    
})

