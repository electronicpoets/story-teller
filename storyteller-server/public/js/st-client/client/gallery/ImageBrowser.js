/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




define(["util/domUtils", "client/story/widgets/Picture"], function(dom, Picture){
    
    var Paginator = function(domNode, model, control){
        this.nodes={
            parent: domNode,
            root: dom.create({class:"paginator", parent: domNode})
        };
        this.model = model;
        this.control=control;
        this.init();
    };

    Paginator.prototype={

        init: function(){
            var nodes = this.nodes,
                root = nodes.root;

            nodes.previous = dom.create({class:"page-button", text:"Previous", parent:root, click:this.previous.bind(this)});
            nodes.next = dom.create({class:"page-button", text:"Next", parent:root, click:this.next.bind(this)});
            this.updateState();
        },

        hasNext: function(){
            var model = this.model;
            return model.total > model.end;
        },

        hasPrevious: function(){
            var model = this.model;
            return model.start > 0;
        },

        // only go next if there IS a next
        next: function(){
            if(this.hasNext()) {
                var model = this.model;
                var start = this.model.start;
                var size = this.model.size;

                model.start+=size;
                model.end+=size;
                this.render(model);
                return model;
            }
        },

         previous: function(){
            if(this.hasPrevious()) {
                var model = this.model;
                var start = this.model.start;
                var size = this.model.size;

                model.start-=size;
                model.end-=size;
                this.render(model);
                return model;
            }
        },

        render: function(newModel){
            var model=newModel||this.model;
            //this.updateState(model);
            this.control.render(model);
        },

        updateState: function(){
            var nodes = this.nodes;
            if(this.hasNext()){
                $(nodes.next).addClass("show");
            } else {
                $(nodes.next).removeClass("show");
            }

            if(this.hasPrevious()){
                $(nodes.previous).addClass("show");
            }else {
                $(nodes.previous).removeClass("show");
            }
        }
    };
    
    
    
    var Browser = function(domNode, source){
        var intro="Developer's Note: This image browser is a BIG piece of "
                    +"work and there's a lot to be done. I'll add features "
                    +"like sorting, searching and better page control at a "
                    +"later date. Thanks for being patient with it!";
        
        dom.create({text:intro, parent: domNode, class:"note"});
        this._localDeletes=window._localDeletes; // help remember local deletes while we wait for the server
        if(!window._localDeletes){
             this._localDeletes=window._localDeletes={};
         }
        this._nameOverrides=window._nameOverrides; // help remember local deletes while we wait for the server
        if(!window._nameOverrides){
             this._nameOverrides=window._nameOverrides={};
        }
        // for now, show the paginator at the top of the page
        this.nodes={
            parent: domNode,
            page: dom.create({parent: domNode, class: "page-control"}),
        };        
        this._paginator = new Paginator(this.nodes.page, this.page, this);
        this.nodes.root = dom.create({parent: domNode, class: "image-browser"}),
        
        this._sourceUrl = source;
        this.init();
    };
    
    Browser.prototype={
        
        page: {
            current: 0, // the current page number
            start: 0,   // the first visible item
            end: 0 ,   // the last visible item
            total: 0,    // the total number of items
            size: 10    // the number of items per page
        },
        
        init: function(){
            this.page.end = this.page.start+this.page.size;
            
          this.loadData();   
        },
        
        loadData: function(){
            $.get(this._sourceUrl, function(data){
                this._data = data.results;
                this.page.total = data.results.length;
                this.render();
            }.bind(this));
        },
        
        _clickHandler:function(model, dom){
            this.select(dom, model);
        },
        
        _dblClickHandler: function(model, dom){
            if(this.onDoubleClick) {
               this.onDoubleClick(model); 
            }
        },
        
        render: function(page){
            // get all the picture data
            var data = this._data;
            if(page) {
                for(var p in page) {
                    this.page[p]=page[p];
                }
            }

            var start = this.page.start,
                end = this.page.end;

            var root = this.nodes.root;
            root.innerHTML = "";

            // Cheap - need to fix this for MM
            var controller = window.controller;
            var player = controller._activePlayer;
            var localDeletes=this._localDeletes;
            var nameOverrides=this._nameOverrides;
            // TODO: set the page size according to screen width
            // the question is: how many rows? Default to two.. user controlled? Auto calculated?
            for(var i=start;i<end;i++) {
                var d = data[i];
                if(d && !localDeletes[d.key]) {
                    var wrapper = dom.create({class: "thumb-wrapper", parent:root});
                    // create a new Picture content for each picture
                    var model = wrapper.model = {
                      content: d.key,
                      ratio: d.ratio,
                      size: {
                          biggest: 150
                      }
                    };

                    var p = new Picture(wrapper, model, {
                        click: this._clickHandler.bind(this),
                        dblclick: this._dblClickHandler.bind(this)
                    });
                    var name = nameOverrides[d.key]||d.name;
                    var titleNode = dom.create({class:"title", parent: wrapper, text: name});
                    var creatorWrapper =  dom.create({class:"creator-wrapper", parent: wrapper});
                    var isOwner=false;
                    if(d.creator) {
                        var name = d.creator.name;
                        dom.create({type: "span",class:"text", parent: creatorWrapper, text: "by"});
                        dom.create({type: "span",class:"author", parent: creatorWrapper, text: name.user||name.first});
                        if(d.createdAt) {
                            dom.create({type: "span",class:"text", parent: creatorWrapper, text:" at "});
                            dom.create({type: "span",class:"date", parent: creatorWrapper, text: d.createdAt});
                        }
                    } else {
                        dom.create({type: "span",class:"text", parent: creatorWrapper, html: "&nbsp;"});
                    }

                    // if the creator is the current user, or a Keystone admin, show editcontrols
                    // just rename (through cheap dialog) and remove for now
                    if(isOwner || player.isAdmin) {
                        var tools = dom.create({type: "span", class:"tooltray", parent: creatorWrapper});
                        var rename = (function(ctx, d,m) {
                            return function(){
                                this.renameImage(m,d);
                            }.bind(ctx)
                        })(this, titleNode, d);
                        dom.create({type: "span", class:"command", text:"Rename", click:rename, parent: tools});
                        
                        var remove = (function(ctx, d,m) {
                            return function(){
                                this.removeImage(m,d);
                            }.bind(ctx)
                        })(this, wrapper, d);
                        dom.create({type: "span", class:"command", text:"Remove", parent: tools, click:remove});
                    }
                    
                    var inserts = dom.create({class:"inserts", parent: creatorWrapper, html: "&nbsp;"});
                    if(d.inserts) {
                        inserts.innerHTML="<span>Times Used:</span>"
                        dom.create({type: "span",class:"count", parent: inserts, text:d.inserts});
                    }
                                            
                    p.render();
                }
            }
            if(this._paginator) {
                this._paginator.updateState(this.page);
            }
        },
        
        // return the selected MODEL
        getSelected: function(){
            if(this._selected) {
                return this._selected.model;
            }
        },
        
        isSelected: function(model){
            var s = this.getSelected();
            if(s) {
                return model.key == s.content;
            }
        },
        
        select: function(node, model){
            if(this._selected) {
                this.deselect(this._selected.node);
            }
            $(node).addClass("selected");
            model = model||node.model;
            this._selected={
                model: model,
                node: node
            };
        },
        
        deselect: function(node){
            $(node).removeClass("selected");
            this._selected=undefined;
        },
        
        renameImage: function(model, domNode){
            var newName = prompt("Please enter the new name for the  picture '"+model.name+"'");
            if(newName) {
                domNode.textContent=newName;
                model.name=newName;
                this._nameOverrides[model.key] = newName;
                
//                // AJAX it up to the server
//                $.post({
//                    url: "/image/"+model.key,
//                    dataType: "application/json",
//                    data: {
//                        name: newName
//                    }
//                });
                var xhr = new XMLHttpRequest();
                xhr.open("post", "/image/"+model.key);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.send(JSON.stringify({ name: newName}));
            }
        },
        
        removeImage: function(model, domNode){
            var name = model.name;
            if(confirm("Are you sure you want to remove the picture '"+name+"'?")) {
                var key = model.key;
                domNode.parentNode.removeChild(domNode);
                for(var i=0;i<this._data.length;i++){
                    var d = this._data[i];
                    if(d.key === key) {
                        this._localDeletes[d.key]=true;
                        this._data.splice(i,1);
                        if(this.isSelected(model)){
                            this.deselect(model);
                        }
                        
                        break;
                    }
                }
                // first, remove the image from the gallery DOM
                // then remove it from local data
                // finally, POST to the server
                $.ajax({
                    type: "DELETE",
                    url: "/image/"+model.key
                })
            }
        }
        
    };
    
    return Browser;
    
});

