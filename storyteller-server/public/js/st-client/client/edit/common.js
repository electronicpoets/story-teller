/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(["util/domUtils"], function(dUtils){
    
    var exports = {
        
        showTT: function(target, content, position, autodismiss){
            if(!target._tt) {
                target._tt = $(target).tooltipster({
                    contentAsHTML: true,
                    contentCloning: false,
                    position:position||"bottom",
                    interactive:true,
                    interactiveTolerance: autodismiss?2000:99999,
                    delay:99999
                });
            } 
            $(target).tooltipster("content", $(content))
                    .tooltipster("show");
        },
        
        hideTT: function(target) {
            if(target._tt) {
                $(target).tooltipster("hide");
            }
        },
        
        _createControl: function(parent, label, type, onChange) {
            var root = dUtils.create("div", "dataField", parent);

            var labelNode = dUtils.create("div", "dataField label", root);
            labelNode.textContent = label;

            var inputNode;
            if (type == "select") {
                var options = arguments[4];
                var selected = arguments[5];
                inputNode = dUtils.create("select", "dataField input", root);
                var idx;
                for (var i = 0; i < options.length; i++) {
                    var o = options[i];
                    var opt = dUtils.create("option", "dataField input", inputNode);
                    var value = o.hasOwnProperty("value")?o.value: (o.label||o);
                    if (value == selected) {
                        idx = i;
                    }
                    if(value) {
                        opt.setAttribute("value", value);
                    }
                    opt.textContent = o.label||o.value||o;
                }
                inputNode.selectedIndex = idx;
                 inputNode.onchange = onChange;
            } else if (type == "label") {
                inputNode = dUtils.create("div", "dataField input", root);
                inputNode.textContent = arguments[4];
                // short-term hack
                root.updateContent = function(content) {
                    inputNode.textContent = content;
                }
                inputNode.onchange = onChange;
            } else if (type == "check") {
                var label2 =  dUtils.create({type: "label", class:"check", parent:root});
                inputNode = dUtils.create({type:"input", class:"dataField input", parent:label2, attrs: {type: "checkbox"}});
                inputNode.checked = arguments[4];
                if(inputNode.checked) {
                    $(label2).addClass("checked");
                }
                inputNode.onchange = function(){
                    if(this.checked) {
                        $(label2).addClass("checked");
                    } else {
                        $(label2).removeClass("checked");
                    }
                    onChange(this.checked);
                }
                
                var tooltip = arguments[5];
                if(tooltip) {
                    dUtils.addTooltip(label2, tooltip)
                }
                //inputNode.appendChild();
            } else {
                var args = {
                    type: type,
                    class: "dataField input",
                    parentNode: root
                };
                var value = arguments[4];
                if(type=="textarea") {
                    args.text=value;
                }
                else if (typeof value == "string") {
                    args.attrs = {value: value};
                }
                inputNode = dUtils.create(args);
                //inputNode = dUtils.create(type, "dataField input", root);
                 $(inputNode).on("blur keyup paste input", onChange);
            }

           
            return root;
        }
    };
    
    return exports;
})