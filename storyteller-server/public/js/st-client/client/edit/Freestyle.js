// Dialog which pops up and configures a page link
// takes a link as an argument to pre-populate the field,
// or creates a new link
// provides onAccept, onCancel events. onAccept passes the new model to the callback.
// TODO - rename to LinkDialog
    
define(["util/domUtils", "client/edit/common"],function(dom, common){
   
    var exports = {
        
        generate: function(model, events, prompt){
            return this._populate(model, events);
        },
        
        _populate: function(model, events, prompt){
            var nodes = this.nodes;
            var model = this._deltas;

            var table = dom.create({class:"st-form freestyle"});
            var intro = dom.create({class:"intro", 
                parentNode: table, 
                text: prompt||"Enter a Freestyle Action to send to the Storyteller"
            });
            
            var left = dom.create({class:"column", parent:table});
            var right = dom.create({class:"column", parent:table});
            
            var input = dom.create({type:"textarea", parent: left});
            dom.create({class:"button", text:"Do it!", parentNode: right, click:function(){
                   var value = input.value;
                   console.log(value);
                   events.onAccept(value); 
            }});
        
            dom.create({class:"button", text:"Close", parentNode: right, click:function(){
               if(events.onCancel) {
                    events.onCancel();
                }
            }});
            
            return table;
        }
    };
    
    return exports;
	
//	var FreestyleDialog = function(player, events, model, options){
//		this._title="Freestyle";
//		//this._defaultHeight=150;
//		
//		this.player=player;
//		this.events=events||{};
//		this.nodes={};
//		this._deltas = $.extend({}, model); // clone the model
//                this._message={};
//	};
//	
//	FreestyleDialog.prototype=new Dialog();
//	
//	FreestyleDialog.prototype._populate = function(parentNode){
//		var nodes = this.nodes;
//		var model = this._deltas;
//                var message = this._message;
//		var playerId = this.player.id;
//
//		nodes.intro = dom.create({class:"intro", 
//			parentNode: parentNode, 
//			text: model.prompt||"Enter a Freestyle Action to send to the Storyteller"
//		});
//
//		var table = dom.create({class:"st-form", parentNode: parentNode});
//
//		nodes.input = this._createField("Action", function(){
//			message.content=this.value;
//		});
//		table.appendChild(nodes.input);
//	};
//	
//	FreestyleDialog.prototype.accept =function(){
//		if(this.events.accept) {
//			this.events.accept(this._deltas, this._message.content);
//		}
//		this._deltas={};
//	};
//	
//	return FreestyleDialog;
	
});