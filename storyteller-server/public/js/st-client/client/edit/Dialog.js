// Abstract class to capture the required behaviour for a dialog

define(["util/domUtils"], function(dom) {

    var Dialog = function() {

    };

    Dialog.prototype = {
        dProps: {
            modal: true
        },
        
        _createField: function(label, onEdit, type, value) {


            var root = dom.create({class: "field"})
            var node = document.createElement("div");

            var labelNode = dom.create({class: "label", text: label, parentNode: root});

            var input = dom.create({type: "input", parentNode: root});
            if (type == "check") {
                input.setAttribute("type", "checkbox");
                input.checked = value;
            } else if (value) {
                input.value = value;
                input.className += " stretch";
            }
            // weak
            else {
                input.className += " stretch";
            }
            input.onchange = onEdit;

            //node.setValue = function(newValue){
            //	input.value=newValue||"";
            //};
            root.disable = function() {
                $(node).addClass("disabled");
                input.setAttribute("disabled", true);
            };
            root.enable = function() {
                $(node).removeClass("disabled");
                input.removeAttribute("disabled");
            };
            return root;
        },
        _populate: function(parentNode) {

        },
        open: function() {
            var dialog = this.nodes.dom = dom.create({attrs: {title: this._title}});

            var body = this.nodes.body = dom.create({class: "st-dialog-body", parentNode: dialog});
            this._populate(body);

            var cmdTray = dom.create({class: "commands"});

            var okButton = dom.create({class: "button", text: "OK", parentNode: cmdTray});
            $(okButton).click(function() {
                this.accept();
                //$(dialog).dialog("close");
            }.bind(this));

            if (this._showCancel) {
                var cancelButton = dom.create({class: "button", text: "Close", parentNode: cmdTray});
                $(cancelButton).click(function() {
                    $(dialog).dialog("close");
                });
            }

            dialog.appendChild(cmdTray);


            $(dialog).dialog(this.dProps);
        },
//        close: function(){
//            $(this.nodes.dom).dialog("close");  
//        },
        // triggers the "accept" event, passing in the new page link and, if appropriate,
        // the new page model
        accept: function() {
            this.events.accept(this._deltas);
            this._deltas = {};
             $(this.nodes.dom).dialog("close");
        },
        cancel: function() {
            this.events.cancel();
        },
        destroy: function() {

        }

    };

    return Dialog;

});