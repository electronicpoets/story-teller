/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(["util/domUtils", "client/edit/common"],function(dom, common){
   
    var exports = {
        
         generate: function(model, events){
            var isLiveStory = model.type=="livestory";
            
            var table = dom.create({class:"st-form"});
            // group controller;
            var buttons={};
            var deltas={};
            var onEdit=function(prop, value){
                deltas[prop]=value;
                $(buttons.save).addClass("active");
            };

            dom.create({parentNode: table, text:"Edit Story Details", class:"header"});

            common._createControl(table, "Title", "input", function(){
                if(model.title!=this.value) {
                    onEdit("title", this.value);
                }
            }, model.title);
            
            common._createControl(table, "Description", "textarea", function(){
                if(model.description!=this.value) {
                    onEdit("description", this.value);
                }   
            }, model.description);
            
             common._createControl(table, "Published", "check", function(checked){
                 if(model.isPublished!=checked) {
                    onEdit("isPublished", checked);
                }   
            }, model.isPublished, "Published stories will be visible under the Alpha Testing Library");

            // finish or publish the story
            // Note: if the story is already  finished or published, this is just a status
            // Mind you, we can unpublish...
            var statusGroup = dom.create({parentNode: table, class:"group"});
            
            var saveGroup = dom.create({parentNode: table, class:"group"});
            buttons.save=dom.create({parentNode: saveGroup, text:"Save", class:"button", click:function(){
                events.onAccept(deltas);
            }});
            buttons.close=dom.create({parentNode: saveGroup, text:"Close", class:"button active", click:events.onClose});
            
            return table;
        }

        
    };
    
    return exports;
});