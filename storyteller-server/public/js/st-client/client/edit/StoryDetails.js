// Dialog which pops up and configures a story
// Can be opened in read or edit mode

define(["client/edit/Dialog", "util/domUtils"], function(Dialog, dom){
	
	var StoryDialog = function(model, events){
		this._model = model;
		this.nodes={};
		this._deltas = {};
		
		this.events=events||{};

		this._title="Edit Story Details";
		this._defaultHeight=300;
		this._showCancel=true;
	};
	
	StoryDialog.prototype=new Dialog();
	
	StoryDialog.prototype._populate = function(parentNode){
		var nodes = this.nodes;
		var model = this._model;
		
		var isLiveStory = model.type=="livestory";
		var table = dom.create({class:"st-form", parentNode: parentNode});
		
		nodes.title = this._createField("Title", function(){
			model.title=this.value;
		}, undefined, model.title);
		table.appendChild(nodes.title);
		//nodes.title.setValue(model.title);
		
		if (isLiveStory) {
			if(!nodes.objective) {
				nodes.objective = this._createField("Objective", function(){
					model.objective=this.value;
				}, undefined, model.objective);
				parentNode.appendChild(nodes.objective);
			}
			//nodes.objective.setValue(model.objective);
		}
		
		nodes.description = this._createField("Description", function(){
			model.description=this.value;
		}, undefined, model.description);
		table.appendChild(nodes.description);
		
		nodes.isComplete = this._createField("Completed", function(evt){
			var n = evt.target;
			this._deltas.isComplete = n.checked;
		}.bind(this), "check", model.isComplete);
		table.appendChild(nodes.isComplete);
		
		nodes.isPublished = this._createField("Published", function(evt){
			var n = evt.target;
			this._deltas.isPublished = n.checked;
		}.bind(this), "check", model.isPublished);
		table.appendChild(nodes.isPublished);
	};
			
	
	return StoryDialog;
	
});