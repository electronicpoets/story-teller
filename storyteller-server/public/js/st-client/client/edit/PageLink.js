// Dialog which pops up and configures a page link
// takes a link as an argument to pre-populate the field,
// or creates a new link
// provides onAccept, onCancel events. onAccept passes the new model to the callback.
// TODO - rename to LinkDialog

define(["client/edit/Dialog", "util/domUtils"], function(Dialog, dom){
	
	var LinkDialog = function(player, events, model, options){
		if(!model) {
			model = {
				id: window.generateLinkId(),
				target: undefined,
			  	title: "Next Page",
			  	subTitle: undefined
		  	};
		} /*else {
			this._restrictNewPage=true;
		}*/

		this._title="Edit Page Link";
		this._defaultHeight=150;
		
		this.player=player;
		this.events=events||{};
		this.nodes={};
		this._deltas = $.extend({}, model); // clone the model
	};
	
	LinkDialog.prototype=new Dialog();
	
	LinkDialog.prototype._populate = function(parentNode){
		var nodes = this.nodes;
		var model = this._deltas;

		var table = dom.create({class:"st-form", parentNode: parentNode});

		nodes.title = this._createField("Title", function(){
			model.title=this.value;
		}, undefined, model.title);
		table.appendChild(nodes.title);
		
		nodes.desc = this._createField("Description", function(){
			model.subtitle=this.value;
		}, undefined, model.subtitle);
		table.appendChild(nodes.desc);


		// TODO - implement an autocomplete or drop-down here. Combobox, init
		nodes.target = this._createField("Target", function(){
			model.target=this.value;
		}, undefined, model.target);
		table.appendChild(nodes.target);
		
		if(!this._restrictNewPage && !nodes.newPage) {
			var self=this;
			nodes.newPage = this._createField("New Page?", function(){
				self._createNewPage=this.checked;
				if(self.checked) {
					self.nodes.target.disable();
				} else {
					self.nodes.target.enable();
				}
			}, "check");
			table.appendChild(nodes.newPage);
		}

		nodes.freestyle = this._createField("Freestyle?", function(){
			model.freestyle=this.checked;
			if(self.checked) {
				self.nodes.prompt.disable();
			} else {
				self.nodes.prompt.enable();
			}
		}, "check", model.freestyle);
		table.appendChild(nodes.freestyle);

		nodes.prompt = this._createField("Prompt:", function(){
			model.prompt=this.value;
		}, undefined, model.prompt);
		table.appendChild(nodes.prompt);
	};
	
	LinkDialog.prototype.accept =function(){
		var page;
		this.player.addLink(this._deltas);
		if(this._createNewPage) {
			page = this.player.addPage(undefined, this.player._currentPageId);
			this._deltas.target=page.id;
		} 
		
		this.events.accept(this._deltas, page);
		this._deltas={};
	};
	
	return LinkDialog;
	
});