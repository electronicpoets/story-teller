// CLass representing the player of a story
// A player can be a story teller, or just a player
// A player has a name, icon, and a Story instance
// Player acts as a proxy to the Story, filtering all next/previous/current requests through 
// the state properties
define(["client/story/Page", "client/util/keys"], function(Page, keys) {

    var pid = 0;

    // A player must take the following kwArgs:
    //		- story: a Story instance
    //		- domNode: a DOM node to render content inside
    //		- session: a Session to synchronise with
    //		- name: the player's name. Will need extending soon.
    //		- state: the player's page state/history
    //		- storyteller: if true, this player will be treated as a storyteller
    //		- changeNotifier: widget to call when state change is detected
    //		- start: the page id to start on
    var Player = function(kwArgs) {
        // set state from the args
        var session;
        var firstPage;
        if (kwArgs) {
            this.name = kwArgs.name;
            this.story = kwArgs.story;
            this.domRoot = kwArgs.domNode;
            session = this._session = kwArgs.session;

            // index of player state for each page in the story	
            if (kwArgs.state) {
                firstPage = kwArgs.state._current;
                // bit messy... take the first page as a seperate argument I think
                if (firstPage) {
                    kwArgs.state._current = null;
                    delete kwArgs.state._current;
                }
                this._state = kwArgs.state;

            } else {
                this._state = {};
            }

            if (kwArgs.storyteller) {
                this.isStoryteller = true;
            }
            if (kwArgs.isOnline) {
                this.isOnline = true;
            }
            if (kwArgs.isAdmin) {
                this.isAdmin = true;
            }
            if (kwArgs.isSpectator) {
                this.isSpectator = true;
            }
            // function to call when this story detects a change
            // Needs a "registerChange" and "clearChange"
            // 
            if (kwArgs.changeNotifier) {
                this.setNotifier(kwArgs.changeNotifier);
            }

        } else {
            throw {message: "Player not initialised correctly "};
        }

        session.on("story.changed", this.onStoryChanged.bind(this));
//        if (this.isStoryteller) {
//            session.on("player.pageComplete", this.onPageComplete.bind(this));
//            session.subscribe("player.pageComplete", this.onPageComplete.bind(this));
//        }

        // set internal state
        this.id = kwArgs.id || pid++;

        // store any active notifications
        this._notifications = {
            change: []
        };

        // Store any local, unsaved changes
        this._localChanges = { /*pageId:*/ /*array of new content*/};

        // store any changes received from other stories
        this._remoteChanges = { /* pageId*/};
        this._pages = {}; // story all pages which this player has rendered

        // finally, set the current page
        //this._setCurrentPage(story.getFirstPageId());
        this._setCurrentPage(firstPage || story.getFirstPageId());
    };

    Player.prototype = {
        // Keep a player-specific track of the current page
        _setCurrentPage: function(pageId) {
            if (pageId != this._currentPageId) {
                //console.log("Player "+this.name+" setting current page id to "+pageId);
                this._currentPageId = pageId;

                var state = this._state[pageId];
                if (!state) {
                    state = this._state[pageId] = {next: undefined, previous: undefined};
                }
            }
        },
        setWaiting: function(linkId, isWaiting){
            var state = this._state[linkId];
            if(isWaiting) {
                if (!state) {
                    state = this._state[linkId] = { waiting: true};
                } else {
                    state.waiting=true;
                }
            } else if(state) {
                state.waiting=false;
            }
            //this._session.emit("player.state", { id: linkId, state: isWaiting? { waiting: true } : null });
                    
        },
        isWaiting: function(linkId){
            var state = this._state[linkId];
            if(state) {
                return state.waiting;
            }
        },
        setActive: function(isActive) {
            this._active = isActive;
            if (isActive && this.changeNotifier) {
                var notes = this._notifications.change;
                while (notes.length > 0) {
                    this.changeNotifier.clear(notes.shift());
                }
            }
        },
        setNotifier: function(notifier) {
            this.changeNotifier = notifier;
        },
        getCurrentPageId: function() {
            return this._currentPageId;
        },
        // get a localised version of a particular page
        // defaults to the current page
        getPage: function(pageIdOrTitle) {
            pageIdOrTitle = pageIdOrTitle || this._currentPageId;
            var pageId = this.story.getPageId(pageIdOrTitle);
            if(pageId) {
                var page = this.story.getPage(pageId);
                if (page && !page.id) {
                    page.id = pageId; // just be sure...
                }
                var deltas = this._getChanges(keys.content.PAGE, pageId);
                this._mergeChanges(page || {id: pageId}, deltas);
                return page;
            }
        },
        getPageInstance: function(pageId){
          pageId=pageId||this._currentPageId;
            return this._pages[pageId]  ;
        },
        _mergeChanges: function(pageData, deltas, isNew) {
            if (deltas) {
                for (var p in deltas) {
                    if (p == "contents") {
                        var changedContent = deltas.contents;
                        if (changedContent && changedContent.length > 0) {
                            var newContent = []; // overwrite the property to save the original array. Shady.
                            // clone and index the original page contents
                            var index = {};
                            if (pageData.contents) {
                                for (var i = 0; i < pageData.contents.length; i++) {
                                    var c = pageData.contents[i]; // shallow clone?
                                    index[c.id] = c;
                                    newContent.push(c);
                                }
                            }

                            // add in new changes
                            for (var j = 0; j < changedContent.length; j++) {
                                var delta = changedContent[j];
                                if (index[delta.id]) {
                                    // merge with an existing piece of content
                                    var c = index[delta.id];
                                    c._delta = delta;
                                } else {
                                    // or jus push the new content into an array
                                    newContent.push(delta);
                                }
                            }
                            pageData.contents = newContent;
                        }
                    }
                    else if (p != "id") {
                        pageData[p] = deltas[p];
                    }
                }
            }
        },

        getPageContent: function(pageId) {
            var page = this.getPage(pageId);
            return page.contents;
        },
        // set the current page
        setPage: function(pageId, isForward) {
            var prevPage = this._currentPageId;
            this._setCurrentPage(pageId);
            if (isForward) {
                this.markPageComplete(prevPage, isForward, pageId);
                this._state[pageId].previous = prevPage;
            }

            // publish notification of the page change
            this._publish("player.page", {pageId: pageId});

            return this.getPage(pageId);
        },
        // should nextId be a page id, or a link id? Probably a page id...
        // I don't think the approach of adding links without pages really works
        markPageComplete: function(pageId, linkId, nextPage, content) {
            if(!window.spectateMode) {
                var state = this._state[pageId];
                if(!state) {
                    state = this._state[pageId] = {};
                }
                // TODO - in prose stories, at least, we want to track the last button that was clicked - so the
                // storyteller stores no state rule might not be valid after all
                // Rather, a storyteller doesn't have their options restricted, that's all
                if ((!state.next && linkId) || this.isStoryteller) {
                    console.log("Player " + this.name + " marked page " + pageId + " as complete");

                    state.next = linkId;
                    // annotate the Story model to indicate what this player selected for this page
                    // the Storyteller will need this information later.
                    var evt ={
                        pageId: pageId, 
                        linkId: linkId, 
                        nextPageId: nextPage, 
                        message:content,
                        timestamp: new Date().toString()
                    };

                    // lookup the link id. If it's not valid, we need to subscribe
                    if(!this.story.isLinkValid(linkId)) {
                        this._session.emit("player.registerLinkListener", { storyId: this.story.id, playerId: this.id, linkId: linkId});
                    }
                    if(!this.isStoryteller) {
                        this._publish("player.pageComplete", evt);
                    }
                }
            }
        },
        // convenience API
        _publish: function(evt, data) {
            data = data || {};
            data.playerId = this.id;
            data.playerName = this.name;
               
            // publish local and remote events
            this._session.emit(evt, data);
            this._session.publish(evt, data);
        },
//        // receive notification that a player has completed a page
//        onPageComplete: function(evt) {
//            console.log("Storyteller (" + this.name + ") :: detected page complete event!");
//
////            var page = this._players[evt.pageId];
////            if(page) {
////                page.addResponse(evt.playerId, evt.linkId);
////            }
//            // also write to the Story? What if the page is created later?
//
////            if (!this._responses) {
////                this._responses = {};
////            }
////            var data = this._responses;
////            if (!data[evt.pageId]) {
////                data[evt.pageId] = {};
////            }
////
////            var pageData = data[evt.pageId];
////            pageData[evt.playerId] = {
////                playerName: evt.playerName,
////                target: evt.linkId
////            };
//        },
//        getResponseData: function(pageId) {
//            if (this._responses) {
//                return this._responses[pageId];
//            }
//        },
        getResponse: function(pageId){
            pageId = pageId||this._currentPageId;
            var responses= this.story.getResponses(pageId);
            if(responses) {
                return responses[this.id];
            }
        },
        isPageComplete: function(pageId) {
            if (!pageId) {
                pageId = this._currentPageId;
            }
            var state = this._state[pageId];
            return state ? state.next : false;
        },
        // get a list of a all the valid next pages
        getNextPages: function() {
            var links = this.getNextLinks();
            var pages = [];
            for (var i = 0; i < links.length; i++) {
                var target = links[i].target;
                if (target) {
                    pages.push(this.getPage(target));
                }
            }
            return pages;
        },
        getNextLinks: function() {
            var currentPageId = this._currentPageId;
            var links = this.story.getNextPages(currentPageId) || [];

            // Identify any local changes
            var changes = this._getChanges(keys.content.LINK);

            // If state is set, limit any links to just those matchingthestate
            var targetId;
            var state = this._state[currentPageId];
            if (!this.isStoryteller && state && state.next) {
                targetId = state.next;
            }

            // Merge any existing links with any local player changes
            var newContent = [];
            var empties = [];
            var base = {};
            var l;
            for (var i = 0; i < links.length; i++) {
                l = links[i];
                if (!targetId || targetId == l.id) {
                    var tmp = base[l.id] = $.extend({}, l); //newContent.length;
                    newContent.push(tmp);
                }
            }
            if (changes) {
                for (var j = 0; j < changes.length; j++) {
                    // TODO - check if this link exists and has simply been modified
                    l = changes[j];
                    if(Object.keys(l).length==1) {
                        empties.push(l);
                    }
                    else if (!targetId || targetId == l.id) {
                        if (base.hasOwnProperty(l.id)) {
                            // newContent.splice(base[l.id],1,l);
                            // overwrite all the original propert
                            $.extend(base[l.id], l);
//                                    var x = base[l.id];
//                                    for(var p in l) {
//                                        x[p]=l[p];
//                                    }
                        } else {
                            newContent.push(l);
                        }
                    }
                }
            }
            while(empties[0]) {
                newContent.push(empties.shift());
            }

            return newContent;
        },
        // get a list of all the valid previous pages
        getPreviousPage: function(pageId) {
            var links = this.getPreviousLinks(pageId);
            if (links && links[0]) {
                return this.getPage(links[0].target);
            }
        },
        getPreviousLinks: function(pageId) {
            var state = this._state[pageId || this._currentPageId];

            if (state && state.previous) {
                // experimental
                var result = {target: state.previous};
                if(this.story.contentType==="Prose") { // todo - make this a global option
                    var st = this._state[state.previous]
                    console.log(st);
                    if(st && st.next) {
                        var link = this.story.getLink(st.next);
                        console.log(link);
                        if(link) {
                            result.title=link.title;
                            result.subtitle = (link.responses ? link.responses[this.id]:undefined) || link.subtitle;
                        }
                    }
                }
                return [result];
            }

            var currentPageId = pageId||this._currentPageId;
            var state = this._state[currentPageId];
            var links;
            if (state && state.previous) {
                links = [state.previous];
            }
            else {
                var pages = this.story.getPreviousPages(currentPageId) || [];
                var links = [];
                var result;
                for (var i = 0; i < pages.length; i++) {
                    // generate a link for each previous page target
                    result = {target: pages[0]};
                    //result.title="Wilshire";
                    links.push(result);
                }
            }

            return links;
        },
        // slightly naughty interface for Buttons
        getPages: function(){
            return this.story._pages;
        },
        getName: function() {
            return this.name;
        },
        // edit interface

        // if "inherit" is true, the page will inherit a bg from the current
        addPage: function(page, prevId, inherit) {
            page=page||{};
            if (!page.id) {
                page.id = this.story._generateId();
            }
            if (!page.contents) {
                page.contents = [];
//                if(this.story.contentType=="Prose") {
//                    page.contents.push([{type:"text"}])
//                }
            }
            if (prevId) {
                page.previous = [prevId];
            }

            var count = page.number = Object.keys(this.story._pages).length+1;
            if(!page.title) {
                page.title = "Page-" + (count);
            }
            if(inherit) {
                var current = this.getPage();
                if(!page.background) {
                    page.background = current.background;
                }
                if(!page.hasOwnProperty("fadeBg")) {
                    page.fadeBg = current.fadeBg;
                }
            }

            // add a dummy entry to the Story, for numbering consistency
            this.story._pages[page.id] = {};
            this._logChange(keys.content.PAGE, page, page.id);
            return page;
        },
        editPage: function(page, deltas) {
            if (typeof page == "string") {
                page = this.getPage(page);
            }

            var model = {
                id: page.id
            };
            for (var p in deltas) {
                model[p] = deltas[p];
            }
            this._logChange(keys.content.PAGE, model, page.id);
            return page;
        },
        // add (or edit) a link to another page
        addLink: function(link) {
            this._logChange(keys.content.LINK, link);
            return link;
        },
        removeLink: function(link) {
            // add to model
            var model = {
                id: link.id,
                destroyed: true
            }
            this._logChange(keys.content.LINK, model);
        },
        editLink: function(model) {
            this._logChange(keys.content.LINK, model);
        },
        _getChanges: function(type, pageId) {
            pageId = pageId || this._currentPageId;

            var changes = type == "page" ? {} : [];
            var sources = [this._remoteChanges[pageId], this._localChanges[pageId]];
            while (sources.length > 0) {
                var s = sources.shift();
                if (s) {
                    if (type == "page") {
                        for (var prop in s) {
                            changes[prop] = s[prop];
                        }
                    }
                    else if (s[type]) {
                        changes = changes.concat(s[type]);
                    }
                }
            }
            if (type == "page") {
                return Object.keys(changes).length > 0 ? changes : undefined;
            }
            else if (changes.length > 0) {
                return changes;
            }
        },
        // Note that a change should ONLY record the changed properties.
        _logChange: function(type, model, pageId) {
            pageId = pageId || this._currentPageId;
            var changes = this._localChanges[pageId];
            if (!changes) {
                changes = this._localChanges[pageId] = {};
            }

            // Page updates are easy: we just write any changed properties to the page object
            if (type == keys.content.PAGE) {
                for (var p in model) {
                    if (p != "id") {
                        changes[p] = model[p];
                    }
                }
            } else {
                if (!changes[type]) {
                    changes[type] = [];
                }

                // TODO: make sure that only one change per model is recorded...
                var typeChanged = changes[type];
                for (var i = 0; i < typeChanged.length; i++) {
                    var delta = typeChanged[i];
                    if (delta.id == model.id) {
                        //console.log("Detected change to content "+model.id+", merging");
                        // merge this change...
                        for (var p in model) {
                            delta[p] = model[p];
//                            if(model[p]) {
//                                delta[p] = model[p];
//                            } else if(delta[p]) {
//                                delta[p]=undefined;
//                                delete delta[p];
//                            }
                        }
                        this._session.publish("player.localChange");
                        return; // all done!
                    }
                }

                // write a temporary id to the model
                if (!model.id) {
                    //model.id="@"+this._contentId++;
                    model.id = this.story._generateId();
                }
                changes[type].push(model);
                // publish a local notification
                //this._session.publish("player.localChange");
                if(Object.keys(model).length>1) {
                    this._session.publish("player.localChange");
                }
            }
        },
        _contentId: 0,
        addContent: function(type, content, pageId) {
            var newContent;
            if (arguments.length == 1) {
                newContent = arguments[0];
            } else {
                newContent = {
                    type: type,
                    content: content
                };
            }

            // center on page (tmp)
            newContent.position = {
                horizontal: "center",
                vertical: "middle"
            };

            // add to model
            this._logChange(keys.content.CONTENT, newContent, pageId);
            return newContent;
        },
        removeContent: function(contentModel) {
            console.log("Removing content " + contentModel.id);

            // add to model
            var model = {
                id: contentModel.id,
                destroyed: true
            };
            this._logChange(keys.content.CONTENT, contentModel);
        },
        // log a change in the propeties of some contnet
        updateContent: function(id, contentDeltas) {
            //console.log("Player detecting change to content");
            contentDeltas.id = id; // so we can track
            this._logChange(keys.content.CONTENT, contentDeltas);
        },
        onLinkValidated: function(){
            console.log("Player "+this.id+" link validated!");
            // if the player is on the page, update the commands list
            
            // otherwise, display a notification. Somewhere.
        },
        // special notification that a page has been published
        onPagePublished: function(pageId, isPublished) {
            if (isPublished) {
                this.story.publishPage(pageId);
            } else {
                this.story.unpublishPage(pageId);
            }

            console.log("Player :: page " + pageId + " published!");
            if (this._publishListeners && this._publishListeners[pageId]) {
                var callbacks = this._publishListeners[pageId];
                while (callbacks[0]) {
                    var callback = callbacks.shift();
                    if (callback) {
                        callback();
                        this._publishListeners[pageId] = undefined;
                        delete this._publishListeners[pageId];
                    }
                }
                this._session.emit("player.unsubscribe", {
                    storyId: this.story.id,
                    playerId: this.id,
                    pageId: pageId
                });
            }
        },
        addPagePublishListener: function(pageId, callback) {
            if (!this._publishListeners) {
                this._publishListeners = {};
            }
            if (!this._publishListeners[pageId]) {
                this._publishListeners[pageId] = [];
            }
            this._publishListeners[pageId].push(callback);
            this._session.emit("player.subscribe", {
                storyId: this.story.id,
                playerId: this.id,
                pageId: pageId
            });
        },
        isPagePublished: function(pageId) {
            var model = this.story._pages[pageId];
            if (model) {
                return model.status == "shared";
            }
        },
        // save and push any changes to the model
        pushChanges: function() {
            // for each changed page, push changes to the approprate page instance
            var deltas = {};

            for (var pageId in this._localChanges) {
                var changes = this._localChanges[pageId];
                var d = deltas[pageId] = {};

                // first add any page changes
                for (var prop in changes) {
                    var value = changes[prop]
                    if (prop != "id" && prop[0] != "_") {
                        if (prop == "next" || prop == "contents") {
                            d[prop] = [];
                            for (var i = 0; i < value.length; i++) {
                                var item = value[i];
                                if (Object.keys(item).length > 1) {
                                    var clone = {};
                                    for (var p in item) {
                                        if (p[0] != "_") {
                                            clone[p] = item[p];
                                        }
                                    }
                                    d[prop].push(clone);
                                }
                            }
                        } else {
                            d[prop] = changes[prop];
                        }
                    }
                }

                this._localChanges[pageId] = undefined;
                delete this._localChanges[pageId];
            }
            console.log("Player deltas");
            console.log(deltas);
            return deltas;
        },
        // receive notification that the story has changed
        // note that a story shouldn't receive its own update...
        onStoryChanged: function(evt) {
            if (evt.playerId != this.id) {
                var deltas = evt.deltas;
                console.log("> " + this.id + " :: story changed!");
                console.log(deltas);
                // save all deltas to the change array
                var changes = this._remoteChanges;
                for (var pageId in deltas) {
                    // only merge in deltas if the player has seen the page
                    // otherwise, the player sees the changed story directly
                    if (this._state[pageId]) {
                        var delta = deltas[pageId];
                        var page = changes[pageId];
                        if (!page) {
                            page = changes[pageId] = {};
                        }

                        for (var prop in delta) {
                            // sloppy condition, refine later
                            if (prop !== "id" && prop != "contents" && prop != "next") {
                                page[prop] = delta[prop];
                                if (prop == "status") {
                                    this.onPagePublished(pageId, delta[prop] == "shared");
                                }
                            }
                        }

                        // now log any content changes
                        var contentChanges = delta.contents;
                        if (contentChanges && contentChanges.length > 0) {
                            if (!page.contents) {
                                page.contents = [];
                            }
                            for (var i = 0; i < contentChanges.length; i++) {
                                var c = contentChanges[i];
                                page.contents.push(c); // just push all changes lazily into the array
                                // we should be able to collapse this easily at render time
                            }
                        }

                        // and finally, links
                        var linkChanges = delta.next;
                        if (linkChanges && linkChanges.length > 0) {
                            if (!page.next) {
                                page.next = [];
                            }
                            for (var i = 0; i < linkChanges.length; i++) {
                                var l = linkChanges[i];
                                page.next.push(l); 		// just push all changes lazily into the array
                                // we should be able to collapse this easily at render time
                            }
                        }
                    }

                }
            }

            // if this is the active story, trigger a re-draw
            if (this._active) {
                this._activePage = this.renderPage(undefined, "active");
                this._session.publish("commands.update");
            } else {
                if (this.changeNotifier) {
                    var noteId = this.changeNotifier.register(this, undefined, "Contents Changed!", undefined);
                    this._notifications.change.push(noteId);
                }
            }

            // otherwise, inform the change notifier
    },
        // 24May14 - new: Now a player renders a page
        // Players track each page they've rendered
        // they also listen to story.changed events and tell their pages when they've changed
        renderPage: function(pageId, state, forceResize) {
            pageId = pageId || this._currentPageId;

            var page = this._pages[pageId];
            if (!page) {
                var model = this.getPage(pageId);
                var before;
                if(state==="previous") {
                    before = 0; // always render "previous" nodes at the start
                }
                this._pages[pageId] = page = new Page(model, this, this.domRoot, before);
                page.hide();
            }

            //var changes = this._remoteChanges;
            //var pageDeltas;
            //if (changes[pageId] && changes[pageId]/*.page*/) {
            //	pageDeltas = changes[pageId]/*.page*/;
            //	changes[pageId] = undefined;
            //}

            var deltas = this._remoteChanges[pageId];
            //page._updatePosition(true);
            page.render(deltas, forceResize);

            if (state == "active" && (deltas && !deltas.link)) {
                this._remoteChanges[pageId] = undefined;
                delete this._remoteChanges[pageId];
            }

            if (state) {
                var pageState = this._pageState;
                if (!pageState) {
                    pageState = this._pageState = {};
                }
//                if (pageState[state]) {
//                    pageState[state].setState();
//                }
                // If this page has already been assigned a state, reset it
                if (page._currentState) {
                    pageState[page._currentState] = undefined;
                }

                // Set the new page state (and index it)
                pageState[state] = page;
                page.setState(state);
            }
            page.show();
            return page;
        },
        updateIds: function(newIds) {
            // update any internal references to the old and new id
            // also update the page
            var state = this._state;
            for (var oldId in newIds) {
                var page = this._pages[oldId];
                var newId = newIds[oldId];
                if (page) {
                    page.setId(newId);

                    var pageData = this._pages[oldId];
                    // probably need to be smarter about current page tracking, less reliant on IDs
                    if (this._currentPageId === oldId) {
                        this._currentPageId = newId;
                    }
                    this._pages[newId] = pageData;
                    this._pages[oldId] = undefined;
                    delete this._pages[oldId];
                    
                    
                }
                if (state[oldId]) {
                    state[newId] = state[oldId];
                    state[oldId] = undefined;
                    delete state[oldId];
                }
            }
            // Upadte any pervious/next state pointers
            for (var p in this._pages) {
                // update state previous/next. Is this a "global" scan?
                var pageState = state[p];
                if(pageState) {
                    for(var k in pageState){
                        var id = pageState[k];
                        if(newIds[id]) {
                            pageState[k]=newIds[id];
                        }
                    }
                }
                // and trigger a general update in the page
                this._pages[p].updateIds(newIds);
            }
            this.story.updateIds(newIds);
        },
        // notification that this player has disconnected
        onDisconnect: function() {
            console.log(this.id + " disconnected!");
        },
        hasChanges: function(){
            var deltas = this._localChanges;
            return Object.keys(deltas).length>1;
        }


    };

    return Player;

});