// A class which holds the details of any particular story
// Load a JSON and build it into this object
// Each story instance has live pagination
// Save a pages array, containing each page, id, plus the previous/next links. 
// Note that the "previous" page is ALWAYS contextual. Does that imply each player has a seperate Story instance?
// Yeah, I think so. 
// Be consistent with APIs. Do APIs return an index, or a page definition?
define(["client/util/keys"], function(keys) {

    var idx = 1;
    var contentId = 1;

    var Story = function(rawJson, session) {
        this.state = {}; // track state variables (arbitrary)
        this._pages = {};
        this._titleIndex={}; // index of page titles to page ids
        this._links = {};
        this._ids = {};

        // load the current page
        this.parseDefinition(rawJson);

        window.story = this;
    };

    Story.prototype = {
        // make sure the central story instance is notified of any changes
        // since the story is the central pool of knowledge, it must track state too
        setSession: function(session) {
            session.on("story.changed", this.onStoryChanged.bind(this));
            //session.on("story.link.validated", this.onLinkValidated.bind(this));
            // session.on("story.link.invalidated", this.onStoryChanged.bind(this));
            //session.on("player.freestyle", this.onFreestyleChanged.bind(this));
            
            session.on("player.pageComplete", this.onPageComplete.bind(this));
        },
        // page IDS need to be unique. So I really need some kind of 
        // hash based on the story title (or id), page title, and a random number
        _generateId: function(){
             var id = "@" + (idx++);
            while (this._ids[id]) {
                id = "@" + (idx++);
            }
            return id;
        },
        _generatePageId: function() {
           return this._generatePageId();
        },
        parseDefinition: function(data) {
            var pages = data.pages, page;

            var root = pages[0]._id; 	// the home page will be the first page ...
            while (pages[0]) {
                page = pages.shift();

                // hack for mongo... don't serialize this!
                page.id = page._id;

                this._pages[page.id] = page;
                if(page.title) {
                    this._titleIndex[page.title] = page.id;
                }
                if (page.home) {		// unless a flag tells us otherwise
                    root = page.id;
                }

                if (page.next) {
                    for (var i = 0; i < page.next.length; i++) {
                        var n = page.next[i];
                        if (!n.id) {
                            n.id = n._id || window.generateLinkId();
                        }
                        this._links[n.id] = n;
                    }
                }

                if (page.contents) {
                    for (var j = 0; j < page.contents.length; j++) {
                        var c = page.contents[j];
                        if (!c.id) {
                            c.id = c._id || contentId++;
                        }
                    }
                }
                
                if(!page.responses) {
                    page.responses={};
                }
            }
            this._rootPage = root;
            this.title = data.title;
            this.description = data.description;
            this.contentType = data.contentType;
            this.isComplete = data.isComplete;
            this.isPublished = data.isPublished;
            this.id = data._id;
        },
        getFirstPageId: function() {
            return this._rootPage;
        },
        getName: function() {
            return this.name;
        },
        getDescription: function() {
            return this.description;
        },
        hasNextPage: function(pageId) {
            var page = this.getPage(pageId);
            if (page && page.next)
                return true;
        },
        // soft look ahead/back
        // NOTE: does this returns pages, or link descriptors?
        // 		 It must return pages. Links must be handled seperately.
        getNextPages: function(pageId) {
            var page = this.getPage(pageId);
            var next = page.next;
            if (next) {
                for (var i = 0; i < next.length; i++) {
                    var n = next[i];
                    if (typeof n == "string") {
                        next.splice(i, 1, this.getPage(n));
                    }
                }

                return next;
            }
        },
        hasPreviousPage: function() {
            var page = this.currentPage;
            if (page && page.previous)
                return true;
        },
        // always return a single page model object. Or null.
        // bear in mind that a previous migth be a string, rather than a model object.
        // I think.
        // Note that this only applies if there are specially defiend previous pages, like "back to stoer"
        getPreviousPages: function(pageId) {
            var page = this.getPage(pageId);
            var prev = page.previous;
            if (prev) {
                if (typeof prev == "string") {
                    return this.getPage[prev];
                }
                else {
                    return prev;
                }
            }
        },
        serialize: function() {
            // save and export to JSON
        },
        setState: function(field, value) {

        },
        getState: function(field) {
        },
        // Note: clone all pages before returning them to allow local changes
        // 			from players
        getPage: function(pageId) {
            if (pageId) {
                var page = this._pages[pageId];
                return $.extend({}, page);
            }
        },
        publishPage: function(pageId) {
            var page = this._pages[pageId];
            // what if this is a new page? Might have a problem here
            if (page) {
                page.status = "shared";
            }

        },
        unpublishPage: function(pageId) {
            var page = this._pages[pageId];
            if (page) {
                page.status = "unshared";
            }
        },
        //// Register a Player to listen to changes to the story
        //addPlayer: function(player){
        //	var players = this._players;
        //	for(var i=0;i<players.length;i++) {
        //		if(players.id==player.id) {
        //			return;
        //		}
        //	}
        //	players.push(player);
        //},
        //
        //removePlayer: function(player){
        //	var players = this._players;
        //	for(var i=0;i<players.length;i++) {
        //		if(players.id==player.id) {
        //			players.splice(i,1);
        //			return;
        //		}
        //	}
        //},

        // add a number of changes, to page content and indeed new pages,
        // in a single pass.
        // Might need to trigger multiple changes for correct update notifications.
        addDeltas: function(deltas) {
            var pageStore = this._pages;
            for (var pageId in deltas) {
                var delta = deltas[pageId];

                var page = pageStore[pageId];
                if (!page) {
                    this.addPage(delta, pageId);
                    continue;
                }
                for (var p in delta) {
                    var value = delta[p];
                    if (p == keys.content.CONTENT) {
                        this.addContent(pageId, value);
                    } else if (p == keys.content.LINK) {
                        this.addLinks(pageId, value);
                    }
                    else {
                        page[p] = value;
                    }
                }
            }

            // publish an event with these changes
            // this must be the same event as we'd receive on the server
        },
        updateIds: function(newIds) {
            var titleIdx = this._titleIndex,
                pages = this._pages,
                links = this._links;
        
            // ugly as sin.. better way?
            for (var oldId in newIds) {
                var newId = newIds[oldId];
                if (pages[oldId]) {
                    pages[newId] = pages[oldId];
                    pages[oldId] = undefined;
                    delete pages[oldId];
                }
                if (links[oldId]) {
                    links[newId] = links[oldId];
                    links[oldId] = undefined;
                    delete links[oldId];
                }
                
                for(var pid in titleIdx) {
                    if(titleIdx[pid]==oldId) {
                        titleIdx[pid]=newId;
                        break;
                    }
                }
            }
        },
        addContent: function(pageId, contents) {
            var page = this._pages[pageId]; 
            if (page) {
                if (!contents.concat && contents.splice) {
                    contents = [contents];
                }

                if (!page.contents) {
                    page.contents = [];
                }
                for (var i = 0; i < contents.length; i++) {
                    page.contents.push(contents[i]);
                }
            }

            this.notifyChange(keys.changes.ADD, keys.content.CONTENT, pageId);
        },
        editPage: function(pageId, deltas) {
            console.log("Story :: accepting page changes to "+pageId);
            var model = this._pages[pageId];
            if (!model) {
                return this.addPage(deltas, pageId); // dubious
            } else {
                for (var d in deltas) {
                    model[d] = deltas[d];
                }
                this.notifyChange(keys.changes.EDIT, keys.content.PAGE, pageId);
            }
        },
        // add a new page to the story
        addPage: function(pageModel, id) {
            console.log("Story :: adding new page")
            console.log(pageModel);
            if (!pageModel) {
                pageModel = {
                    id: id || this._generatePageId(),
                    next: [],
                    previous: []
                };
            }
            if (!pageModel.contents) {
                pageModel.contents = [];
            }
            if (!pageModel.next) {
                pageModel.next = [];
            } else {
                for(var i=0;i<pageModel.next.length;i++) {
                    // register any new page links
                    var l = pageModel.next[i];
                    console.log("*** registering new link "+l.id);
                    this._links[l.id] = l;
                }
            }
            pageModel.id = id;
            
            if(pageModel.title) {
                this._titleIndex[pageModel.title] = id;
            }
            if(!pageModel.responses) {
                pageModel.responses={};
            }

            pageModel = $.extend({}, pageModel);

            this._pages[pageModel.id] = pageModel;
//                    var count = pageModel.number = Object.keys(this._pages).length;
//                    pageModel.title="Page-"+count;
//                    console.log(pageModel);
            this.notifyChange(keys.changes.ADD, keys.content.PAGE, pageModel.id);
            return pageModel;
        },
        addLinks: function(pageId, newLink) {
            var links = newLink.concat && newLink.splice ? newLink : [newLink];

            var currentLinks = this.getNextPages(pageId);
            if(!currentLinks) {
                var page = this._pages[pageId];
                currentLinks=page.next=[];
            }

            // TODO - make this more efficient with the links index...
            var hasAdded, hasModified;
            for (var i = 0; i < links.length; i++) {
                var link = links[i], added = false;
                //if (currentLinks) {
                    for (var i = 0; i < currentLinks.length; i++) {
                        if (currentLinks[i].id == link.id) {
                            var current = currentLinks[i];
                            for(var p in link) {
                                if(p!=="id") {
                                    if(typeof link[p]==undefined && current.hasOwnProperty(p)) {
                                        current[p]=undefined;
                                        delete current[p];
                                    } else {
                                        current[p]=link[p];
                                    }
                                }
                            }
                            hasModified = true;
                            added = true;
                            break;
                        }
                    }
                    if (!added) {
                        hasAdded = true;
                        currentLinks.push(link);
                        console.log("** added new link "+link.id)
                        this._links[link.id] = link;
                    }
//                } else {
//                    // push into the page
//                }
            }
            if (!hasAdded) {
                this.notifyChange(keys.changes.ADD, keys.content.LINK, pageId);
            }
            if (hasModified) {
                this.notifyChange(keys.changes.EDIT, keys.content.LINK, pageId);
            }

      },
        // Change has:
        //	 * a content type (page/link/content),
        //	 * a change type (add/edit/remove)
        //	 * a target Id (the thing changed) 
        //	 * optionally a custom message
        // TODO - emit a story.changed event (probably via the controller)
        notifyChange: function(changeType, contentType, targetId, message) {
            // tell all players that the story state has changed
            console.log("Story changed!!");


            //var players = this._players;
            //for(var i=0;i<players.length;i++) {
            //	players[i].onStoryChanged(changeType, contentType, targetId, message);
            //}
        },
        onStoryChanged: function(evt) {
            console.log("Story changed");
            console.log(evt);

            var deltas = evt.deltas;
            this.addDeltas(deltas);
        },
//        // respond to a freestyle event change by updating the local story instance directly
//        onFreestyleChanged: function(evt) {
//            var linkId = evt.linkId;
//
//            var link = this._links[linkId];
//            if (link) {
//                console.log("Story :: updating link freestyle:");
//                // create/read the input property
//                if (!link.input) {
//                    link.input = {};
//                }
//
//                // write the player's input
//                link.input[evt.playerId] = evt.input;
//            }
//
//        },
        
        // convert a page title into a page id
        getPageId: function(titleOrId){
            if(this._pages[titleOrId]) {
                return titleOrId;
            } 
            
            return this._titleIndex[titleOrId];
        },
        
        // note: must return an error if the title isn't unique
        updatePageTitle: function(oldTitle, newTitle){
            var idx = this._titleIndex;
            for(var pid in idx) {
                if(pid == oldTitle) {
                    var id = idx[pid];
                    idx[oldTitle] = undefined;
                    delete idx[oldTitle];
                    idx[newTitle] = id;
                    return true;
                }
            }
        },
        
        onPageComplete: function(evt){
            console.log(evt);
            this.addResponse(evt); //evt.pageId, evt.playerId, evt.linkId, evt.message);
        },
        
        addResponse: function(responseData){
            var pageId = responseData.pageId,
                playerId = responseData.playerId;
        
            var page = this._pages[pageId];
            if(page) {
                // cheap...
                if(!page.responses) {
                    page.responses={};
                }
                // very cheap...
                var cache = page.responses[playerId];
                if(!cache) {
                     cache = page.responses[playerId]={};
                }
                cache.target = responseData.linkId;
                if(responseData.message) {
                   cache.message=responseData.message;
                }
                if(responseData.timestamp) {
                    cache.timestamp=responseData.timestamp;
                }
            }
        },
        getResponses: function(pageId){
            var page = this._pages[pageId];
            if(page) {
                return page.responses;
            }
        },
        hasResponse:function(pageId, playerId) {
            var page = this._pages[pageId];
            if(page) {
                if(page.responses[playerId]) return true;
            }
        },
        
        getLink: function(linkId){
            return this._links[linkId];
        },
        
        onLinkValidated: function(evt) {
            var linkId = evt.linkId;
            console.log("Link "+linkId+" validated! ");
            // update subscribers/watchers
            if(this._links[linkId]) {
                return this._links[linkId].isValid = true;
            }
            
        },
        
        isLinkValid: function(linkId){
            if(this._links[linkId]) {
                return this._links[linkId].isValid;
            }
            return false;
        },
        
        // load changes to the story...
        update: function(deltas){
            for(var p in deltas) {
                this[p] = deltas[p];
            }
        }


    };

    return Story;

});