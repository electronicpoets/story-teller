// render "pages" as paragraphs of text, with scroll-up scroll-down commands
define(["client/story/widgets/Content"], function(Content) {

    var Prose = function(parentNode, model) {
          Content.apply(this, arguments);
          this._edit=false;
    };

    Prose.prototype = Object.create(Content.prototype);

    Prose.prototype.init = function(parentNode) {
        Content.prototype.init.call(this, parentNode);
        if (this._model.style) {
            this.setStyle(this._model.style);
        } /*if(this._model.size) {
            this.setSize(this._model.soze);
        }*/
        
        // on double-click, we go into "edit" mode
        // until when...?
        
        this.domNode.className += " prose";
        $(this.domNode.firstChild).addClass("show");
    };

    // quick fix to kil draggable behaviour
    Prose.prototype._makeDraggable = function(){};
    
    Prose.prototype._evt = function(event){
        if(event=="dblclick") {
            this.acceptInput();
        } else if(!this._edit) {
            Content.prototype._evt.apply(this, arguments);
        }
    };

    Prose.prototype.setSize = function(newSize) {
        // prose size is always page width and font-size controlled
    };

    Prose.prototype.update = function(deltas) {
        Content.prototype.update.call(this, deltas);
        if (deltas.style) {
            this.setStyle(deltas.style);
        }
        if (deltas.content) {
            this.contentNode.innerHTML = deltas.content;
        }
    };

    // internal event to fire when the content changes
    Prose.prototype._onChange = function(props) {
        var newContent = props.text;
        if (newContent && newContent != this._contentBuffer) {
            this._contentBuffer = newContent;
            if (this._changeHandler) {
                this._changeHandler({id: this._model.id, content: newContent});
            }
        }
        // invoke super...
        Content.prototype._onChange.apply(this, arguments);
    };

    Prose.prototype.generateDom = function() {
        var content = this._model.content;

        this._contentBuffer = this.contentNode.innerHTML = content||"Here is a prose widget";
        // hook a change event to your content
        // whenever the text changes, publish an event
        // Bit of a hacky approach
        $(this.contentNode).on('blur keyup paste input', function() {
            var newContent = this.contentNode.innerHTML;
            this._onChange({content: newContent});
        }.bind(this));
    };

    Prose.prototype.initCommands = function() {
        this._createCommand("Remove", this.remove, undefined, "Remove from the page");
        
        var model = this._model;
        this._createCommand("style", function(evt) {
            var select = evt.target;
            var item = select.item(select.selectedIndex);
            var value = item.getAttribute("value");
            model.style=value;
            this.setStyle(value);
            this._onChange({style: value});
        }, function() { 
            return model.style||"<no style>" 
        }, 
        "Set the text style",["<no style>", "code", "comic", "italic"]);
    };

    Prose.prototype.enableEdit = function(changeHandler) {
        Content.prototype.enableEdit.call(this, changeHandler);
        this.contentNode.setAttribute("contenteditable", true);
    };

    Prose.prototype.disableEdit = function() {
        Content.prototype.disableEdit.call(this);
        this.contentNode.removeAttribute("contenteditable");
    };

    Prose.prototype.setStyle = function(style) {
        if (this._styleClass) {
            $(this.contentNode).removeClass(this._styleClass);
        }
        this._styleClass = style;
        $(this.contentNode).addClass(style);
        //this.contentNode.className="content text "+style;
    };

    Prose.prototype.positionDom = function(positionData, sizeData) {
       // dom positioning is "natural"
    };

    // on edit, update the bound model instance

    return Prose;

});