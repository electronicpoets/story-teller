// Render a single image, with a number of edit-time behaviours
define(["client/story/widgets/Content", "util/ImageBank"], function(Content, imageBank) {

    // content of a picture is a URL to the picture source
    // display a loading gif while loading
    var Picture = function(parentNode, model) {
          Content.apply(this, arguments);
    };

    Picture.prototype = Object.create(Content.prototype);

    Picture.prototype.init = function(parentNode) {
        Content.prototype.init.call(this, parentNode);
        this.dragHandle = this.contentNode;
        this.domNode.className += " picture";
    };
    
    Picture.prototype.generateDom = function() {
        var model = this._model.content;
        if(!this._isLoading) {
            this._isLoading=true;
            $(this.domNode).addClass("loading");
            imageBank.get(model, function(modelData){
                this._isLoading=false;
                $(this.domNode).removeClass("loading"); 
                var wrapper = document.createElement("div");
                wrapper.className = "picture wrapper"; // + loading
                var img = this.imageNode;
                if (!img) {
                    img = this.imageNode = document.createElement("img");
                    img.setAttribute("draggable", false);
                }
                img.setAttribute("src", modelData); 
                wrapper.appendChild(img);
                this.contentNode.appendChild(wrapper);
            }.bind(this));
        }
    };

    Picture.prototype.getSize = function() {
        var page = this.domNode.parentNode,
                pageWidth = page.offsetWidth,
                pageHeight = page.offsetHeight;

        var width = (this.imageNode.offsetWidth / pageWidth) * 100,
                height = (this.imageNode.offsetHeight / pageHeight) * 100;

        return {width: width, height: height};
    };

    Picture.prototype.setSize = function() {
        var data = this._model.size;
        var page = this.domNode.parentNode;
        //var width=0, height=0;
        var width, height;
        if (!page.offsetWidth||!this.imageNode) {
            clearTimeout(this._loadTimeout);
            this._loadTimeout=setTimeout(this.setSize.bind(this), 500); // hack to work around page loading
            return;
        }
        //console.log("**** SETTING PICTURE SIZE***");
        if (data) {
            if(this._loadTimeout) {
                clearTimeout(this._loadTimeout);
                this._loadTimeout = undefined;
            }
            var pageWidth = page.offsetWidth,
                    pageHeight = page.offsetHeight,
                    maxWidth = 0.8 * pageWidth,
                    maxHeight = 0.8 * pageHeight;
            console.log("> "+pageWidth);
            console.log(page)
            var img = this.imageNode;
            // a "biggest" constraint will restrict the largest edge
            if(data.biggest) {
                var size = data.biggest;
                if(this._model.ratio) {
                    var bits = this._model.ratio.split(":");
                    var w = parseFloat(bits.shift());
                    var h = parseFloat(bits.shift());
                    if(w > h ) {
                        width = size;
                        height = (h/w)*size;
                    } else {
                        height = size;
                        width = (w/h)*size;
                    }
                }
                // now set the size as a constant height
                if(img) {
                    img.setAttribute("width", Math.max(50, (width||size)));
                    img.setAttribute("height", Math.max(50, (height||size)));
               }
            } else {
                // otherwise, set the value as a percentage. Probably ought to be smarter here
                if (data.width)
                    width = Math.min((data.width / 100) * pageWidth, maxWidth);
                if (data.height)
                    height = Math.min((data.height / 100) * pageHeight, maxHeight);
                
                if(img) {
                    if (!isNaN(width))
                        //img.setAttribute("width", Math.max(50, width));
                        img.style.width = Math.max(50, width)+"px";
                    if (!isNaN(height)) {
                        //console.log(" setting height to "+height);
                        //img.setAttribute("height", Math.max(50, height));
                        img.style.height = Math.max(50, height)+"px";
                    }
                }
            }
             $(this.domNode.firstChild).addClass("show");
            console.log("Setting picture size\n Width: "+(data.width / 100)+"% = "+width+"px"+
            								"\n Height: "+(data.height / 100)+"% = "+height+"px");

        }
    };

    Picture.prototype.enableEdit = function() {
        Content.prototype.enableEdit.apply(this, arguments);
        this.enableResize();
    };
    
    Picture.prototype.disableEdit = function() {
        Content.prototype.disableEdit.call(this);
        this.disableResize();
    };
    
    Picture.prototype.enableResize=function(){
         // Use JQuery to make the picture resizable
        var img = this.imageNode;
        if(img) {
            img._rsz = $(img).resizable({
                handles: "n, e, s, w",
                aspectRatio: true,
                helper: "resizable-helper",
                stop: function() {
                    //console.log("**Resize Complete**");
                    var size = this.getSize();
                    this._onChange({size: size});
                }.bind(this)
            });
        }
    };
    
    Picture.prototype.disableResize = function(){
        var img = this.imageNode;
        if(img && img._rsz) {
            $(img).resizable("destroy");
            img._rsz=undefined;
        }
    }





    return Picture;

});