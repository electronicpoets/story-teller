define(["jquery", "/js/lib/jquery-ui-1.11.1/jquery-ui.js"], function($) {

    // don't really like declaring this in-line each time, but on the other
    // hand I'm not sure how else to set the default selection
    $.widget( "custom.stselect", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
          var li = $( "<li>");
          $(li).attr("data-value", item.label);
          var label = $( "<span>", {text:item.label, class:"label"}).appendTo(li);
          $("<span>", {class:"fi selector"}).appendTo(li);
          return li.appendTo( ul );
        }
    });

    var Content = function(parentNode, model, evt) {
        this._model = model;
        this._evts={};
        if(evt) {
            if(evt.click) {
                this._evts.click=evt.click;    
            }
            if(evt.dblclick) {
                this._evts.dblclick=evt.dblclick;    
            }
        }
        // optional args (support 0-constructor)
        this.init(parentNode);
    };

    Content.prototype = {
        init: function(parentNode) {
            if (parentNode) {
                this.domNode = document.createElement("div");
                this.domNode.className = "content-wrapper";

                $(this.domNode).mouseover(this.onMouseOver.bind(this));
                $(this.domNode).mouseout(this.onMouseOut.bind(this));

                this.contentNode = document.createElement("div");
                this.contentNode.className = "content";
                this.domNode.appendChild(this.contentNode);
                
                $(this.contentNode).click(function(){
                   this._evt("click");
                }.bind(this));
                
                $(this.contentNode).dblclick(function(){
                   this._evt("dblclick");
                }.bind(this));

                //this.initTitlebar(); // TODO - create the basic titlebar shape/size, but don't render
                //			it unless we need to. Or, only render if it's POSSIBLE to
                //  		go into edit mode in this session...

                parentNode.appendChild(this.domNode);
            }
        },
        
        onMouseOver: function(){
            if(this._outTimer) {
                clearTimeout(this._outTimer);
                this._outTimer=null;
            }
            if(this._editMode) {
                $(this.domNode).addClass("hover");
            }
        },
        
        onMouseOut: function(){
            if(this._editMode && !this._outTimer /*&& !this.hasFocus*/) {
                this._outTimer = setTimeout(function(){
                    $(this).removeClass("hover");
                }.bind(this.domNode), 150);
            }
        },
        
        _evt: function(event){
            var handler = this._evts[event];
            if(handler) {
                if(!this._edit) {
                    console.log(event);
                    handler(this._model, this.domNode);
                }
            }
        },
        
        render: function(isEditMode) {
            var dom = this.domNode, content = this._model.content;
            if (dom && content) {
                this.generateDom();
                this.setSize();
                this.positionDom();
                // if we're in edit mode, we need to auto-enable editing
                if (isEditMode) {
                    this.enableEdit();
                }

            }
        },
        // Run a live-update (passively) of this contnet
        // deltas is an object of k/v pairs, ie content: "This is the new content"
        // content types should produce an animation
        update: function(deltas) {
            var dom = this.domNode;
            $(dom).addClass("new");
            setTimeout(function() {
                $(dom).removeClass("new");
            }, 3000);

            if (deltas.position || deltas.size) {
                this.positionDom(deltas.position, deltas.size);
            }
        },
        generateDom: function() {

        },
        // positional information should be pretty standard
        // left / right / center
        // top / bottom / middle
        // size? 
        // Note that % based positions go from the content center, so
        // need to calculate content's height/width
        positionDom: function(positionData, sizeData) {
            if (!this.domNode.parentNode.offsetWidth) {
                // another hack to ensure the page is positioned before we set positions
                setTimeout(function() {
                    this.positionDom(positionData, sizeData);
                }.bind(this), 500);

                return;
            }

            positionData = positionData || this._model.position;
            sizeData = sizeData || this._model.size;

            var dom = this.domNode;
            // calculate the page width & height (including content halves)
            var pWidth = dom.parentNode.offsetWidth;
            var pHeight = dom.parentNode.offsetHeight;

            if (positionData) {
                // horizontal alignment
                var h = positionData.horizontal;
                if (h == "center") {
                    //this.domNode.className+=" center";
                    h = 50;
                }

                // if there's a width in %, subtract half the width
                if (sizeData && sizeData.width) {
                    h -= sizeData.width / 2;
                }

                this.domNode.style.left = h + "%";

                // vertical alignment
                var v = positionData.vertical;
                if (v == "middle") {
                    v = 50;
                }
                // load the height as a % from the model
                if (sizeData && sizeData.height) {
                    var ht = sizeData.height;

                    // and subtract half the height to align centrally
                    var offset = ht / 2;
                    v -= offset;
                }

                this.domNode.style.top = v + "%";
            }
        },
        // calculate the position of this content relative to the page
        // position from... center? top-left? Center, I think..
        calculatePosition: function() {
            var dom = this.domNode;
            // calculate the page width & height (including content halves)
            var contentCenter = dom.offsetLeft + Math.ceil(dom.offsetWidth / 2);
            var horizOffset = Math.floor((contentCenter / dom.parentNode.offsetWidth) * 100);
            //console.log("> content " + this._model._id + " left = " + horizOffset + "%");

            var contentMiddle = dom.offsetTop + Math.ceil(dom.offsetHeight / 2);
            var vertOffset = Math.floor((contentMiddle / dom.parentNode.offsetHeight) * 100);

            return {
                horizontal: horizOffset,
                vertical: vertOffset
            };
        },
        // more like an update size really...
        setSize: function() {
            var sizeData = this._model.size;
        },
        // enter edit-mode
        // allow application of styles, position data, attaches drag-drop
        enableEdit: function(changeHandlers) {
            if (!this._editMode) {
                this._editMode = true;
                if (changeHandlers) {
                    this._changeHandler = changeHandlers.edit;
                    this._deleteHandler = changeHandlers.delete;
                }
                this.initTitlebar();

                this.domNode.className += " edit";
                if ($) {
                    // if this has been aligned, we need to strip the existing alignment CSS 
                    var dom = this.domNode;
//					$(dom).removeClass("center");
//					$(dom).removeClass("middle");

                    this._makeDraggable();
                }
            }
        },
        _makeDraggable: function() {
            if (!this._draggable) {
                this._draggable = true;
                var handle = this.dragHandle||this.titlebar;
                var boundary = this.domNode.parentNode;
                var content = this.contentNode;
                var self = this;
                $(this.domNode).draggable({
                    handle: handle,
                    containment: boundary,
                    start: function(event, ui) {
                        // remove the restrictive CSS
                        $(this).removeClass("center");
                        $(this).removeClass("middle");
                    },
                    // trigger a positional re-calculation and update the model
                    stop: function() {
                        console.log(" -> end move");
                        self._move();
                    }

                });
            }
        },
        // publish notification that this content has been edited.
        // the active player needs to listen to this event
        _onChange: function(props) {
            if (props && this._changeHandler) {
                props.id = this._model.id;
                this._changeHandler(props);
            }
        },
        _destroyDraggable: function() {
            if (this._draggable) {
                $(this.domNode).draggable("destroy");
                this._draggable = undefined;
                delete this._draggable;
            }
        },
        disableEdit: function() {
            if (this._editMode) {
                this._editMode = undefined;
                this._changeHandler = undefined;
                this._deleteHandler = undefined;
                delete this._editMode;

                this.domNode.className = this.domNode.className.replace(" edit", "");
                this.destroyTitlebar();
                if ($) {
                    // remove the draggable behaviour
                    this._destroyDraggable();
                }
            }
        },
        initTitlebar: function() {
            var wrapper = document.createElement("div");
            wrapper.className = "titlebar-wrapper";

            var spacer = document.createElement("div");
            spacer.className = "spacer";
             wrapper.appendChild(spacer);

            var titlebar = this.titlebar = document.createElement("div");
            titlebar.className = "titlebar";
            // mimic a standard toolbar
            // need to make this easy really - implement an array of commands, and just render the commands
            // align pops up a 9*9 grid which aligns the widget
            // on click of "center", we'll snap to center. Later, we'll add the grid with smart stuff
            // on center, we must remove the draggable behaviour

            this.initCommands();

            wrapper.appendChild(titlebar);
            this.domNode.insertBefore(wrapper, this.domNode.firstChild);
        },
        initCommands: function() {
            this._createCommand("Center", this._center.bind(this), undefined, "Center on the page");
            this._createCommand("Remove", this.remove, undefined, "Remove from the page");
        },
        _center: function() {
//            //this._destroyDraggable();
//            $(this.domNode).addClass("middle center");
//            // clear all positional information
//            $(this.domNode).css({
//                left: "",
//                right: "",
//                top: "",
//                bottom: ""
//            });
            
            
            
            this._onChange({
                position: {
                    horizontal: "center",
                    vertical: "middle"
                }
            });
            this._model.position={
                horizontal: "center",
                vertical: "middle"
            };
            this.positionDom();
        },
        // calculate new screen position as a %
        _move: function() {
            var position = this.calculatePosition();
            this._onChange({position: position});
        },
        _createCommand: function(commandName, action, selection, tooltip, options) {
            var cmd;
            if (options) {
                // Multiple options, create a drop-down box
                cmd = document.createElement("select");
                for (var i = 0; i < options.length; i++) {
                    var child = document.createElement("option");
                    var x = options[i];
                    child.setAttribute("value", x.toLowerCase());
                    child.textContent = x;
                    cmd.appendChild(child);
                }
                
                this.titlebar.appendChild(cmd);
                $(cmd).stselect({ 
                    icons: { button: "fi "+commandName.toLowerCase() },
                    // Add special CSS stylign to the currently selected element. 
                    // This feels like WAY too much work
                    open: function(evt, ui){
                        var w = $(this).stselect("instance");
                        var items = w.menuItems;
                        var idx = w.focusIndex;
                        if(selection.apply) {
                            var sel = selection();
                            for(var i=0;i<items.length;i++) {
                                var item = items[i];
                                if(item.getAttribute("data-value") === sel) {
                                    $(item).addClass("selected");
                                    idx=i;
                                } else {
                                    $(item).removeClass("selected");
                                }
                            }
                        }
                        evt.target.selectedIndex=idx;
                    },
                    change: action.bind(this)
                });
            } else {
                // or just create a span
                cmd = document.createElement("span");
                this.titlebar.appendChild(cmd);
                cmd.className="fi "+commandName.toLowerCase();
                if (action) {
                    $(cmd).click(action.bind(this));
                    cmd.className += " link";
                }
                if(tooltip) {
                    cmd.title=tooltip;
                }
                //cmd.innerHTML = "[" + commandName + "]";
            }
        },
        
        remove: function() {
            this.destroy();

            // remove the model
            this._model.destroyed = true;

            if (this._deleteHandler) {
                this._deleteHandler(this._model);
            }
        },
        destroy: function() {
            // remove the render
            if (this.domNode) {
                this.domNode.parentNode.removeChild(this.domNode);
                this.domNode = undefined;
            }

        },
        destroyTitlebar: function() {
            if (this.domNode) {
                this.domNode.removeChild(this.domNode.firstChild);
            }
        },
        // allow the Content to be duplicated with the same state
        // Useful for palettes (and as a nice editor short-cut)
        clone: function() {
            var clone = $.extend({}, this._model);
            if (clone.size) {
                clone.size = undefined;
            }
            console.log(clone);
            return clone;
        },
        
//        makeDraggable: function() {
//            var drag = this.dragNode;
//            if(img) {
//                img._rsz = $(img).resizable({
//                    handles: "n, e, s, w",
//                    aspectRatio: true,
//                    helper: "resizable-helper",
//                    stop: function() {
//                        //console.log("**Resize Complete**");
//                        var size = this.getSize();
//                        this._onChange({size: size});
//                    }.bind(this)
//                });
//            }
//        }

    };

    return Content;

});
