// render "pages" as paragraphs of text, with scroll-up scroll-down commands
define(["client/story/widgets/Content"], function(Content) {

    var sizeMap={
        small: 0.03,
        medium: 0.045,
        large: 0.07
    };

    var Text = function(parentNode, model) {
          Content.apply(this, arguments);
          this._edit=false;
    };

    Text.prototype = Object.create(Content.prototype);

    Text.prototype.init = function(parentNode) {
        Content.prototype.init.call(this, parentNode);
        this.dragHandle=this.contentNode;
        if (this._model.style) {
            this.setStyle(this._model.style);
        } /*if(this._model.size) {
            this.setSize(this._model.soze);
        }*/
        
        // on double-click, we go into "edit" mode
        // until when...?
        
        this.domNode.className += " text";
        this.domNode.className += " top";
    };

    Text.prototype._evt = function(event){
        if(event=="dblclick") {
            this.acceptInput();
        } else if(!this._edit) {
            Content.prototype._evt.apply(this, arguments);
        }
    };
    
    // trigger double-click
    Text.prototype.acceptInput = function(){
        this._edit=true;
        //this.hasFocus=true;
        this._destroyDraggable();
        $(this.contentNode).attr("contenteditable", true)
            .focus()
            .addClass("edit")
            .on("blur", this.cancelInput.bind(this));
        };

    // trigger on blur or enter (seems to happen automatically)
    Text.prototype.cancelInput = function(){
        this._edit=false;
        //this.hasFocus=false;
        this._makeDraggable();
        $(this.contentNode).removeAttr("contenteditable")
            .removeClass("edit");
    };

    Text.prototype.setSize = function(newSize) {
        // calculate font size relative to the page, in pixels.
        // Base font size will be, say, 20% height, modified by a
        // user-set mutiplier
        if(newSize) {
            this._model.textSize = newSize;
        } else { 
            newSize = this._model.textSize;
        }
        
        var sizeRatio;
        if(typeof newSize=="string") {
            sizeRatio = sizeMap[newSize];
        } else if(!isNaN(newSize)) {
            sizeRatio = newSize;
        } else {
            sizeRatio = sizeMap.medium;
        }
        
        var page = this.domNode.parentNode,
                pageHeight = page.offsetHeight;

        if (pageHeight == 0) {
            setTimeout(this.setSize.bind(this), 100);
        } else {
            var baseHeight = Math.max(10, Math.floor(pageHeight * sizeRatio));
            //console.log("Setting text height to "+baseHeight)
            $(this.contentNode).css({
                fontSize: baseHeight + "px"
            });

            $(this.domNode.firstChild).addClass("show");
        }
    };

    Text.prototype.update = function(deltas) {
        Content.prototype.update.call(this, deltas);
        if (deltas.style) {
            this.setStyle(deltas.style);
        }
        if (deltas.content) {
            this.contentNode.innerHTML = deltas.content;
        }
    };

    // internal event to fire when the content changes
    Text.prototype._onChange = function(props) {
        var newContent = props.text;
        if (newContent && newContent != this._contentBuffer) {
            this._contentBuffer = newContent;
            if (this._changeHandler) {
                this._changeHandler({id: this._model.id, content: newContent});
            }
        }
        // invoke super...
        Content.prototype._onChange.apply(this, arguments);
    };

    Text.prototype.generateDom = function() {
        var content = this._model.content;

        this._contentBuffer = this.contentNode.innerHTML = content;
        // hook a change event to your content
        // whenever the text changes, publish an event
        // Bit of a hacky approach
        $(this.contentNode).on('blur keyup paste input', function() {
            var newContent = this.contentNode.innerHTML;
            this._onChange({content: newContent});
        }.bind(this));
    };

    Text.prototype.initCommands = function() {
        var model = this._model;
        this._createCommand("size", function(evt) {
            var select = evt.target;
            var item = select.item(select.selectedIndex);
            var value = item.getAttribute("value");
            model.size=value;
            this.setSize(value);
            this._onChange({textSize: value});
        }, function(){ return model.textSize||"medium" }, "Set the text size",["small", "medium", "large"]);
        
        this._createCommand("style", function(evt) {
            var select = evt.target;
            var item = select.item(select.selectedIndex);
            var value = item.getAttribute("value");
            model.style=value;
            this.setStyle(value);
            this._onChange({style: value});
        }, function() { return model.style||"<no style>" }, "Set the text style",["<no style>", "blue", "yellow", "white", "black"]);

        Content.prototype.initCommands.apply(this, arguments);
    };

//    Text.prototype.enableEdit = function(changeHandler) {
//        Content.prototype.enableEdit.call(this, changeHandler);
//        this.contentNode.setAttribute("contenteditable", true);
//    };
//
//    Text.prototype.disableEdit = function() {
//        Content.prototype.disableEdit.call(this);
//        this.contentNode.removeAttribute("contenteditable");
//    };

    Text.prototype.setStyle = function(style) {
        if (this._styleClass) {
            $(this.contentNode).removeClass(this._styleClass);
        }
        this._styleClass = style;
        $(this.contentNode).addClass(style);
        //this.contentNode.className="content text "+style;
    };

    Text.prototype.positionDom = function(positionData, sizeData) {
        if (!this.domNode.parentNode.offsetWidth) {
            // another hack to ensure the page is positioned before we set positions
            setTimeout(function() {
                this.positionDom(positionData, sizeData);
            }.bind(this), 100);

            return;
        }

        positionData = positionData || this._model.position;

        var dom = this.domNode;
        // calculate the page width & height (including content halves)
        var pWidth = dom.parentNode.offsetWidth;
        var pHeight = dom.parentNode.offsetHeight;

        // calculate the text's size as a % of the page
        var tWidth = Math.floor((dom.offsetWidth / pWidth) * 100),
                tHeight = Math.floor((dom.offsetHeight / pHeight) * 100);


        if (positionData) {
            // horizontal alignment
            var h = positionData.horizontal;
            if (h == "center") {
                h = 50;
            }
            // if there's a width in %, subtract half the width
            if (tWidth) {
                h -= tWidth / 2;
            }
            this.domNode.style.left = h + "%";

            // vertical alignment
            var v = positionData.vertical;
            if (v == "middle") {
                v = 50;
            }
            // load the height as a % from the model
            if (tHeight) {
                v -= tHeight / 2;
            }

            this.domNode.style.top = v + "%";
        }
    };

    // on edit, update the bound model instance

    return Text;

});