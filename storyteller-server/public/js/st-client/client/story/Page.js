// Represents a single page of a story
// Has a reference to a model object and a player
// Mediates between a player's representation of a model and the live dom
// Updated 4October14 - moving almost alldom management into the renderer
define(["client/render/ComicRenderer", "client/render/ProseRenderer",
        "client/app/widgets/Button", "client/app/widgets/ForwardButton"], 
        function(ComicRenderer, ProseRenderer,Button, ForwardButton) {

    var Page = function(model, player, parentNode, beforeNode) {
        this._model = model;
        
        this._responses={};
        this._renderState = {}; // index of which content has been rendered
        this._deltas = {};
        this._loadResponses(this._model.responses);

        // TODO - load a renderer according to page type
        var story = player.story; // cheap
        this._renderer = story.contentType=="Prose" ? new ProseRenderer(this) : new ComicRenderer(this);
        this.nodes = {
            parent: parentNode,
            beforeRef: beforeNode
        };
        this._player = player;
    };
    
//    var backgrounds=[{label:"<none>", value: undefined}];
//    $.get("/data/backgrounds", function(data){
//        console.log("Loaded Backgrounds!");
//        console.log(data);
//        for(var i=0;i<data.length;i++) {
//            backgrounds.push({label:data[i].name, value: data[i].key});
//        }
//    });

    Page.prototype = {
        panelMargin: 60,
        /// TODO - fill all this out
        // add a delta to the data of this page
        // will be an object with properties, including content and links
        // deltas are saved into an array and "squashed" at render time
        addDelta: function(delta) {

        },
        // share and publish an event
        share: function() {

        },
        unshare: function() {

        },
        initDom: function() {
            this.inited = true;
            this.nodes.root = this._renderer.initDom(this.nodes.parent, this._model, this.nodes.beforeRef);
        },
        
         setState: function(state) {
             //var title = this._model.title;
             //console.log("-> "+title+" Setting state to "+state)
             if(state!=="active" && this._editMode) {
                 this.edit();
             }
             this._renderer.setState(state);
         },
        _loadResponses: function(data){
            if(data) {
                var resp=this._responses;
                for(var playerId in data) {
                    resp[playerId] = data[playerId];
                }
            }
        },
        addResponse: function(playerId, linkId){
            this._responses[playerId] = linkId;
        },
        getResponses: function(){
            return this._responses;
        },
        
        hide: function() {
            var dom = this.nodes.root;
            if (dom) {
                $(dom).css({visibility: "hidden"});
                $(dom).removeClass("animate");
            }
        },
        show: function() {
            var dom = this.nodes.root;
            if (dom) {
                // note: if a page was hidden and is still animating
                $(dom).css({visibility: "visible"});
                // hack
                setTimeout(function() {
                    $(dom).addClass("animate");
                }, 250);
            }
        },
        toggleEdit: function(){
            // todo,clean this up
            window.edit(this._model.id);
        },
        // switch all widgets in the page into edit mode
        edit: function(enter, modelContent) {
            if (this._editMode != enter) {
                this._editMode = enter;
                this.render(modelContent);
                if(this._renderer.nextTray) {
                    this._renderer.nextTray.edit(enter);    
                }
            }
        },
        // accept any changed page state
        render: function(deltas, forceResize) {
            var content = this._player.getPageContent(this._model.id);
            if (this._renderer) {
                if (!this.inited) {
                    this.initDom();
                } else if(forceResize) {
                    this._renderer._updatePosition(true);
                }

                if (deltas) {
                    if (deltas.background || deltas.fadeBg) {
                        this.setBackground(deltas.background, deltas.fadeBg);

                        deltas._background = undefined;
                        delete deltas._background;
                    }
                }

                if (this._player.isPagePublished(this._model.id)) {
                    //if(this._model.status!="shared") {
                    this.hideUnpublished();
                } else {
                    this.showUnpublished();
                }

                //// flag any updated content as being "new"
                //// new content will show an animation when it first appears. Doesn't occur if a page
                //// is being rendered for the first time
                if (deltas && deltas.contents) {
                    var dindex = {};
                    for (var i = 0; i < deltas.contents.length; i++) {
                        var d = deltas.contents[i];
                        dindex[d.id] = d;
                    }

                    if (content) {
                        for (var j = 0; j < content.length; j++) {
                            var c = content[j];
                            var delta = dindex[c.id];
                            if (delta) {
                                c._delta = delta;
                            }
                        }
                    }
                }

                var handlers;
                var player = this._player;
                if (this._editMode) {
                    handlers = {
                        edit: function(model) {
                            // content has been changed - inform... the owning player? Yes...
                            this.updateContent(model.id, model);
                        }.bind(player),
                        delete: function(model) {
                            this.removeContent(model);
                        }.bind(player)
                    };
                }

                this.controls = this._renderer.render(this._renderState, content, handlers, forceResize);
                this.drawTitle();
            }
        },

        showUnpublished: function() {
            this._renderer.showUnpublished();
        },
        hideUnpublished: function() {
            this._renderer.hideUnpublished();
        },
        setTitle: function() {

        },
        setId: function(newId) {
            this._model.id = newId;
        },
        // Updated 26Aug14 to take content ID, not a path. This might break stuff...
        setBackground: function(id, fade) {
            this._renderer.setBackground(id, fade);
         },
        // return all the Content for the page
        getContents: function() {
            return this._model.contents;
        },
        // indicate that a particular piece of Content is currently rendered on-screen
        markRendered: function() {

        },
        // return the model which represents this page
        getModel: function() {
            return this._model;
        },
        // remove all rendererings
        destroy: function() {
            var controls = this.controls;
            while (controls[0]) {
                controls.shift().destroy();
            }
        },
        updateIds: function(newIds) {
            var st = this._renderState;


            if (this._idNode) {
                this._idNode.updateContent(this._model.id);
            }
            for (var oldId in newIds) {
                var newId = newIds[oldId];
                if (st[oldId]) {
                    st[newId] = st[oldId];
                    st[newId]._model.id = newId;
                    st[oldId] = undefined;
                    delete st[oldId];
                }
            }
        },
        
        drawTitle: function(){
            this._renderer.drawTitle(this._model);
        },
        
        showBgPopup: function(target){
            this._renderer.showBgPopup(target);
        },
        
         updateCommands: function() {
            var buttons = this._prevButtons||[];
            // be smarter about this - this function will be called a lot
            // Ok, now this is only deleting previous buttons
            while (buttons[0]) {
                buttons.shift().destroy();
            }

            var player = this._player;
            var links = player.getNextLinks();
            //var group = this.rightTray;
            var group = this._renderer.nextTray;
            // if in edit mode make sure the "next" link has exactly one "empty" button
            var ids={};
            if(this._editMode) {
                var hasNull;
                for(var i=0;i<links.length;i++) {
                    var l = links[i];
                    if(!l.title) {
                        if(!hasNull) {
                            hasNull=true;
                            if(l.destroyed) {
                                l.destroyed=undefined;
                                delete l.destroyed;
                            }
                        } else {
                            links.splice(i,1);
                            i--;
                            continue;
                        }
                    }
                    ids[l.id]=true;
                }
                if(!hasNull) {
                    var newId = player.story._generateId();
                    links.push({ id: newId, title:"" });
                    ids[newId]=true;
                }
            }
            group.prune(ids);
            this._generatePageCommands(group, links, "Next", player, true);
            var left = this._renderer.previousTray;
            if(left) {
                this._prevButtons = this._generatePageCommands(left, player.getPreviousLinks(), "Previous", player);
            }
        },
        
        // return the Id of the previous page
        getPrevious: function(){
            if(this._model.previous) {
                return this._model.previous[0];
            }
        },
        
          // TODO - move this logic into the page instance? getPageCommands() ?
        _generatePageCommands: function(tray, links, defaultTitle, player, isForward) {
            var buttons = [];
            var player = this._player;
            if (links) {
                if (!links.splice)
                    links = [links]; // short-term hack

                for (var i = 0; i < links.length; i++) {
                    var link = links[i];
                    if (link.destroyed || (link.id&&tray.has(link.id)) ) {
                        continue;
                    }

                    var args = {};
                    args.model = link;
                    if (!isForward && !link.title) {
                        args.title = defaultTitle;
                    }

                    // tmp, lazy
                    var controller = window.controller;
                    var clickHandler;
                    if(isForward) {
                        clickHandler = controller._clickHandler.bind(controller);
                    }
                    else  {
                         clickHandler=function(model) {
                            if(model.target)  {
                                 this.page(model.target, model.id); // don't like this isForward parameter...	
                            }
                         }.bind(controller);
                    }
//                    var clickHandler=function(){};

                    var button = this.generateButton(tray, link.id, clickHandler, args, player, isForward);
                    buttons.push(button);
                    // if the link isn't valid, notify the user
                    if(!link.isValid) {
                        if(player.isStoryteller) {
                             button.setMessage("This action hasn't been created and published yet", "warning", true);
                         } else if(player.isPageComplete()){
                             button.setMessage("Waiting for Storyteller...", "info");
                         }
                    } else if(!player.isStoryteller && player.isWaiting(link.id)) {
                        // only do this if the player has been waiting (Which means writing and persisting some state)
                        button.setMessage("This page is ready! Click again to proceed", "ok");
                    }

                }
            }
            return buttons;
        },
        
        // generate a next/previous button
        // this helper function sets the button so that it controller to the active story
        // Delegate down to the Renderer?
        generateButton: function(commandTray, id, handler, args, player, isForward) {
            var ButtonClass = isForward ? ForwardButton : Button;
            args = args || {};
            args.group=commandTray;
            args.player = player;
            var button = new ButtonClass(undefined, args);
            button.controller = this;
            button.command = handler;
            commandTray.addButton(id, button);

            if (isForward && this._editMode) {
                button.edit(true);
            }

            return button;
        }
    };

    return Page;

});