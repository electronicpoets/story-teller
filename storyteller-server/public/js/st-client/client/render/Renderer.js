// generic, abstract page renderer
define(["util/domUtils", 
        "client/story/widgets/Text", 
        "client/story/widgets/Picture", 
        "client/app/widgets/ButtonGroup",
        "client/edit/common"], function(dom, Text, Picture, ButtonGroup, common) {

    var Renderer = function(page) {
        this._page = page; // the logical page which controls this renderer
        this.nodes={};
    };

    Renderer.prototype = {
        
        type: "default",
        
        // Set-up the page and its chrome
        initDom: function(parentNode, model, before){
            var nodes = this.nodes;
            nodes.root = dom.create({class:"page "+this.type, attrs: {"id": model.id}, parent: parentNode, order: before});
            
            nodes.status = dom.create({
                class:"status",
                parent: nodes.root,
                tooltip: "Publish this page so that other players can see it. <br/><b>Note</b> This will apply to all pages and automatically save any changes."
            });
            
            this.initPageControls();
            // provide a left and right tray node
            return nodes.root;
        },
        
        initPageControls: function(){
            var root = this.nodes.root;
            
            var left = dom.create({parent: root});
            this.previousTray = new ButtonGroup(left, {className: "left"});
            var right = dom.create({parent: root});
            this.nextTray = new ButtonGroup(right, {className: "right"});
        },
        
        showUnpublished: function(){
            var nodes = this.nodes;
            var root = nodes.status;
            $(root).addClass("show");
            var icon = nodes.pubstatus;
            root.onclick = function(){
                window.publishAll();
                
                $(icon).tooltipster("content", "This page is shared with other players");
                icon.className = "editOnly fi published";
            }
            root.textContent = "UNPUBLISHED";
        },
        
        hideUnpublished: function(){
            var root = this.nodes.status;
            // Note: will I need to update change listeners on re-draw? Probably...
            if (root) {
                $(root).removeClass("show");
            }
        },

        drawTitle: function(model){
            var nodes = this.nodes,
                parentNode = nodes.title||nodes.root,
                player = this._page._player;

            if(!nodes.titleWrapper) {
                var root = nodes.titleWrapper = dom.create({class:"titleTray", parent: parentNode, order:0});

                if(this.hideTitle) {
                    root.className+=" collapse collapsable";
                    var self=this;
                    $(root).on('mouseover', function(){
                        if(self._currentState!=="active") {
                            $(root).removeClass("collapse");
                        }
                    }).on('mouseout', function(){
                        $(root).addClass("collapse");
                    })
                }

                nodes.pageNumber = dom.create({
                    type:"span", 
                    class:"pageNumber", 
                    text:model.number+".",
                    parent: root
                });
                var titleInput = nodes.pageTitle = dom.create({
                    type:"span", 
                    class:"pagetitle", 
                    text:model.title,
                    parent: root
                });
                $(titleInput).on("paste keyup", function() {
                    var newTitle = this.textContent;
                    if(newTitle!==model.title) {
                        model.title=newTitle;
                        player.editPage(model.id, {title: newTitle});
                    }
                });
                
                if(player.isStoryteller){
                    var page = this._page;
                    dom.create({
                        type:"span", 
                        class:"fi edit",
                        parent: root,
                        tooltip: "Edit this page",
                        click: function() {
                            page.toggleEdit();
                        }
                    });
                }
                
                var isShared = model.status==="shared";
                var ttmsg = isShared ? "This page is shared with other players" : "This shared is private; other players cannot see it";
                this.nodes.pubstatus = dom.create({
                    type:"span", 
                    class:"editOnly fi "+(isShared?"published":"unpublished"),
                    parent: root,
                    tooltip: ttmsg
                });

            }
            var edit = this._page._editMode;
            nodes.pageTitle.setAttribute("contenteditable", !!edit);
            if(edit) {
                $(nodes.titleWrapper).addClass("edit");
            } else {
                $(nodes.titleWrapper).removeClass("edit");
            }  
        },
        
        setState: function(state) {
            var dom = this.nodes.root;

            if (state != this._currentState) {
                $(dom).removeClass("previous active next");
                if (state) {
                    $(dom).addClass(state);
                    //$(dom).removeClass(this._currentState);
                }
                this._currentState = state;
                if (state) {
                    $(dom).addClass(state);
                    this._updatePosition();
                }
                // if state is not active, remove any notifications
                if(state!=="active") {
                    this.previousTray.hide();
                    this.nextTray.hide();
                } else {
                    this.previousTray.hide();
                    this.nextTray.hide();
                }
            }
        },
        
        // stub, called when the page resizes. This is here to lock aspect ratios
        // probably ought to rename to onSizeChanged
        _updatePosition: function(disableAnimation){
            var state = this._currentState;
            var dom = this.nodes.root;

            var parentNode = dom.parentNode;
            var screenWidth = parentNode.offsetWidth;

            var height = parentNode.offsetHeight;
            var width = Math.floor(height * 1.78);

            // note that in my later UIs, I'll probably want uneven margins
            var margin = (screenWidth - width) / 2;
            if (width >= screenWidth) {
                var diff = width - screenWidth;
                top = bottom = (diff / 4) + "px";
                //width = screenWidth;
                margin = 0;
            }
            // make sure the width doesn't exceed the screen width - if so, reduce the height accordingly
            var left, right, top, bottom;
            if (state == "active") {
                left = margin + "px";
                right = margin + "px";
            } else if (state == "previous") {
                left = -(screenWidth - margin) + "px";
                right = (screenWidth + margin) + "px";
            } else if (state == "next") {
                left = (screenWidth + margin) + "px";
                right = -(screenWidth - margin) + "px";
            }

            var args = {
                left: left,
                right: right
            };
            if (top) {
                args.top = top;
                args.bottom = bottom;
            }

            if (disableAnimation) {
                $(dom).removeClass("animate");
            } else {
                $(dom).addClass("animate");
            }
            $(dom).css(args);
            // if(disableAnimation) {
            // 	$(dom).addClass("animate");
            // }
        },
        
        showBgPopup: function(target) {
             if(!this.nodes.editBg) {
                 if(!this._backgrounds) {
                    this._backgrounds = [{label:"<none>", value: undefined}];
                    $.get("/data/backgrounds", function(data){
                        //console.log("Loaded Backgrounds!");
                        //console.log(data);
                        for(var i=0;i<data.length;i++) {
                            this._backgrounds.push({label:data[i].name, value: data[i].key});
                        }
                        this.showBgPopup(target);
                    }.bind(this));
                    var tmp = dom.create({text:"Loading backgrounds..."});
                    common.showTT(target, tmp, undefined, true);
                    return;
                }
                
                this.nodes.editBg = dom.create({parentNode: $('body')[0]});
                
                var player = this._page._player; // dubious
                var model=this._page._model;     // also dubious
                //console.log(model);
                var changeBg = function(evt) {
                    var select = evt.target;
                    var option = select.item(select.selectedIndex);
                    console.log("Setting bg to "+option.value);
                    this.setBackground(option.value);
                    player.editPage(model.id, {background: option.value});
                    // flag change... ? Yes, this needs to go through the player interface
                }.bind(this);
                common._createControl(this.nodes.editBg, "Background", "select", changeBg, this._backgrounds, model.background);

                // TODO - toggle whether the background image is faded or not
                common._createControl(this.nodes.editBg, "Fade Background?", "check", function(checked) {
                    this.setBackground(undefined, checked);
                    player.editPage(model.id, {fadeBg: checked});
                }.bind(this), model.fadeBg);
            }
            
            common.showTT(target, $(this.nodes.editBg));
        },
        
        _lookupWidget: function(type) {
            if (type == "picture") {
                return Picture;
            }
            else
                return Text;
        },
        // return an array with all the widgets that have been rendered
        // Do I realy want this... ?
        // 7April 14 not really happy about the render state thing
        render: function(renderState, contents, editHandlers, forceResize) {
            // parse the page data
            // terminology isn't right here... 
            var targetNode = this.nodes.content || this.nodes.root;
            
            contents = contents || [];
            var controls = [];
            for (var i = 0; i < contents.length; i++) {
                var data = contents[i];
                if (!data) {
                    continue;
                }
                // make sure each content is rendered
                var render = renderState[data.id];

                var isDestroyed = data.destroyed || (data._delta ? data._delta.destroyed : false);

                if (!isDestroyed && !render) {
                    var constructor = this._lookupWidget(data.type);
                    var render = new constructor(targetNode, data);
                    if (render) {
                        renderState[data.id] = render;
                        render.render();
                    }
                } else if (render && isDestroyed) {
                    render.destroy();
                    renderState[data.id] = undefined;
                    delete renderState[data.id];
                    continue;
                } else if (isDestroyed) {
                    continue;
                }

                // JC 12May14 sloppy solution to force content to be updated
                // in practice, we should trigger an update event on the existing content,
                // just passing in the delta.
                if (render && data._delta) {
                    //console.log("Forcing re-render of "+data.id);
                    var delta = data._delta;
                    render.update(delta);
                    // now write the change back to the content
                    for (var p in delta) {
                        data[p] = delta[p];
                    }
//                    data._delta = undefined;
//                    delete data._delta;
                    //continue;
                }
                else if (render && data._new_) {
                    data._new_ = false;
                    delete data._new_;
                }
                
                if (render && forceResize) {
                    render.setSize();
                    render.positionDom();
                }

                controls.push(render);

                // ensure edit mode is correctly set
                if (editHandlers) {
                    render.enableEdit(editHandlers);
                } else {
                    render.disableEdit();
                }
            }
            return renderState;
        },
        setSize: function(args) {

        }

    };

    return Renderer;

});