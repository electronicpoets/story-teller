// render a page comprising up to four panels in a simple web-comic style

define(["util/domUtils", "client/render/Renderer","util/ImageBank"], function(dom, Renderer, imageBank){

	var ComicRenderer = function(){
            Renderer.apply(this, arguments);
	};
	
	ComicRenderer.prototype = Object.create(Renderer.prototype);
	
        ComicRenderer.prototype.type="comic";
        
        ComicRenderer.prototype.initDom = function(parentNode, model){
            var nodes = this.nodes;
            Renderer.prototype.initDom.apply(this, arguments);
            
            if(!nodes.bg) {
                nodes.bg = dom.create("div", "background", nodes.root);
                if (model.background) {
                    this.setBackground(model.background, model.fadeBg);
                }
            }
        };
        
        ComicRenderer.prototype.drawTitle = function(model){
            var nodes = this.nodes,
                    self = this;
            
            Renderer.prototype.drawTitle.apply(this, arguments);
            if(!nodes.bgEdit) {
                nodes.bgEdit = dom.create({
                    type:"span", 
                    class:"editOnly fi bg",
                    parent: nodes.titleWrapper,
                    //tooltip: "Edit the page background",
                    click: function(){
                        self.showBgPopup(this);
                    }
                });
            }
        };
        
        ComicRenderer.prototype.setBackground = function(id, fade){
            var dom = this.nodes.bg;
            
            if(typeof fade=="boolean") {
                if (fade) {
                    $(dom).addClass("fade");
                } else {
                    $(dom).removeClass("fade");
                }
            }

            var clearBg = id=="<none>"
            if(clearBg) {
                $(dom).css({
                    backgroundImage: "none"
                });
            }
            else if(id) {
                $(dom).css({
                    backgroundImage: "none"
                });
                imageBank.get(id, function(data){
                    var url;
                    if(data) {
                        url="url('"+data+"')";
                    }
                    $(dom).css({
                        backgroundImage: url || "none"
                    });
                });
            }
        };
        
        // 8June14 - ensure sure matches a 16:9 (1.78:1) ratio
//        ComicRenderer.prototype._updatePosition = function(disableAnimation) {
//            var state = this._currentState;
//            var dom = this.nodes.root;
//
//            var parentNode = dom.parentNode;
//            var screenWidth = parentNode.offsetWidth;
//
//            var height = parentNode.offsetHeight;
//            var width = Math.floor(height * 1.78);
//
//            // note that in my later UIs, I'll probably want uneven margins
//            var margin = (screenWidth - width) / 2;
//            if (width >= screenWidth) {
//                var diff = width - screenWidth;
//                top = bottom = (diff / 4) + "px";
//                //width = screenWidth;
//                margin = 0;
//            }
//            // make sure the width doesn't exceed the screen width - if so, reduce the height accordingly
//            var left, right, top, bottom;
//            if (state == "active") {
//                left = margin + "px";
//                right = margin + "px";
//            } else if (state == "previous") {
//                left = -(screenWidth - margin) + "px";
//                right = (screenWidth + margin) + "px";
//            } else if (state == "next") {
//                left = (screenWidth + margin) + "px";
//                right = -(screenWidth - margin) + "px";
//            }
//
//            var args = {
//                left: left,
//                right: right
//            };
//            if (top) {
//                args.top = top;
//                args.bottom = bottom;
//            }
//
//            if (disableAnimation) {
//                $(dom).removeClass("animate");
//            }
//            $(dom).css(args);
//            // if(disableAnimation) {
//            // 	$(dom).addClass("animate");
//            // }
//        };
        
        
        ComicRenderer.prototype.transition = function(from, to) {
            
        };
        
	return ComicRenderer;
	
});