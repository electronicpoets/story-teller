// prose files have large bodies of text.
// each "page" verically scrolls a new set of paragraphs. Old pages are pushed off the screen, but 
// the player scrolls up to see old content
// next/previous is more like a chapter control
// paragraphs can optionall be divided into player perspectives (ie, have a title)

define(["util/domUtils", "client/render/Renderer", "client/story/widgets/Prose","client/app/widgets/ButtonGroup"], 
        function(dom,Renderer, Prose, ButtonGroup){

	var ProseRenderer = function(){
            Renderer.apply(this, arguments);
            console.log("Creating new Prose Renderer");
            
            // total hack - force scrollong on the parent
            var container = document.querySelector(".pageContainer");
            container.style.overflowY="auto";
	};
        
	ProseRenderer.prototype= Object.create(Renderer.prototype);
	
        ProseRenderer.prototype.type="prose";
        
        ProseRenderer.prototype.hideTitle=true;
        
        ProseRenderer.prototype._lookupWidget = function(type) {
            if (type == "picture") {
                return Picture;
            }
            else
                return Prose;
        };
        
        ProseRenderer.prototype._updatePosition = function(disableAnimation){
            // alculate scroll offset and jq animate
            if(!disableAnimation && this._currentState=="active") {
                var root = this.nodes.root;
                var scrollTarget = root.offsetTop;
                $(root.parentNode).animate({
                    scrollTop: scrollTarget
                });
            }
        };
        
        // Set-up the page and its chrome
         ProseRenderer.prototype.initDom = function(parentNode, model, before){
            var nodes = this.nodes;
            var root = nodes.root = dom.create({class:"page "+this.type, attrs: {"id": model.id}, parent: parentNode, order:before});
           
            nodes.header = dom.create({class:"header", parent: root});
            nodes.headerCells=[];
             for(var i=0;i<3;i++){
                nodes.headerCells.push(dom.create({class:"cell", parent: nodes.header}));
            }
            nodes.title = nodes.headerCells[0];
            
            nodes.content = dom.create({class:"body", parent: root});
            nodes.footer = dom.create({class:"footer", parent: root});
   
            nodes.bg = dom.create("div", "background", root);
            if (model.background) {
                this.setBackground(model.background, model.fadeBg);
            }
            
            this.initPageControls();
            return nodes.root;
        };
        
        ProseRenderer.prototype.initPageControls = function(){
            var topTray = dom.create({parent: this.nodes.headerCells[1], class:"pageControls top"});
            var bottomTray = dom.create({parent: this.nodes.footer, class:"pageControls bottom"});
            
            this.nextTray = new ButtonGroup(dom.create({parent:bottomTray}), {className: "right"});
            this.previousTray = new ButtonGroup(dom.create({parent:topTray}), {className: "left"});
            
            this.nodes.status = dom.create({
                class:"status",
                parent: this.nodes.headerCells[2],
                tooltip: "Publish this page so that other players can see it. <br/><b>Note</b> This will apply to all pages and automatically save any changes.",
                html: "o"
            });
        };
        
	return ProseRenderer;
	
});