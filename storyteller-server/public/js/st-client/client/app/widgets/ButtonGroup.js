// Class which acts as a container for a set of buttons
// Has an edit mode toggle, which changes the behaviour of its nested controls
// Buttons can be added as "edit only", which means they are invisible in read mode

define(["util/domUtils"], function(dom) {

    var ButtonGroup = function(parentNode, args) {
        this.clazz = args.className;
        this.nodes = {
            root: parentNode
        };
        this._idx={};
        //this.controls = [];
        this.initDom();
    };

    ButtonGroup.prototype = {
        initDom: function() {
            //var clazz = (this.clazz || "") + " buttonGroup commandTray";
            var clazz = (this.clazz || "") + " buttonGroup";
            this.nodes.root.className = clazz;
            this.nodes.header = dom.create({class:"header", parent:this.nodes.root});
        },
        addButton: function(id, button) {
            this._idx[id] = button;
            //this.controls.push(button);
            button.create(this.nodes.root);
        },
        has: function(id){
            if(this._idx[id]) {
                return true;
            }
        },
        remove: function(id){
            var button = this._idx[id];
            if(button) {
                button.destroy();
                
                this._idx[id] = undefined;
                delete this._idx[id];
            }
        },
        // prune contents so that only these links remain
        // keepIds must be an object
        prune: function(keepIds){
            var idx = this._idx;
            for(var id in idx) {
                if(!keepIds[id]) {
                    this.remove(id);
                }
            }
        },
        edit: function(enableEdit) {
            if (enableEdit != this._isEditMode) {
                this._isEditMode = enableEdit;
                
                for(var c in this._idx) {
                    this._idx[c].edit(enableEdit);
                }
//                for (var i = 0; i < this.controls.length; i++) {
//                    this.controls[i].edit(enableEdit);
//                }
                
                if(enableEdit) {
                    this.showHeader();
                } else {
                    this.hideHeader();
                    this.expand();
                }
                this.triggerEvent(enableEdit?this._editListeners:this._uneditListeners);
             }
        },
        
        addEditListener: function(ctx, func){
            if(!this._editListeners) {
                this._editListeners=[];
            }
            this._editListeners.push({ctx:ctx, handler: func});
        },
        
        addUneditListener: function(ctx,func){
            if(!this._uneditListeners) {
                this._uneditListeners=[];
            }
            this._uneditListeners.push({ctx:ctx, handler: func});
        },
        
        triggerEvent: function(handlers) {
            if(handlers) {
                for(var i=0;i<handlers.length;i++) {
                    var evt = handlers[i];
                    evt.handler.call(evt.ctx);
                }
            }
        },
        
        // TODO:
        // create a header area
        showHeader: function(){
            var header = this.nodes.header;
            if(header.children.length==0) {
                //var wrapper = dom.create({class:"wrapper",parent:header});
                var left = dom.create({class:"cell left",parent:header});
                var center = this.nodes.label = dom.create({class:"cell center", text: "Hide Links",parent:header});
                $(center).click(this.toggleCollapse.bind(this));
                var right = dom.create({class:"cell right",parent:header});
            }
            $(header).addClass("show");
        },
        
        hideHeader: function(){
            var header = this.nodes.header;
            if(header) {
                $(header).removeClass("show");                
            }
        },
        
        collapse: function(){
            if(!this._collapsed) {
                this._collapsed=true;
                this.nodes.label.textContent = "Show Links";
                var root = this.nodes.root;
                $(root).addClass("collapsed");
            }
        },
        
        expand: function(){
            if(this._collapsed) {
                this._collapsed=false;
                this.nodes.label.textContent = "Hide Links";
                var root = this.nodes.root;
                $(root).removeClass("collapsed");
            }
        },
        
        toggleCollapse: function(){
            if(this._collapsed) {
                this.expand();
            } else {
               this.collapse(); 
            }
        },
        
        focusPrevious: function(id){
            var target;
            var index = Object.keys(this._idx);
            for(var i=index.length;i<0;i--) {
                if(index[i]==id) {
                    target = i-1;
                }
            }
            if(target<0) {
                target = index.length-1;
            }
            
            var buttonId=index[target];
            if(buttonId) {
                this._idx[buttonId].focus();
            }
        },
        
        focusNext: function(id){
            var target;
            var index = Object.keys(this._idx);
            for(var i=0;i<index.length;i++) {
                if(index[i]==id) {
                    target = i+1;
                }
            }
            if(target>=index.length) {
                target = 0;
            }
            
            var buttonId=index[target];
            if(buttonId) {
                this._idx[buttonId].focus(true);
            }
        },
        
        hide: function(){
            // hide all TT notifications
            for(var idx in this._idx){
                var b = this._idx[idx];
                if(b.hide) {
                    b.hide();
                }
            }
        },
        
        show: function(){
            
        }

    };

    return ButtonGroup;

});