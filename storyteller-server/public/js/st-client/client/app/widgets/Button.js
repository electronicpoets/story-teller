// A pre-canned client-side button
// has dynamic behaviours on-click

define([], function(){

	var Button = function(parent, args) {
		if (args.command) {
			this.setCommand(args.command);
		}
		
		// a button may be defined by a model
		if(args.model) {
			var model = this.model = args.model;
			this.title=args.title||model.title;
			this.subtitle = args.subtitle||model.subtitle;
		} else {
			this.title = args.title;
			this.subtitle = args.subtitle;			
		}
		
		// some buttons are only seen at edit time
		if(args.editOnly) {
			this.editOnly=true;
		}
		
		if(args.onEdit) {
			this.onEdit = args.onEdit;
		}
		
		if (args.onRemove) {
			this.onRemove = args.onRemove;
		}
                if(args.player) {
                    this.player = args.player;
                }
                if(args.group) {
                    this.group = args.group;
                }
		
		this.nodes={
			parent: undefined,
			root: undefined
		};
		
		if(parent) {
			this.create(parent);
		}
	};
	
	Button.prototype = {
	
		controller: undefined, // the controller to manipulate on press
		
		setCommand: function(cmd){
			this.command = cmd;
		},
		
		// creates the button inside the target container
		// TODO - on edit, we change the model and re-render the button's content
		create: function(parent){
			var container = document.createElement("div");
			container.className="buttonContainer";
			if(this.editOnly) container.className+=" editOnly";
			this.nodes.root = container;
			
			var dom = this.nodes.dom = document.createElement("div");
			dom.onclick=this.doClick.bind(this);
			dom.className="button";
			container.appendChild(dom);
			
			var title = document.createElement("span");
			if(this.title) {
				title.textContent = this.title;
			}
			title.className="label";
			this.nodes.title = title;
			dom.appendChild(title);
			
			if(this.subtitle) {
				this.setSubtitle(this.subtitle, true);
			}
			
			// todo - create a longer, hideable, optional subtitle
			if(parent) {
				this.nodes.parent=parent;
				parent.appendChild(container);
			}
		},
		
		doClick: function(){
			if(this.command && !this._disabled) {
				this.command(this.model, this);
			}
		},
		
                destroy: function(){
                    var root= this.nodes.root;
                    if(root && root.parentNode) {
                        root.parentNode.removeChild(root);
                    }
                    // disconnect any event handlers?
                },
                
		// remove and destroy this button.
		// if animate is true, the button will remove itself prettily
		remove: function(animate){
                    this.destroy();
                    var player=this.player;
                    if(player) {
                        player.removeLink(this.model);
                    }
                    // deprecated...
                    if(this.onRemove) {
                        this.onRemove(this.model);
                    }
		},
		
		edit: function(doEdit){
			if(doEdit!=this._edit) {
				var root = this.nodes.root;
				if(doEdit) {
					if(!this.editOnly && !this._editControl) {
						this._editControl=document.createElement("div");
						this._editControl.className="editControl button edit";
						this._editControl.textContent="Edit";
						if(this.onEdit) {
							$(this._editControl).click(function(){
								this.onEdit(this.model);	
							}.bind(this));
						}
						
						this._removeControl=document.createElement("div");
						this._removeControl.className="editControl button remove";
						this._removeControl.textContent="X";
						if(this.onRemove) {
							$(this._removeControl).click(function(){
								this.onRemove(this.model);	
							}.bind(this));
						}

						//root.insertBefore(this._removeControl,root.firstChild);						
						this.nodes.root.insertBefore(this._editControl, this.nodes.root.firstChild);
						root.appendChild(this._removeControl);
					} else if (this.editOnly && !this._filler) {
						var filler = this._filler = document.createElement("div");
						filler.className="editControl button";
						filler.style.visibility="hidden";
						root.insertBefore(filler, root.firstChild);						
					}
					
					$(this.nodes.root).addClass("edit");
				} else {
					$(this.nodes.root).removeClass("edit");
				}
				
				this._edit = doEdit;
			}
		},
		
		disable: function(){
			if (!this._disabled) {
				this._disabled = true;
				$(this.nodes.dom).addClass("disabled");
			}
		},
		
		enable: function(){
			if (this._disabled) {
				this._disabled = false;
				$(this.nodes.dom).removeClass("disabled");
			}
		},
		
		setSubtitle: function(text, transient){
			var nodes = this.nodes;
			if(!nodes.sub) {
				var sub = nodes.sub = document.createElement("div");
				sub.className="subtitle";
				nodes.dom.appendChild(sub);
				nodes.sub.innerText = text;
			}
			else if(text!=this.subtitle) {
				this.subtitle = text;
				nodes.sub.innerText = this.subtitle;
			}
			
			if (!transient) {
				// write to the model
				this.model.subtitle = text;
				// trigger event?
			}

		},
                
                setMessage: function(message){
                    
                },
                
                // set the focus on this button
                focus: function(isfirst){
                    var input = isfirst?this._firstInput:this._lastInput;
                    if(input) {
                        $(input).focus();
                    }
                }
		
	};
	
	return Button;

});