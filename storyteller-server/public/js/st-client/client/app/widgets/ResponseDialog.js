// silly, last-minute widget to report response data
define(["client/story/widgets/Picture", "util/domUtils", "util/TimeStamp"], function(Picture, dUtils, time) {


    var Popup = function(controller, parentNode, controlNode, changeNotifier) {
        this.parentNode = parentNode;
        this.controlNode = controlNode;
        this.controller = controller;
        this._changeNotifier = changeNotifier;
        this.initDom();
    };

    Popup.prototype = {
        initDom: function() {
            var handle = this.controlNode;

            handle.onclick = function() {
                if (this._open) {
                    this.close();
                } else {
                    this.open();
                }
            }.bind(this);

            var tray = this.trayNode = document.createElement("div");
            tray.className = "drawer tray responses";

            var contentNode = this.contentNode = document.createElement("div");
            contentNode.className = "tray-content";
            tray.appendChild(contentNode);

            this.parentNode.appendChild(tray);
        },
        open: function(content) {
            if (!this._open) {
                this._open = true;

                if (this._changeNotifier) {
                    this._changeNotifier.clear();
                }

                var tray = this.trayNode;
                console.log(tray);
                var handle = this.controlNode;
                $([tray, handle]).addClass("open");

                tray.style.position = "absolute";

                tray.style.left = handle.offsetLeft + "px";
                //tray.style.right = (document.body.offsetWidth - (handle.offsetLeft+handle.offsetWidth))+"px";
                tray.style.top = (handle.offsetTop + handle.offsetHeight) + "px";

                this.contentNode.innerHTML = "";
                // split dialog into this page | previous page sections


                // TODO - only show this when there IS a previous page
                // maybe group players into pages
                // look to see if there's a previous page
                var prev = this.controller.getResponses(true);
                if (prev && prev.length > 0) {
                    var prevPage = dUtils.create("div", "column", this.contentNode);
                    var prevTitle = dUtils.create("div", "title", prevPage);
                    prevTitle.textContent = "Previous Page";
                    this.printResponses(prev, prevPage);
                }

                var current = this.controller.getResponses();
                var currentPage = dUtils.create("div", "column current", this.contentNode);

                var title = dUtils.create("div", "title", currentPage);
                title.textContent = "This Page";
                this.printResponses(current, currentPage);
            }
        },
        printResponses: function(responses, parent) {
            if (responses && responses.length > 0) {
                for (var i = 0; i < responses.length; i++) {
                    // list player: option
                    var x = responses[i];
                    var option = dUtils.create({parentNode: parent});

                    dUtils.create({class: "player", parentNode: option, text: x.playerName});
                    dUtils.create({class: "option", parentNode: option, text: x.linkName});

                    // if there's a free style response, print taht to
                    if (x.message) {
                        dUtils.create({class: "detail", parentNode: option, text: x.message});
                    }
                    if(x.timestamp) {
                        var stamp = time.stamp(x.timestamp);
                        option.appendChild(stamp);
                    }

                    // include the source page?
                }
            } else {
                var empty = dUtils.create("div", "empty", parent);
                empty.textContent = "No responses available...";
            }
        },
        // probably want to destroy content when the tray is closed, there might be a lot of HTML here.
        // maybe keep the front picture, but lose all the widget boiler-plate
        close: function() {
            if (this._open) {
                this._open = undefined;
                $([this.trayNode, this.controlNode]).removeClass("open");
            }
        }
    };

    return Popup;

});