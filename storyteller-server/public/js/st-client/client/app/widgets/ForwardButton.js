// A pre-canned client-side button
// has dynamic behaviours on-click

// JC 10Oct14 This is actually the worst hack in the world.
// TODO - make it more general, a horizontal or vertical orientation
var isProseStory = window.storyJson.contentType=="Prose";
var tmp =isProseStory ? "text!client/app/widgets/template/pButton.html" :  "text!client/app/widgets/template/Button.html";

define(["client/app/widgets/Button", 
        tmp, 
        "util/TimeStamp",
        "util/domUtils"], 
    function(Button, template, time, dom){
    
    // do I need to do anything special with this constructor?
    var ForwardButton = function(){
        // invoke the parent constructor
        Button.apply(this, arguments);
        this.orientation = isProseStory ? "horizontal" : "vertical";
    };
    
    var body = {
       
        _tooltip: function(node, content){
            return $(node).tooltipster({
                content: content,
                position: this.orientation ? "right" : "top",
                animation: "grow",
                delay: 800
            });
        },
        
        // override the create method to use a template
        create: function(parent){
            if(template) {

                var container = this.nodes.root=document.createElement("div");
		container.className="buttonContainer";
                container.innerHTML = template;
                
                // query for any propertybindings
                var nodes = this.nodes;
                $("[data-binding]", container).each(function(idx,node){
                    var bindAs = node.getAttribute("data-binding");
                    nodes[bindAs] = node;
                });
                
                // also bind event handlers as data-on
                var self=this;
                $("[data-click]", container).each(function(idx,node){
                    var cmd = node.getAttribute("data-click");
                    var handler = self[cmd].bind(self);
                    $(node).click(function(evt){
                        evt.stopPropagation();
                        handler(this);
                    });
                    //$(node).onClick(function() { alert("Hello world!)") });
                });
                // finally, set soem default values
                if(this.title) {
                    this.setTitle(this.title);
                }
                if(this.subtitle) {
                    this.setSubtitle(this.subtitle);
                }
                
                if(parent) {
                    this.nodes.parent=parent;
                    parent.appendChild(container);
                }
                
                if(this.model.freestyle) {
                    this.toggleType(true);
                } else {
                    this.toggleType(false);
                }
                this._firstInput=this.nodes.title;
                this._lastInput=this.nodes.subtitle;
            }
            this.updateNullStatus();
            this.inited=true;
        },
        
        setTitle: function(title) {
            var nodes = this.nodes;
            this.title = title;
            nodes.title.textContent = title;
        },
        
        setSubtitle: function(text, transient){
            var nodes = this.nodes;
            this.subtitle = text;
            nodes.subtitle.textContent = text;

            if (!transient) {
                this.model.subtitle = text;
            }
        },
        
        doClick: function(){
          if(!this._edit)   {
              Button.prototype.doClick.apply(this, arguments);
          }
        },
        
        
        edit: function(doEdit){
            if(!this._edit === doEdit) {
                var nodes = this.nodes;
                var root = nodes.root;
                var toolbar = nodes.cmdTray;
                if(doEdit) {
                    $(root).addClass("edit");
                    this.renderTargets();
                    // Note: we can only do this if another page/story doesn't have a dependency on it
                    if(toolbar && !this._removeIcon) {
                        this._removeIcon = dom.create({class:"fi remove",parent:toolbar, click:this.remove.bind(this)});
                        this._tooltip(this._removeIcon, "Remove this action from the page");
                    }
                } else {
                    $(root).removeClass("edit");
                    if(this._removeIcon) {
                        this._removeIcon.parentNode.removeChild( this._removeIcon);
                        this._removeIcon=undefined;
                    }
                    if(this.isNull()) {
                        this.remove();
                    }
                }
                this._edit = doEdit;                

//                if(this.model.target) {
//                    $(root).addClass("fixed");
//                }
//                else {
                var onEditLabel=function(node, value){
                   var prop = node.getAttribute("data-editProp")
                   this._onChange(prop, value);  
                }.bind(this);
                this._makeEditable([nodes.title, nodes.subtitle], onEditLabel);

//                if(this.model.target) {
                    var links =[];
                    var set = nodes.targetOutput.children;
                    for(var i=0;i<set.length;i++) {
                        var item = set[i];
                        links.push(item.firstChild.firstChild);
                    }
    //                }

                    this._makeEditable(links, this._onEditLink.bind(this), true);
//                }
            }
        },
        
        _onEditLink: function(node, value){
            // lookup the page associated with the value
            var page = this.player.getPage(value);
            // if there is no page, invalidate yourself
            // if there is a page, set its id as the target
            if(page) {
                this._onChange("target", page.id);
            }
        },
        
        _makeEditable: function(node, onEdit, filter){
            if(node.concat && node.length>0) {
                while(node.length>0) {
                    this._makeEditable.call(this, node.shift(), onEdit, filter);
                }
            } else {
                var nodes = this.nodes;
                var editable=this._edit;
                var self=this;

                var toPlaceholder=function(owner){
                    if(owner.getAttribute("data-editProp")=="title" && this.isNull()) { // dislike
                        owner._contentBuffer=owner.textContent="-Add Action-";
                    } else {
                        owner._contentBuffer=owner.textContent="-"+owner.getAttribute("data-placeholder")+"-";
                    }
                    $(owner).addClass("placeholder");
                    $(owner).on('focus', function(){
                        $(owner).removeClass("placeholder");
                        owner._contentBuffer="";
                        owner.textContent="";
                    });
                }.bind(this);

                
                 if(editable) {
                     if(filter) {
                        if(node.textContent)  {
                            // A set filter is a clickable link
                            $(node).removeAttr("contentEditable");
                            $(node).removeClass("editable");
                            $(node.parentNode.parentNode).addClass("active");
                            $(node.parentNode).click(Button.prototype.doClick.bind(this));
                            return;
                        }else {
                            // An empty filter is a placeholder with autocomplete
                            node._ac = this._createAutocomplete(node);
                            $(node).attr("contentEditable", true);         
                            $(node).addClass("editable");
                            $(node.parentNode.parentNode).removeClass("active");
                            $(node.parentNode).click(function(){});
                            toPlaceholder(node);
                         }
                     }else {
                        // A non-filter (ie,a label) is editable and may be a placeholder
                        $(node).attr("contentEditable", true);         
                        $(node).addClass("editable"); 
                        if(!node.textContent) {
                            toPlaceholder(node);
                        }
                     }

                    $(node).on('blur keydown paste input', function(evt){
                        if(self.isNull() && evt.keyCode==9) {
                            self.selectNext();
                            evt.preventDefault();
                        } else {
                            var newContent = this.textContent;
                            if(newContent != this._contentBuffer) {
                                this._contentBuffer = newContent;
                                onEdit(this, newContent);

                                $(this).removeClass("placeholder");
                                $(this).off('focus');
                            } else if(!newContent){
                                // If a filter select, be careful about when we insert placeholder text, b/c
                                // it'll confuse the filter itself
                                if(/*!filter ||*/ (evt.type!=="input" && evt.type!=="keydown")) {
                                    toPlaceholder(this);
                                }
                            }
                        }
                    });
                    this._lastInput=node;
                 } else {
                     $(node).removeAttr("contentEditable");
                     if(node._ac) {
                         $(node).autocomplete("destroy");
                     }
                     $(node).off('blur keydown paste input');
                     if($(node).hasClass("placeholder")) {
                         node.textContent="";
                         $(node).removeClass("placeholder");
                     }
                 }
            }
        },
        
        // write some state change back to the model
        _onChange: function(property, value){
            if(this.player) {
                var model = this.model;
                var data ={ id: model._id||model.id };
                model[property]=data[property]=value;
                if(!value) {
                    model[property]=data[property]=undefined;
//                    delete data[property];
//                    delete model[property];
                } 
                this.player.editLink(data);                
                this.updateNullStatus();
            }
        },
               
        renderTargets: function(){
            var model = this.model;
            var targetId = model.target;
            this.initPageTarget(targetId);
        },
        
        _refreshTarget: function(targetId, newId) {
            var root = this.nodes[targetId];
            if(root) {
                this.nodes[targetId]=undefined;
                root.parentNode.removeChild(root);
                this.initPageTarget(newId);
            }
        },
        
        initPageTarget: function(targetId){
            // load the target page details from the story
            if(this.player) {
                var pageData;
                var hasTarget;
                if(targetId) {
                    pageData = this.player.getPage(targetId);
                    hasTarget=true;
                } else {
                    pageData = { title: "" };
                    targetId="new";
                }
                
                var nodes = this.nodes;
                var parent = this.nodes.targetOutput;
                if(!nodes[targetId]) { // probably need to rebuild this every time... or at least on change
                    var body = nodes[targetId] =  dom.create({class:"targetWrapper",parent:parent});
                    if(pageData) {
                       var pageLink = dom.create({class:"linkTarget", parent:body});
                       var linkInput = dom.create({
                           class:"linkInput", 
                           text: pageData.title, 
                           parent:pageLink, 
                           attrs:{
                               'data-placeholder':'Page Title',
                               'data-editProp': 'target'
                            }
                        });
                       this._makeEditable(linkInput, this._onEditLink.bind(this), true);
                    }
                    var toolbar = dom.create({class:"iconTray",parent:body});
                    var delHandler=function(){
                        var id=targetId;
                        this.model.target=undefined;
                        this.player.editLink({id: this.model.id, target: null});
                        this._refreshTarget(id);
                    }.bind(this);
                    dom.create({class:"fi remove",parent:toolbar, click: delHandler});
                }
            }
        },
        
        // let the button display a message through a tooltip or pop-up of some kind
        // messages can be dismissed, but upon hovering over the node they re-appear
        // or, if messages are saved, they re-appear on load
        // we also need an icon to indicate a message
        setMessage: function(message, iconClass, hide){
            this._message=message;
            if(message) {
                var nodes = this.nodes;
                var msgNode;
                if(iconClass) {
                    this.showMessageIcon(iconClass);
                    msgNode = this.nodes.message;
                }
                else { 
                    msgNode = nodes.buttonRoot;
                }
                msgNode.tt =  $(msgNode).tooltipster({
                    content: message,
                    contentAsHTML:true,
                    position: this.orientation ? "right" : "top",
                    animation: "grow",
                    theme: iconClass=="ok"?"tooltipster-default st-tooltips-ok":"tooltipster-default st-tooltips"
                });
                if(!hide) {
                    this.showMessage();
                }

            } else {
                this.hideMessage();
                this.hideMessageIcon();
            }
            // update the icon tray to show there's a message here
        },
        
        showMessage: function() {
            //var message = this._message;
            var msg = this.nodes.message;
            if(msg && msg.tt) {
                $(msg).tooltipster("show");
            }
        },
        
        hideMessage: function(){
            var msg = this.nodes.message;
            if(msg && msg.tt) {
                $(msg).tooltipster("hide");
            }
        },
        
        showMessageIcon: function(className){
            var toolbar = this.nodes.cmdTray;
            if(toolbar) {
                this.nodes.message = dom.create({class:"fi inert "+className,parent:toolbar, order:0});
            }
        },
        
        hideMessageIcon: function(){
            var icon = this.nodes.message;
            if(icon) {
                this.nodes.message=undefined;
                icon.parentNode.removeChild(icon);  
            }
        },
        
        typeHandler: function(){
          this.toggleType();  
        },
        
        toggleType: function(override){
            var isFree =  this._isFreestyle = arguments.length==1 ? arguments[0] : this._isFreestyle=!this._isFreestyle;
            if(isFree!= !!this.model.freestyle) {
                this.model.freestyle=isFree;
                this.player.editLink({id:this.model.id, freestyle:isFree?true:null});
            }
            
            this.nodes.turnType.className="fi "+(isFree?"freestyle":"static");
            var content = isFree?"Click to disable a Freestyle action":"Click to enable a Freestyle action";
            
            if(this._turnTypeTooltip) {
              $(this.nodes.turnType).tooltipster("content", content);    
            } else {
                this._turnTypeTooltip = $(this.nodes.turnType).tooltipster({
                    content: content,
                    position: this.orientation ? "right" : "top",
                    animation: "grow",
                    delay: 800
                });
            }
            
            this.updateFreeStyleMessage();
       },
        
        updateFreeStyleMessage: function(){
            if(this._isFreestyle) {
                //var node = this._freeStyleIcon = dom.create({class:"icon free",parent:this.nodes.cmdTray, order:0});
                var node = this._freeStyleIcon = dom.create({class:"fi freestyle",parent:this.nodes.cmdTray, order:0});

                var iconContent;
                var response = this.player.getResponse();
                if(response) {
                    if(response.message) {
                        var name = window.spectateMode?this.player.getName()+"'s":"Your";
                        iconContent="<div class=\"freestyleTitle\">"+name+" Freestyle Response</div>"
                                     +"<div class=\"freestyleBody\">"+response.message+"</div>";
                    }
                    if(response.timestamp) {
                        iconContent+="<div class=\"freestyleDate\">"+time.getString(response.timestamp)+"</div>";
                    }
                }
                
                var args={
                    position: this.orientation ? "right" : "top",
                    animation: "grow",
                    delay: 600
                };
                if(iconContent) {
                    args.contentAsHTML=true;
                } 
                args.content=iconContent||"This is a Freestyle action, which allows you to input anything you want and send it to the Storyteller";
                $(node).tooltipster(args);
            }else if(this._freeStyleIcon) {
                 this._freeStyleIcon.parentNode.removeChild(this._freeStyleIcon);
            }
        }, 
        
        onFocus: function(){
            
        },
        
        onBlur: function(){
            
        },
        
        isNull: function(){
            var model = this.model;
            return !(model.title||model.subtitle);
            //return Object.keys(model).length<=1;
        },
        
        // check if this is a null button. If the status has changed since 
        // last time, return true
        checkNull: function(){
            var current = this._isNull;
            var isNull = this.isNull();
            if(isNull!==current) {
                this._isNull = isNull;
                return true;
            }
        },
        
        updateNullStatus: function(){
            var changed = this.checkNull();
            if(changed) {
                var root = $(this.nodes.root);
                var isNull = this._isNull;
                var player = this;
                if(isNull) {
                    root.addClass("null");
                    //this.model.isNull=true;
                } else {
                    root.removeClass("null");
                    //this.model.isNull=undefined;
                    //delete this.model.isNull;
                }
                // tmp: update the button list
                if(this.inited) {
                    window.controller.updateCommands();
                }
            }
        },
        
        selectNext: function(){
            if(this.group) {
                this.group.focusNext(this.model.id);
            }
        },
        
        selectPrevious: function(){
            if(this.group) {
                this.group.focusPrevious(this.model.id);
            }
        },
        
        _createAutocomplete: function(node){
            var self=this;
            var handleInput = function(content, isNew){
                if(isNew) {
                    var value = content === "<new>"?undefined:_.unescape(content);
                    self._createPage(value, true);
                }
                else {
                    node.textContent=content;
                    self._makeEditable(node, self._onEditLink.bind(this), true);
                }
            };

            var ac = $(node).autocomplete({
                minLength: 0,
                autoFocus: true,
                position: { my: "right top", at: "right bottom", collision: "flip" },
                select: function(event, ui){
                    if(event.keyCode==9) {
                        // tab to next button
                        if(event.shiftKey) {
                            $(self.nodes.subtitle).focus();
                        }else {
                            self.selectNext();
                        }
                    } else {
                        handleInput(ui.item.value, ui.item.isNew);
                    }
                }
            });
            ac.autocomplete('instance')._renderItem = function( ul, item) {
                var content = "<span class=\"command\">"+item.cmd+"</span><span class=\"label\">"+(item.label)+"</span><span class=\"icon fi "+(item.isNew?"newpage":"goto")+"\"></span>";
                return $( "<li></li>" )
                        .data( "item.autocomplete", item )
                        .append( $( "<a></a>" ).html(content) )
                        .appendTo( ul );
            };
            ac.focus(function(){            
                self._populateAutocomplete(ac);
                ac.autocomplete("search", "");
            });
            
                        
            // experimental: whenever the content of the element changes,
            // check if the currnet content matches, and set an icon
            // the icon is only visible until the content is "input",
            // either via ENTER or using the AC.
            // Very complex thing to do, though
            //$(node).on("input", function(){
//            ac.blur(function(){
//                var value = this.textContent;
//                console.log("Input value changed to: "+value);
//                var result = this._isPageTitle(value);
//                if(result==1) {
//                    // display a link icon
//                } else {
//                    // display a create new page icon
//                } 
//            }.bind(this));
            // add a special on-enter handler...
            $(node).on("input keydown", function(evt){
                if(evt.keyCode==13) {
                    evt.preventDefault();    
                   handleInput(this.textContent);
                } else if(evt.keyCode==9) {
                    evt.stopImmediatePropagation();    
                    evt.preventDefault();    
                    //self.selectNext();
                     if(!event.shiftKey) {
                        self.selectNext();
                     }
                }
            });
            return ac;
        },
        
        _populateAutocomplete: function(ac){
            var data = this._createPageList();
            ac.autocomplete({ source: data});
        },
        
        // TODO, cache this (until we detect a change event), maybe improve the player dependency
        _createPageList: function(){
            var pages = this.player.getPages();
            var currentPage = this.player.getCurrentPageId();
//            var result = [];
//            for(var p in pages) {
//                if(p!=currentPage) {
//                    var page = pages[p];
//                    result.push(page.title);
//                }
//            }
//            return result;
            
            var handler = function(req,resp){
                var isPage;
                var term = req.term;
                var data = [];
                var match;
                for(var p in pages) {
                    if(p!=currentPage) {
                        var page = pages[p];
                        var title = page.title||"";
                        var result = title.match(new RegExp("^"+term));
                        if(result) {
                            data.push({cmd:"Link to:", label:title,value:title});
                            if(result[0].length===title.length) {
                                match=true;
                            }
                        }
                    }
                }
                if(!match) {
                    var newCmd = {cmd:"Create Page:",label: _.escape(term||"<new>"), value: req.term||"<new>", isNew:true};
                    data.splice(0,0,newCmd);
                }
                resp(data);
            }
            return handler;
        },
        
        // returns 1 if an exact match, 0 if partial, and -1 if not a match. -2 if if matches the current page?
        _isPageTitle: function(content){
            var pages = this.player.getPages();
            var currentPage = this.player.getCurrentPageId();
            for(var p in pages) {
                if(p!=currentPage) {
                    var title = pages[p].title||"";
                    var result = title.match(new RegExp("^"+content));
                    if(result) {
                        if(result[0].length===title.length) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }
            }
            return -1;
        },
        
        _createPage: function(title, goto){
            var data = {},
                 model = this.model,
                 player = this.player;
         
            if(title) data.title=title;
            var page = player.addPage(data, player._currentPageId, true);
            model.target=page.id;
            player.editLink({id:model.id, target:page.id});
            if(window.storyJson.contentType=="Prose") {
                player.addContent("prose", "Enter Text Here...", page.id)
            }
            if(goto) {
                Button.prototype.doClick.apply(this);

            }
        },
        
        // hide any tooltips
        hide: function(){
            console.log("Hiding button...");
            this.hideMessage();
        }
        
    };
    

    
    // extend the prototype
    var proto = ForwardButton.prototype=Object.create(Button.prototype);
    for(var b in body) {
        proto[b] = body[b];
    };
    return ForwardButton;
});
