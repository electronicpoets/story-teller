// Widget which takes change notification from a variety of classes
// A notification can be set and cleared
// While notifications are active, teh notifier displays an alert
// While active and visible, shows a tooltip on hover summarising changes
// Note that buttons should be able to embed notifiers, to alert when a new 
// link has been added, or a new chat message has been received
define([], function(){
	
	// API definition
	var notification = {
		source: ""||{}, // a String or object (must have a "getName()" function [?])
						// the chap who registered this notification. Likely a player.
		message: "", // a string to display for when the notification changes
		type: "" // ? Ie, "change", "new content", "event" etc
	};
	
	var notId=1;
	
	var Notifier = function(parentNode){
		this._notifications=[];
		
		var dom = document.createElement("div");
		dom.className="notifier";
		dom.textContent="!";
		this.nodes={
			root: dom
		};
		parentNode.appendChild(dom);
	};
	
	Notifier.prototype={
		
		// TODO: check for duplication, aggregate like messages
		// Source is the Player/Page which hosts the change
		// Target is the content whcih has changed
		// Message is an arbitary String
		// Type is a change type (add/edit/remove)
		register: function(source, target, message, type){
			var note = {
				id: notId++,
				source: source,
				target: target,
				message: message,
				type: type
			};
			
			this._notifications.push(note);
			this.show();
			
			return note.id;
		},
		
		clear: function(noteId){
			var notes = this._notifications;
			if(noteId) {
				for(var i=0;i<notes.length;i++) {
					if(notes[i].id==noteId) {
						notes.splice(i, 1);
						break;
					}
				}
			} else {
				while(notes[0]) {
					notes.shift();
				}
			}
			if(notes.length==0) {
				this.hide();
			}
		},

		// Consider two animations: one for when the notifier first appears,
		// and another for when a new notification is added. Ie, pop-in and shake
		show: function(){
			$(this.nodes.root).addClass("show");
			
			$(this.nodes.root).tooltip({ 
				content: this._generateUi.bind(this),
				items: "div"
			});
		},
		
		hide: function(){
			$(this.nodes.root).removeClass("show");
		},
		
		// show a tooltip which lists all the notes
		open: function(){
			var content = this._generateUi();
		},
		
		// Remember that you wrote a  util API for a lot of this boiler-plate!
		_generateUi: function(){
			var dom = document.createElement("div");
			dom.className="notifications";
			
			var notes = this._notifications;
			var header = document.createElement("div");
			header.className="header";
			header.textContent="Unseen Changes";
			dom.appendChild(header);
			
			var body = document.createElement("div");
			header.className="body";
			dom.appendChild(body);
			
			for(var i=0;i<notes.length;i++) {
				var note = notes[i];
				var wrapper = document.createElement("div");
				wrapper.className="note-wrapper";
				
				var source = document.createElement("div");
				source.className="source";
				source.textContent = note.source.getName? note.source.getName() : note.source;
				wrapper.appendChild(source);
				
				var message = document.createElement("div");
				message.className="message";
				message.textContent = note.message;
				wrapper.appendChild(message);
				body.appendChild(wrapper);
			}

			return dom;
		}
	};
	
	return Notifier;
	
});