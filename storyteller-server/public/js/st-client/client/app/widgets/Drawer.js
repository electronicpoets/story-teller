// An openable (collapsible) tray which stores widgets
// The tray opens/closes from a titlebar with a smooth animation
// Once set, it contains a number of widgets which can be dragged into a live screen
// It takes a set of live, pre-canned widgets (with an "install profile") from a 
// metadata defnition
// The first use-cuse is a set of images
// Drawers can nest other drawers (providing horizontal scrolling).
// Content can either be a list (in which case there's a vertical scroller),
// or it can be a stack (like a stack of cards), with left/right scrolling 

// A drawer has a handle (the main drawer), several trays, and several content items for each tray
// Maintains two indexes - selected tray and selected content
// 

// 7April14
// A drawer should know who the current player is, because different players will save different
// presets, preferences, contents, etc. And the player is the interface through which content is added.
// So all I have to do is set the player, then let the drawer add content to the player. Zing!
define(["client/story/widgets/Picture", "util/ImageBank"], function(Picture, imageBank) {


    var Drawer = function(parentNode, content, name) {
        // always create collapsed
        this.parentNode = parentNode;
        this.name = name;
        this.contents = content;

        this.contentIndex = undefined;
        this.trayIndex = undefined;

        this.initDom();
    };

    Drawer.prototype = {
        initDom: function() {
            var content = this.activeTray;

            var handle = this.handleNode = document.createElement("div");
            handle.innerHTML = "<u>" + this.name + "</ul>";
            handle.className = "button";
            this.trayTitleNode = handle;
            this.parentNode.appendChild(handle);

            handle.onclick = function() {
                if (this._open) {
                    this.close();
                } else {
                    this.open();
                }
            }.bind(this);

            var tray = this.trayNode = document.createElement("div");
            tray.className = "drawer tray";

            var contentNode = this.contentNode = document.createElement("div");
            contentNode.className = "drawer-content";
            contentNode.onclick = function() {
                console.log("Clicked on drawer content!");
                this.insertCurrent();
            }.bind(this);
            tray.appendChild(contentNode);

            var left = document.createElement("div");
            left.className = "scroller left";
            left.textContent = "<-";
            $(left).click(function(evt) {
                evt.stopPropagation();
                this.contentLeft();
            }.bind(this));
            contentNode.appendChild(left);

            var right = document.createElement("div");
            right.className = "scroller right";
            right.textContent = "->";
            $(right).click(function(evt) {
                evt.stopPropagation();
                this.contentRight();
            }.bind(this));
            contentNode.appendChild(right);

            var selector = this.selector = document.createElement("div");
            selector.className = "selector";

            var previous = document.createElement("div");
            previous.className = "left";
            previous.textContent = "<-";
            $(previous).click(function(evt) {
                evt.stopPropagation();
                this.trayLeft();
            }.bind(this));
            selector.appendChild(previous);

            var label = this.trayLabelNode = document.createElement("div");
            label.className = "label";
            label.textContent = "(loading)";
            selector.appendChild(label);

            var next = document.createElement("div");
            next.className = "right";
            next.textContent = "->";
            $(next).click(function(evt) {
                evt.stopPropagation();
                this.trayRight();
            }.bind(this));
            selector.appendChild(next);

            tray.appendChild(selector);

            this.parentNode.appendChild(tray);
        },
        // TODO - go through the active player, not the controller
        insertCurrent: function() {
            // clone the selected drawer item
            var model = this._current.clone();

            // in practice, we want to scale only on the biggest dimension
            model.size = {
                height: 30
            };

            // insert the clone into the model
            window.controller.addContent(model);
            this.close();
        },
        open: function() {
            if (!this._open) {
                this._open = true;
                var tray = this.trayNode;
                var handle = this.handleNode;
                $([tray, handle]).addClass("open");

                tray.style.position = "absolute";

                tray.style.right = (document.body.offsetWidth - (handle.offsetLeft + handle.offsetWidth)) + "px";
                tray.style.top = (handle.offsetTop + handle.offsetHeight) + "px";

                this.selectContent(this.trayIndex || 0, this.contentIndex || 0);
            }
        },
        // probably want to destroy content when the tray is closed, there might be a lot of HTML here.
        // maybe keep the front picture, but lose all the widget boiler-plate
        close: function() {
            if (this._open) {
                this._open = undefined;
                $([this.trayNode, this.handleNode]).removeClass("open");
            }
        },
        // TODO - must be able to clean up these next four methods
        trayLeft: function() {
            var idx = this.trayIndex;
            var last = this.contents.length - 1;
            if (idx == 0) {
                idx = last;
            } else {
                idx--;
            }

            this.selectContent(idx, 0);
        },
        trayRight: function() {
            var idx = this.trayIndex;
            var last = this.contents.length - 1;
            if (idx == last) {
                idx = 0;
            } else {
                idx++;
            }

            this.selectContent(idx, 0);
        },
        contentLeft: function() {
            var idx = this.contentIndex;
            var last = this.activeTray.contents.length - 1;
            if (idx == 0) {
                idx = last;
            } else {
                idx--;
            }

            this.selectContent(this.activeTray, idx);
        },
        contentRight: function() {
            var idx = this.contentIndex;
            var last = this.activeTray.contents.length - 1;
            if (idx == last) {
                idx = 0;
            } else {
                idx++;
            }

            this.selectContent(this.activeTray, idx);
        },
        selectContent: function(trayIndex, contentIndex) {
            var tray;
            if (isNaN(trayIndex)) {
                trayIndex = this.trayIndex;
            } else {
                this.trayIndex = trayIndex;
            }

            var tray = this.activeTray = this.contents[trayIndex];
            // TODO - only update this when neccessary
            this.trayLabelNode.textContent = tray.name;

            if (isNaN(contentIndex)) {
                contentIndex = this.contentIndex;
            }

            var contentId = tray.contents[contentIndex];
            // hang on, is this even needed now?
           // var load = function(id, modelData) {
                //this._data = modelData;
                this.contentIndex = contentIndex;

                if (this._current) {
                    this._current.destroy();
                }

                var model = {
                    type: "picture", // not neccessary really...
                    content: contentId,
                    position: {
                        horizontal: "center"
                    },
                    // TODO - scale across the dimension here...
                    size: {
                        height: 100
                    }
                };

                var pic = this._current = new Picture(this.contentNode, model);
                pic.render();
//
//            }.bind(this);
//
//            var contentId = tray.contents[contentIndex];
//            imageBank.get(contentId, function(data){
//                load(contentId, data);
//            });
//            var data = tray.contents[contentIndex];
//            if (this._data != data) {
//                if (typeof data == "string") {
//                    $.get("/data/images/" + data, load);
//                }
//                else {
//                    load(data);
//                }
//            }
        }
    };

    return Drawer;

});