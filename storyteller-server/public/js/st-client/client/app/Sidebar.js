/* 
 * Provides a collapsible sidebar which allows navigation over one (or more)
 * embedded widgets
 */

define([], function(){
   
    // tell the sidebar which node should be converted into a sidebar
    // the width of this node will be controlled
    Sidebar = function(node){
        this.nodes= {
            root: node
        };
        
        node.style.width="0px";
        this.controls=[];
        this.init();
    };
    
    Sidebar.prototype = {
        
        init: function(){
            var root = this.nodes.root;
            root.style.visibility="visible";
        },
        
        expand: function(width){
            var root = this.nodes.root;
            root.style.width=width+"px";
            
            for(var i=0;i<this.controls.length;i++) {
                this.controls[i].show();
            }
        },
        
        collapse: function(){
            var root = this.nodes.root;
            root.style.width="0px";
            
            for(var i=0;i<this.controls.length;i++) {
                this.controls[i].hide();
            }
        },
        
        setTitle: function(){
            
        },
        
        // add a component to the sidebar
        // multiple components will trigger the craetion of a tab area
        add: function(control){
            this.controls.push(control);
            control.setParent(this.nodes.root);
        }
        
    };
    
    return Sidebar;
    
});
