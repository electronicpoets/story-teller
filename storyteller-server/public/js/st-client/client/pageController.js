// Class to handle the basic rendering and next/previous navigation of a story
// Interfaces between a story interface and the GUI
// Note that a single page controller may need to manage multiple players in the same page.
// Or, does each player have an independent PageController? I doubt that, since the story itself
// contains an instanced story state.
// JC 5April14
// A Controller must be loaded with a story first, and then have players added
// The first player defaults to the "active" player.
// Will likely want to add in more flexibility here
define(["client/story/Player",  "client/app/widgets/Notifier", "client/app/Sidebar",
         "client/edit/common", "client/edit/EditStory", "client/edit/Freestyle"],
        function(Player, Notifier, Sidebar, common, EditStory, FreestyleEditor) {

            var PageController = function(domNode, sidebarNode, story, session, spectateMode) {
                this.spectateMode = spectateMode;
                this._playerSelector = $("#playerSelect")[0]; // selector UI
                this._players = {}; // track all active players
                this.nodes = {
                    root: domNode,
                    sidebar: sidebarNode
                };

                this.sidebar = new Sidebar(sidebarNode);
                this.init(session);
                this.setStory(story);

                window.controller = this;
            };

            PageController.prototype = {
                panelMargin: 30,
                activePage: undefined,
                domNode: undefined,
                // get the left and right page positions (which may depend on page state)
                getLeftMargin: function() {
                    return this.panelMargin;
                },
                getRightMargin: function() {
                    return this.panelMargin;
                },
                getTopMargin: function() {
                    return this.panelMargin;
                },
                getBottomMargin: function() {
                    return this.panelMargin;
                },
                getWidth: function() {
                    return this.domNode.offsetWidth;
                },
                getHeight: function() {
                    return this.domNode.offsetHeight;
                },
                init: function(session) {
                    this.domNode = document.getElementsByTagName("body")[0];

                    this.nodes.title = $("#storyTitle .title")[0];
                    
                    // Prime the player selector
                    var select = this._playerSelector;
                    if (select) {
                        var self = this;
                        select.onchange = function() {
                            var newPlayerId = this.item(this.selectedIndex).value;
                            self.setActivePlayer(newPlayerId, true);
                        };
                        // Create a notification widget
                        this._notifier = new Notifier(select.parentNode);
                    }

                    // set up a websocket connection
                    this._session = session;

                    // listen for page size events
                    $(window).resize(this.refresh.bind(this));

                    // subscribe to local requests to update the command list
                    session.subscribe("commands.update", this.updateCommands.bind(this));

                    session.on("story.link.validated", this.onLinkValidated.bind(this));
                    
                    // subscribe to remote requests indicating a new player is connected
                    session.on("player.connected", function(evt) {
                        console.log(" -> Detected player connection event!");
                        console.log(evt)
                        // add this player
                        this.addPlayer(evt.id, {isOnline: true, name: evt.name})
                    }.bind(this));

                    //this.showSidebar();
                },
                toggleSidebar: function() {
                    if (this._sidebarOpen) {
                        this.hideSidebar();
                    } else {
                        this.showSidebar();
                    }
                },
                showSidebar: function() {
                    if (this.sidebar && !this._sidebarOpen) {
                        var width = this._sidebarWidth = 300;

                        this._sidebarOpen = true;
                        this.sidebar.expand(width);

                        var root = this.nodes.root;
                        root.style.left = width + 6 + "px";
                        this.refresh();

                        // update the left button tray position
                        if (this.leftTray) {
                            var tray = this.leftTray.nodes.root;
                            tray.style.left = (tray.offsetLeft + width) + "px";
                        }
                    }
                },
                hideSidebar: function() {
                    if (this.sidebar && this._sidebarOpen) {
                        this._sidebarOpen = false;
                        this.sidebar.collapse();

                        this.nodes.root.style.left = "0px";
                        // resize the page
                        this.refresh();

                        // update the left button tray position
                        if (this.leftTray) {
                            var tray = this.leftTray.nodes.root;
                            tray.style.left = (tray.offsetLeft - this._sidebarWidth) + "px";
                        }
                    }
                },
                setSidebarWidth: function() {

                },
                onStoryChanged: function() {
                    // tell each player that the story has hanged

                },

                // Set the active story instance
                setStory: function(story) {
                    this.story = story;
                    var titleNode = this.nodes.title;
                    // set the header title
                    $(titleNode.parentNode).addClass("show");
                    titleNode.textContent = story.title;
                },
                // 5April14 - always render from the page model, not the player?
                render: function(pageOrId, state, forceResize) {
                    if (pageOrId) {
                        var player = this._activePlayer;

                        var page = player.renderPage(typeof pageOrId == "string" ? pageOrId : pageOrId.id, state, forceResize);

                        return page;
                    }
                },
                // redraw the current page
                refresh: function(deltas) {
                    var player = this._activePlayer;
                    if (player) {
                        var pageModel = player.getPage();
                        var page = this.render(pageModel, deltas ? deltas[pageModel.id] : undefined, true);
                       //page._updatePosition(true);
                    }
                },
                // Set the current page to view for the active player
                // Automatically triggers a transition
                // Accepts a page id
                page: function(targetPageId, isForward, loadAll) {
                    // update the page index
                    var stale = {};
                    var player = this._activePlayer;
                    if (!player) {
                        console.log("ERROR :: no active player detected");
                        return;
                    }
                    
                    var toHide=[];
                    if(!isForward) {
                        // find every page between the target page and the current page which needs to be zapped
                        var page = this.activePage;
                        
                        var toHide =[page];
                        while(page) {
                            page = page.getPrevious();
                            if(page==targetPageId){
                                break;
                            }
                            if(page) {
                                page = player.getPageInstance(page);
                                toHide.push(page);
                            }
                        }
                    }
                    var pinstance;
                    while(toHide[0]) {
                        pinstance = toHide.shift();
                        //console.log("-> Hiding "+pinstance._model.title);
                        if(pinstance) {
                            pinstance.setState();
                        }
                    }
                    

                    if (targetPageId) {
                        player.setPage(targetPageId, isForward);
                        // if the page hasns't been rendered, render it as "next"
                        if(this.story.contentType=="Comic") {
                            this.render(targetPageId, "next");    
                        }
                    }
                    else if (this.activePage && isForward) {
                        player.markPageComplete(this.activePage._model.id);
                    }
                    
                    // first make sure the right pages are rendered
                    var activeModel = this.activePage = player.getPage();
                    if(!activeModel) {
                        // get the first page
                        // work-around for a state-tracking bug
                        var firstId = this.story.getFirstPageId();
                        activeModel = this.activePage = this.story.getPage(firstId);
                        player._currentPageId=firstId;
                        console.log("Warning :: couldn't load state page, defaulting to first page");
                    }
                    
                    // how many previous pages do we render?
                    var previous = player.getPreviousPage();
                    while(previous) {
                        this.render(previous.id, "previous");
                        previous = loadAll ? player.getPreviousPage(previous.id) : null;
                    }
                    
                   var active = this.activePage = this.render(activeModel, "active");
                   active.edit(this._editMode, player.getPageContent());
                    
                    var next = player.getNextPages();
                    if (next) {
                        for (var i = 0; i < next.length; i++) {
                            this.render(next[i], "next");
                        }
                    }

                    this.updateCommands();
                    // publish a local event
                    //this._session.publish("page.changed", { player: player, page: targetPage });
                },
                updateCommands: function(pageId) {
                  // tell teh active page to update  
                  var player = this._activePlayer;
                  var page = player.getPageInstance(pageId);
                  page.updateCommands();
                },

                // TODO - move into Page
                _clickHandler: function(link, button){
                    var player = this._activePlayer;
                    var session = this._session;
                    var activePageId = this.activePage._model.id;
                    if(!player.isStoryteller && link.freestyle && !player.isPageComplete()) {
                        var buttonRoot=button.nodes.root;
                        var events={
                            onAccept: function(message) {
                                var id =  link._id || link.id;
                                var evt = {
                                    id: id,
                                    input: {}
                                };
                                player.editLink(evt);
                                player.setWaiting(id, true);
                                player.markPageComplete(activePageId, id, undefined, message);
                                session.publish("commands.update");
                            },
                            onCancel: function(){
                                common.hideTT(buttonRoot);
                            }
                        };
                        
                        var content = FreestyleEditor.generate(this.story, events);
                        common.showTT(buttonRoot, content, "bottom-left");
                    }
                    // the link is invalid, which means nothing can happen on click
                    else if(!link.isValid && !player.isStoryteller) {
                        player.setWaiting(link.id, true);
                        player.markPageComplete(activePageId, link.id);
                        this.updateCommands();
                    } 
                   else  {
                        if(link.target)  {
                            player.setWaiting(link.id, false);
                            this.page(link.target, link.id); // don't like this isForward parameter...	
                        }
                    }
                },
                
                // enter edit-mode - switch all active widgets to edit mode,
                // and show the edit-time toolbar
                // return true if edit mode is enabled after this call
                toggleEdit: function(pageId, override) {
                    var enable;
                    if (typeof override == "boolean") {
                        enable = override;
                    } else {
                        enable = !this._editMode;
                    }
                    if (enable != this._editMode) {
                        this._editMode = enable;
                        var contents = this._activePlayer.getPageContent();
                        
                        var page = this._activePlayer.getPageInstance(pageId);
                        if(page) {
                            page.edit(enable, contents);
                        }
                        this.updateCommands();
                        //this.rightTray.edit(this._editMode);
                    }
                    return this._editMode;
                },
                saveStoryMetadata: function(deltas) {
                    var story = this.story;
                    for (var d in deltas) {
                        story[d] = deltas[d];
                    }

                    this._session.emit("story.changed", {story: deltas});
                },
                // save any changes made by the current player
                // note that while a save is in progress, we need to prevent any more saves
                save: function() {
                    var player = this._activePlayer;
                    var deltas = player.pushChanges();
                    // do an XHR POST on data/story/id
                    if (deltas) {
                        // lazy solution
                        var unknownIds = {};
                        var unknownTargets = {};
                        for (var pid in deltas) {
                            // Write temporary ids to any new content
                            var pageDeltas = deltas[pid];

                            var contentTypes = ["next", "contents"];
                            while (contentTypes[0]) {
                                var x = pageDeltas[contentTypes.shift()];
                                if (x) {
                                    for (var i = 0; i < x.length; i++) {
                                        var item = x[i];
                                        if (item.id[0] == "@") {
                                            // this is a new id
                                            unknownIds[item.id] = item;
                                        }
                                        if (item.target && item.target[0] == "@") {
                                            unknownTargets[item.target] = item;
                                        }

                                        // 12June14
                                        // also convert any new Picture types back to images
                                        if (item.content && item.content.key) {
                                            item.content = item.content.key;
                                        }
                                    }
                                }
                            }
                        }

                        var evt = {
                            playerId: player.id,
                            deltas: deltas
                        };

                        var story = this.story;
                        var session = this._session;
                        session.emit("story.changed", evt, false, function(data) {
                            console.log("Receiving updated ids:");
                            console.log(data);
                            for (var oldId in data) {
                                if (unknownIds[oldId]) {
                                    unknownIds[oldId].id = data[oldId];
                                }
                                if (unknownTargets[oldId]) {
                                    unknownTargets[oldId].target = data[oldId];
                                }
                            }
                            player.updateIds(data);
                            //this._updateIds(data);
                            this.updateCommands();
                        }.bind(this));
                        // also trigger a local event
                        //session.publish("story.changed", evt);
                    }
                },
//                _updateIds: function(newIds) {
//                    // bit nasty, update the ids in the commandtrays
//                    var trays = [ this.leftTray, this.rightTray];
//                    for(var i=0;i<trays.length;i++) {
//                        var t = trays[i];
//                        for(var id in t._idx) {
//                            var current = t._idx[id];
//                            var x = newIds[current.model.target];
//                            if(x) {
//                                var m = t._idx[x];
//                                if(m.model) {
//                                    m.model.target=x;
//                                }
//                                t._idx[x] = m;
//                                
//                                t._idx[id] = undefined;
//                                delete t._idx[id];
//                            }
//                        }
//                    }
//                },
                isEditMode: function() {
                    return this._editMode;
                },
                getPage: function(pageId) {
                    if(pageId) {
                        return this.story.getPage(pageId);
                    }else {
                        return this.activePage;
                    }
                },
                getPlayerName: function(playerId) {
                    var player = this._players[playerId];
                    if (player) {
                        return player.name;
                    }
                },
                addPlayer: function(playerId, options) {
                    var player = this._players[playerId];
                    if (!player) {
                        options = options || {};
                        // all player pages go here
                        var dom = document.createElement("div");
                        dom.className = "pageContainer";

                        // nasty... 
                        if(this.story.contentType=="Prose") {
                            dom.style.padding="0px 20px";
                        }

                        this.nodes.root.appendChild(dom);
                        //document.getElementsByClassName("storyboard")[0].appendChild(dom);
                        // JC 22Sept14 - quick and dirty, disable current page state if in spectate mode
                        //                 (in practice, the current page needs to be saved to the spectator
                        if(this.spectateMode && options.state) {
                            options.state._current=undefined;
                        }
                        var args = $.extend({
                            id: playerId,
                            story: this.story,
                            domNode: dom,
                            session: this._session
                        }, options);
                        var player = new Player(args);

                        this._players[playerId] = player;
                        dom.setAttribute("id", playerId);

                        if (options.isPrimary) {
                            this._primaryPlayer = player;
                            this._session.publish("player.primary", player);
                        }
                        if(options.storyteller){
                            dom.className += " storyteller"; 
                            this.hasStoryteller=true;
                        }

                        player.setNotifier(this._notifier);

                        if(!this.spectateMode ||
                                !(player.isStoryteller || player.isSpectator)) {
                                    
                            var select = this._playerSelector;
                            if (select) {
                                var opt = document.createElement("option");
                                opt.value = player.id;
                                opt.textContent = player.name + (player.isStoryteller ? "*" : "");
                                select.appendChild(opt);
                            }

                            var isActive;
                            if (/*!this._activePlayer ||*/ options.isPrimary) {
                                this.setActivePlayer(player);
                                isActive = true;
                            }
                            this._positionPlayerContainer(player, isActive, false);
                            // publish a local event with the new player data
                            this._session.emit("player.added", player, true);
                        }
                    }
                    return player;
                },
                removePlayer: function() {

                },
                getPlayer: function(playerId){
                    return this._players[playerId];
                },
                getPlayers: function() {
                    return this._players;
                },
                // set the player controlling this controller
                setActivePlayer: function(player, animate) {
                    if (typeof player == "string") {
                        player = this._players[player];
                    }

                    if (!player) {
                        return;
                    }

                    var activeId = this._activePlayer ? this._activePlayer.id : undefined;
                    if (player.id != activeId) {
                        // disable the inactive container
                        var oldPlayer, oldDom;
                        if (activeId) {
                            //oldDom = this._players[activeId].storyboard;
                            oldDom = this._players[activeId].domRoot;
                            oldPlayer = this._activePlayer;
                            $(oldDom).removeClass("active");
                            //this._scrollOut(oldDom);
                        }

                        // TODO - make sure the select widget is updated
                        // 		low priority since this is the only contorl we have atm
                        if (this._activePlayer) {
                            this._activePlayer.setActive();
                        }
                        this._activePlayer = player;
                        this.enableEditMode(player.isStoryteller ? true : false);
                        player.setActive(true);

                        var newDom = this._players[player.id].domRoot;
                        $(newDom).addClass("active");
                        // this._scrollIn(newDom);

                        // reset the page cache and rendered pages
                        // temporary measure to ensure behaviour is correct
                        $("#" + player.id + " .page").removeClass("next previous active");

                        this.page(undefined, false, this.story.contentType=="Prose");
                        if (oldDom) {
                            this._positionPlayerContainer(oldPlayer, false, animate);
                        }
                        this._positionPlayerContainer(player, true, animate);
                    }
                },
                _positionPlayerContainer: function(player, isActive, animate) {
                    var dom = this._players[player.id].domRoot;
                    var pageHeight = dom.offsetHeight || 500; // take a punt at the first height, just so that we're moving something
                    var margin = 0; //this.getTopMargin();
                    var top, bottom;

                    if (isActive) {
                        top = bottom = margin;
                    } else {
                        var activeId = this._activePlayer ? this._activePlayer.id : undefined;
                        var isBefore = false;
                        var d = dom;
                        while (d) {
                            if (d.getAttribute("id") == activeId) {
                                isBefore = true;
                                break;
                            }
                            d = d.previousElementSibling;
                        }

                        var offset = pageHeight - (margin * 2);
                        if (isBefore) {
                            top = offset;
                            bottom = -offset;
                        } else {
                            top = -offset;
                            bottom = offset;
                        }
                    }
                    this._scroll(dom, top, bottom, animate);
                },
                _scroll: function(div, top, bottom, animate) {
                    if (animate) {
                        $(div).addClass("animate");
                    } else {
                        $(div).removeClass("animate");
                    }

                    $(div).css({
                        top: top + "px",
                        bottom: bottom + "px"
                    });
                },
//		_scrollOut: function(div){
//			var ht = this.domNode.scrollHeight + 20;
//			$(div).css({
//				top: ht+"px"
//			})
//		},

                enableEditMode: function(override) {
                    var enable = typeof override == "boolean" ? override : !this._enableEditMode;
                    if (enable != this._enableEditMode) {
                        if (enable) {
                            $(".edit-toolbar").addClass("enable");
                        } else {
                            $(".edit-toolbar").removeClass("enable");
                        }

                        this._enableEditMode = enable;

                        if (this._editMode && !enable) {
                            this.toggleEdit(this.activePage._model.id, false);
                        } /*else if(override) {
                         this.toggleEdit(true);
                         }*/
                    }
                },
                addContent: function() {
                    this._activePlayer.addContent.apply(this._activePlayer, arguments);
                    this.refresh();
                },
                // Content in the page has changed
                // Inform the active player of the changes
                // Takes an argument, representing changes to a model (as k/v pairs)
                // MUST include the content id
                onContentChanged: function(modelDeltas) {
                    this._activePlayer.updateContent(modelDeltas.id, modelDeltas);
                },
                // content int he page has been removed
                // Note: not at all sure about either edit or delete handlers
                onContentRemoved: function(removedContent) {
                    this._activePlayer.removeContent(removedContent);
                },
                getResponses: function(isPrevious) {
                    var player = this._activePlayer;

                    var pageId;
                    if (isPrevious) {
                        var page = player.getPreviousPage();
                        if (page) {
                            // TODO 30Jun14 fix this - resolve the id/_id conflict. Both values are different.
                            pageId = page._id || page.id;
                        }
                    } else {
                        pageId = this.activePage._model.id;
                    }

                    var results = [];
                    if (pageId) {
                        // convert each link ID to the real link..
                        //var responses = player.getResponseData(pageId);
                        var responses= this.story.getResponses(pageId);
                        var page = this.story.getPage(pageId);
                        if (responses) {
                            for(var playerId in responses) {
                                var resp = responses[playerId];
                                var player = this._players[playerId];
                                for (var i = 0; i < page.next.length; i++) {
                                    var link = page.next[i];
                                    if (link.id == resp.target) {
                                        var data = {playerId: playerId, linkName: link.title, playerName: player.name, timestamp:resp.timestamp};
                                        if(resp.message) {
                                             data.message = resp.message;
                                        }
                                        results.push(data);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return results;
                },
                // edit story data through a pop-up dialog
                editStory: function(target) {
                    if(this._activePlayer.isStoryteller) {
                        var close = function(){
                            this._storyEditorOpen = false;
                            common.hideTT(target);
                        }.bind(this);
                        
                        if(this._storyEditorOpen) {
                            close();
                        }else {
                            this._storyEditorOpen = true;
                            var session=this._session;
                            var titleNode = this.nodes.title;
                            var story = this.story;
                            var events = {
                                onAccept: function(model){
                                    var evt = {
                                        story: model
                                    };
                                    if(model.title) {
                                        titleNode.textContent = story.title;
                                    }
                                    story.update(model);
                                    var oldId = story.id;
                                    session.emit("story.changed", evt, false, function(newStory){
                                        if(newStory.id!=oldId) {
                                            // make sure the path is correct
                                            window.history.pushState("string", "Title", "/story/"+newStory.id);
                                        }
                                    });
                                    close();
                                },
                                onClose: close
                            };
                            var content = EditStory.generate(this.story, events);
                            common.showTT(target, content);
                        }
                    }
                },
                
                onLinkValidated: function(evt){
                    this.story.onLinkValidated(evt);
                    this.updateCommands();
                }

            };


            return PageController;
        });