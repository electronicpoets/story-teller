define([], function(){
	
	var keys = {
		
		// keys for content types
		content: {
			LINK: "next",
			PAGE: "page",
			CONTENT: "contents" 
		},
	
		// keys for changes to content
		changes: {
			ADD: "add",
			EDIT: "edit",
			REMOVE: "remove"
		}
	
	};
	
	// lock the object
	
	return keys;
});