/*
 * A chat session manages a chat interface to the server,
 * providing notifications of messages to any listening UIs
 * 
 * Interesting thought: is there any downside to maintaining two
 * websockets, one for chat and another for story changes?
 * 
 * I think in practice, there will be one socket (associated with the page address),
 * with different messages: 
 * 		 * chat.player.joined, chat.player.left, chat.message.received
 * 		 * story.content.added, story.content.changed, story.page.added, story.turn.ended etc
 * 
 * So obviously this becomes a generic IO service, rather than a chat service.
 * 
 * What about sending messages across the client? Am I hand building a little event bus,
 * or is there something off-the-shelf that I can use?
 */

define(["socketio", "chat/events"], function(io, iEvents){
	
	var Session = function(storyId){
		iEvents.mixin(this);
		this.connect(undefined, storyId);
	}; 
	Session.prototype={
		
		connect: function(userId, storyId){
			if(io) {
				console.log("* connecting to "+storyId);
				// NOTE: I can't seem to connect to a particular URL, I have to connect to root. Weird.
				
				var sio = io.connect(/*"/story/"+storyId*/);
				console.log(sio);
				var socket = this._socket = sio; //.in(storyId);
				
				// JC since the handshake won't work ion the server,
				// try a manual handshake. It's crude, but I'm not sure
				// what else to do here
				var intId = setInterval(function() {
					console.log("Socket pinging for connection...");
					socket.emit("register", {
						storyId: storyId,
						playerId: userId // how can I find out the active player?
					});
				}, 100);
				
				socket.on("connected", function(){
					console.log("Socket connected!");
					if (intId) {
						clearTimeout(intId);
					}
				});
			}
		},
		
		// send an event to the server
		// do we also publish client-side? Don't think so. Maybe we take a flag.
		emit: function(event, data, local, callback){
                    var socket = this._socket;
                    //socket.emit(event, data, callback);    
                    if (local) {
                        this.publish(event, data);
                    } else {
                        socket.emit(event, data, callback);
                    }
		},
		
		// subscribe to an event on the websocket
		on: function(event, callback, local){
                    var socket = this._socket;
                    if (local) {
                        this.subscribe(event, callback);
                    } else {
                        socket.on(event, callback);    
                    }
		}
	};
	
	return Session;
	
	
});