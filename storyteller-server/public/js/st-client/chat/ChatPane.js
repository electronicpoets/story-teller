/* 
 * Interface which hosts both a player list and chat room.
 * 
 * May evolve for more functionality, like a toolbar
 * 
 */


define(["chat/PlayerList", "chat/ChatRoom", "util/domUtils"], function(PlayerList, ChatRoom, dom){
    
    ChatPane = function(controller, session, chatId){
        this.controller = controller;
        this.session = session;
        this.chatId = chatId;
    };
    
    ChatPane.prototype = {
        
        init: function(){
           this._controls=[];
           var root = dom.create({class:"chatPane", parentNode: this.nodes.parent});
            
           var playerList = new PlayerList(this.controller, this.session, root);
           playerList.show(); // TODO show the widget a little more intelligently
           
           var c = this.controller;
           var chat = this.chatroom = new ChatRoom(this.controller, this.controller._session, this.chatId, root, c._primaryPlayer||c._activePlayer);
           //chat.show();
           
           this._controls.splice(0,0,playerList, chat);
        },
        
        setParent: function(parentNode){
            this.nodes= {
                parent: parentNode
            };
            
            this.init();
        },
        
        show: function(){
            for(var i=0;i<this._controls.length;i++) {
                this._controls[i].show();
            }
        },
        
        hide: function(){
             for(var i=0;i<this._controls.length;i++) {
                this._controls[i].hide();
            }
        }
    };
    
    return ChatPane;
    
})

