/*
 * A chat notifier is a button which opens up a chat room instance.
 * 
 * It can be resized to several states (?) and prints how many players and unread
 * messages there are.
 */

define(["util/domUtils", "chat/events"], function(domUtils, iEvents){
	
    var ChatNotifier = function(parentNode, session, chatroom){
        iEvents.mixin(this);
        this._nots = 0; // track notifications
        this._chatroom=chatroom;
        
        this.initDom(parentNode);
        if(session) {
            this.hookToSession(session);
        }
    };

    ChatNotifier.prototype={

            initDom: function(parentNode){
                this.nodes = {
                    parent: parentNode
                };

                var dom = this.nodes.root = domUtils.create({class:"chatNotifier", parentNode: parentNode});
                domUtils.create({type: "span", class:"icon", parentNode: dom});	
                domUtils.create({type: "span", class: "label", text: "Players", parentNode: dom});
                this.nodes.playerCount = domUtils.create({type:"span", class:"playerCount", parentNode: dom});
            },

            hookToSession: function(session){
                this._session = session;

                this._session.on("players.count", function(evt){
                    console.log("ChatNotifier :: receiving player count update...");
                    this._updatePlayerCount(evt.count);
                }.bind(this));
                
                var chatroom = this._chatroom;
                this._session.on("chat.message", function(evt){
                    console.log("ChatNotifier :: Received chat message!");
                    if(!chatroom.isVisible()) {
                        this.addNotification();
                    }
                }.bind(this));
                
                this._session.on("chat.visibilityChanged", this.onChatroomVisibilityChanged.bind(this), true);
            },
            
            addNotification: function(){
                this._nots++;
                var msg = "You have "+this._nots+" new message" + (this._nots>1?"s!":"!");
                domUtils.addTooltip(this.nodes.root, msg, true);
            },
            
            clearNotifications: function(){
                this._nots=0;
                domUtils.removeTooltip(this.nodes.root);
            },
            
            onChatroomVisibilityChanged: function(){
                if(this._chatroom.isVisible()){
                      this.clearNotifications();
                }
            },

//            open: function(){
////                if(this._chatroom) {
////                    this._chatroom.open();
////                }
//                this.clearNotifications();
//            },

            onPlayerJoined: function(){
                this._updatePlayerCount();
            },

            onPlayerLeft: function(){
                this._updatePlayerCount();
            },

            _updatePlayerList: function(data){
                var count = data.players.length;

                var playerCount = this.nodes.playerCount;
                // count the number of connected players and update the UI
                playerCount.textContent="("+count+")";
            },

            _updatePlayerCount: function(count){
                var playerCount = this.nodes.playerCount;
                // count the number of connected players and update the UI
                playerCount.textContent="("+count+")";
            }
    };

    return ChatNotifier;
});