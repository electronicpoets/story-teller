define(["util/domUtils", "util/TimeStamp"], function(dom, timestamp){
    
    ChatRoom = function(controller, session, chatId, parentNode, primaryPlayer) {
        this.nodes = {
            parent: parentNode
        };
        this.controller=controller;
        this.session=session;
        if(!session) {
            console.log("ERROR :: no session detected for chat!!");
        } 
        this._player=primaryPlayer;
        
        this.init(chatId);
    };
    
    ChatRoom.prototype = {
        
        init: function(chatId){
            var root = this.nodes.root = dom.create({class: "chatRoom", parentNode: this.nodes.parent});
           
            dom.create({text:"Chat", class:"title", parentNode: root});
            this.nodes.messages = dom.create({class:"messages", parentNode: root});
            
            var inputWrapper = dom.create({class:"input-wrapper", parentNode: root});
            var row = dom.create({class:"row", parentNode: inputWrapper});
            var cell = dom.create({class:"cell input", parentNode: row});
            var input = this.nodes.input = dom.create({type: "input", parentNode: cell});
            input.onkeypress=function(evt){
                var code = evt.key || evt.keyIdentifier;
                if(code=="Enter") {
                    this.send();
                }
            }.bind(this);
            
            cell = dom.create({class:"cell submit", parentNode: row});
            var button = dom.create({type: "button", text: "->", parentNode: cell});
            button.onclick=this.send.bind(this);
            
            var msgRoot = this.nodes.messages;
            if(chatId) {
                this._chatId=chatId;
                msgRoot.textContent="Loading chat... ";
            } else {
                 this._empty = true;
                 $(msgRoot).addClass("empty");
                 msgRoot.textContent="There are no chat messages yet."
            }
            
            this.initSession();
            this.onSizeChanged();
        },
        
        _loadHistory: function(){
            $.getJSON("/data/chat/"+this._chatId, function(chatHistory){
                this._loaded=true;
                var root = this.nodes.messages;
                root.innerHTML="";
                var messages = chatHistory.messages;
                // sort by timestamp... ?
                for(var i=0;i<messages.length;i++) {
                    this.receiveMessage(messages[i]);
                }
                root.scrollTop=$(root.lastChild).offset().top;
            }.bind(this));
            
        },
        
        initSession: function(){
           var session = this.session;
           if(session) {
                session.on("chat.message", function(m){
                    this.receiveMessage(m, true);
                }.bind(this));
            }
        },
        
        // scale the chat window to fill the available space
        // Not prepared to waste any more time doing this with css...
        onSizeChanged: function(){
            var root = this.nodes.root;
            
            // bit sloppy - look to the container height, not the panel height
            // (because its fixed)
            var parentHeight = root.parentNode.parentNode.offsetHeight;
            var actualHeight = 0;
            var node = root.parentNode.firstChild;
            while(node) {
                actualHeight += node.offsetHeight;
                node = node.nextSibling;
            }
            var messages = this.nodes.messages;
            actualHeight -= messages.offsetHeight;
            
            // now calculate the available space (minus a margin? magic number?)
            var messageHeight = parentHeight - actualHeight - 8;
            messages.style.height=messageHeight+"px";
            
            messages.scrollTop = messages.scrollHeight;
        },
        
        // hook to the session
        // chat.newMessage
        // chat.connect
        // chat.disconnect
        connect: function(){
            var session = this.session;
            
        },
        
        show: function(){
            if(!this._loaded) {
                this._loadHistory();
            }
             
            $(this.nodes.root).addClass("show");
            this._isOpen = true;
            this.onSizeChanged();
            this.session.publish("chat.visibilityChanged");
        },
        
        hide: function(){
            $(this.nodes.root).removeClass("show");
            this._isOpen = false;
            this.onSizeChanged();
            this.session.publish("chat.visibilityChanged");
        },
        
        isVisible: function(){
            return this._isOpen;  
        },
        
        // a message can be a system message, which has sepecial styling
        receiveMessage: function(messageData, animate){
            var root = this.nodes.messages;
            if(this._empty){
                $(root).removeClass("empty");
                root.innerHTML="";
                this._empty = false;
            } /*else {
                root.lastChild.removeAttribute("id");
            }*/
            
            var isSelf = messageData.author == this._player.id;
            var wrapperClass= "message-wrapper";
            if(isSelf) {
                wrapperClass+=" self";
            }
            var prev = this._lastMessage;
            var day = moment(messageData.timestamp).date();
            var isDivided;
            if(!prev || moment(prev.timestamp).date()!=day) {
                var divider = dom.create({class:"message-divider ", parentNode: root});
                timestamp.date(messageData.timestamp, divider);
                isDivided=true;
            }
            if(isDivided || prev.author!=messageData.author) {
                $(wrapper).addClass("delta");
                var authorName = this.controller.getPlayerName(messageData.author);
                dom.create({text: authorName, class:"author", parentNode: root});
            }
            
            var wrapper = dom.create({class: wrapperClass, parentNode: root});
            var meta = dom.create({class:"meta-wrapper", parentNode: wrapper});

            
            //var time = moment(messageData.timestamp).time();
            
            timestamp.time(messageData.timestamp, meta);
        
            var msg = dom.create({text: messageData.content, class:"message", parentNode: wrapper});

            var parent = wrapper.parentNode;
            var clientTop = parent.scrollHeight - parent.clientHeight;
            var wrapperTop  = $(wrapper).offset().top;
            var targetScroll = Math.max(wrapperTop, clientTop);
            if(animate) {
                // add a "glow in" class, then after 3 seconds, remove it
                $(wrapper).addClass("glow in");
                $(root).animate({scrollTop:targetScroll}, 500);
                setTimeout(function() {
                   $(wrapper).removeClass("glow in"); 
                   $(wrapper).addClass("glow out");
                }, 1500);
            }
            
            this._lastMessage = messageData;
        },
        
        send: function(){
            var input = this.nodes.input;
            var content = input.value;
            this.sendMessage(content);
            input.value="";
        },
        
        // a sent message is instantly cleared from the input, but won't
        // display in the history until the server's seen it.
        // Maybe. This is a crap implementaton, but easy
        sendMessage: function(message){
            if(message && message.length>0) {
                var session = this.session;
                var evt = {
                    timestamp: new Date(),
                    content: message,
                    author: this._player.id
                };

                var session = this.session;
                session.emit("chat.message", evt);
             }
        }
        
    };
    
    return ChatRoom;
    
})