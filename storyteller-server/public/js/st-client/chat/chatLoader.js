/*
* Util script to bootstrap a chat engine
* 
* Assumes an element called "chatRoot" in which to place the notifier, and works
* the rest out from there.
*/

define(["chat/ChatNotifier", "chat/ChatPane", "socket/Session"], function(Notifier, Pane,Session){
	
    return {
        initChat: function(session, controller, chatId){
            var parent = document.getElementById("chatRoot");

                // tmp
            if(parent) {
                parent.onclick=function(){
                    window.controller.toggleSidebar();
                };
            }    

            // also create a UI in the sidebar
            if(controller && controller.sidebar) {
                var pane = new Pane(controller, session, chatId);
                controller.sidebar.add(pane);
                
                var notifier = new Notifier(parent, session, pane.chatroom);
            }
        }
    };
	
});