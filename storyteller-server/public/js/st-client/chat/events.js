/*
 * Mixin to add an eventing API
 *
 * Uses amplify in the short-term, but I think i'd
 * prefer something instance-based, not global
 * 
 */

define(["/js/lib/amplify-1.1.2/amplify.js"], function(){
  
  var mixin = {
    
    publish: function(){
      amplify.publish.apply(this, arguments);
    },
    
    subscribe: function(){
      amplify.subscribe.apply(this, arguments);
    }
    
  };
  
  var exports={
    mixin: function(target){
      target.publish=mixin.publish;
      target.subscribe=mixin.subscribe;
    }
  };
  
  return exports;
  
});