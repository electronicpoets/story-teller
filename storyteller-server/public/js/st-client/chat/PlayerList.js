/* 
 * Lists the players in the current game, including their status (Online, Idle, Offline)
 * 
 */

define(["util/domUtils"], function(dom) {

    PlayerList = function(controller, session, parentNode) {
        this.nodes = {
            parent: parentNode
        };
        this.controller = controller;
        this.session = session;
        this._players = {};
        this._domIndex = {};

        this.init();
    };

    PlayerList.prototype = {
        init: function() {
            var root = this.nodes.dom = dom.create({class: "playerList", parentNode: this.nodes.parent});

            dom.create({text: "Player List", class: "title", parentNode: root});
            this.nodes.players = dom.create({class: "players", parentNode: root});
            this.initSession();
        },
        initSession: function() {
            var session = this.session;
            if (session) {
                session.on("player.connected", function(evt) {
                    var player =this.controller.getPlayer(evt.id);
                    player.isOnline=true;
                    this._updatePlayerStatus(player, player._currentPageId);
                }.bind(this));

                session.on("player.disconnected", function(evt) {
                    var player =this.controller.getPlayer(evt.id);
                    player.isOnline=false;
                    this._updatePlayerStatus(player);
                }.bind(this));

                var updatePage = function(evt) {
                    var player = evt.player || this.controller.getPlayer(evt.playerId);
                    this._updatePlayerStatus(player, evt.pageId);
                }.bind(this)

                session.on("player.page", updatePage);
            }
        },
        show: function() {
            var players = this.controller.getPlayers();
            for (var p in players) {
                this._addPlayer(players[p]);
            }
        },
        hide: function() {

        },
        // WARNING: disparity as to whether I connect with a name or an ID
        _addPlayer: function(player) {
            if (!this._domIndex[player.id]) {
                var root = this._domIndex[player.id] = dom.create({class: "playerRoot", parentNode: this.nodes.players});

                dom.create({text: player.name || player.id, class: "playerName", parentNode: root});
                var stClazz = "storyteller";
                if (player.isStoryteller) {
                    stClazz += " show";
                }

                // show the current page
                root.pageNode = dom.create({class: "pageName", parentNode: root, tooltip: "If the player is online, This shows which page the player is currently on"});
                var controller = this.controller;
                
                // Note: this is cute, but it's possible a player might be able to step to an unpublished page this way
                if(controller.hasStoryteller) {
                    root.pageNode.onclick=function(){
                        controller.page(this._currentPageId);
                    }.bind(player);
                }

                dom.create({class: stClazz, parentNode: root, tooltip: "This player is the Storyteller"});
                root.statusNode = dom.create({parentNode: root, tooltip: "Loading..."});
                return root;
            }
            this._updatePlayerStatus(player, player._currentPageId);
        },
        _getStatusData: function(status) {
            var res = {
                class: "fi playerStatus",
                tt: "This player is currently "
            };
            if (status == "connected") {
                res.class += " online";
                res.tt += "online";
            } else {
                res.class += " offline";
                res.tt += "offline";
            }

            return res;
        },
        _updatePlayerStatus: function(player, pageId) {
            var root = this._domIndex[player.id];
            if (!root) {
                root = this._addPlayer(player);
            }

            var newStatus = player.isOnline ? "connected" : "disconnected"

            var data = this._getStatusData(newStatus);
            var statusDom = root.statusNode;
            statusDom.className = data.class;
            //statusDom.setAttribute("title", data.tt);
            dom.addTooltip(statusDom, data.tt);

            var pageNode = root.pageNode;
            if (player.isOnline) {
                $(pageNode).removeClass("hide");
                var page = this.controller.getPage(pageId);
                pageNode.textContent = page.title;
                // on click - go to that page?

            } else {
                $(pageNode).addClass("hide");
            }
        }
    };

    return PlayerList;

});