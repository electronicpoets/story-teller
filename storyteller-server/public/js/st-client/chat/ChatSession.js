/*
 * A chat session manages a chat interface to the server,
 * providing notifications of messages to any listening UIs
 * 
 * Interesting thought: is there any downside to maintaining two
 * websockets, one for chat and another for story changes?
 * 
 * I think in practice, there will be one socket (associated with the page address),
 * with different messages: 
 * 		 * chat.player.joined, chat.player.left, chat.message.received
 * 		 * story.content.added, story.content.changed, story.page.added, story.turn.ended etc
 * 
 * So obviously this becomes a generic IO service, rather than a chat service.
 * 
 * What about sending messages across the client? Am I hand building a little event bus,
 * or is there something off-the-shelf that I can use?
 */

define(["socketio", "chat/events"], function(io, iEvents){
	
	var Session = function(){
		iEvents.mixin(this);
		this.connect();
	};
	
	Session.prototype={
		
		connect: function(){
			if(io) {
				//console.log("* connecting to "+targetUrl);
				// NOTE: I can't seem to connect to a particular URL, I have to connect to root. Weird.
				var socket = io.connect();
				
				socket.on("players.count", function(data){
					console.log("Number of players: "+data.count);
					this.publish("players.count", data);
				}.bind(this));
			}
		},
		
			
	};
	
	return Session;
	
	
});