// an abstract model of a story
var story = {
	title: "", // the title of this story
	players: 1 // the number of players this story is designed for
};