/* 
 * ImageBank caches images in local storage. There's a big question mark over this:
 * am I better off relying on the brower's own image caching?
 * 
 * Images which aren't present are fetched from the server.
 * 
 * This a single static class. Note that it is coupled with Storyteller image content,
 * so it stores and indexed by image id.
 * 
 * Also note that it returns images as encoded base64 strings.
 * 
 */

define([], function(){

    // init - connect to local db
    
    var exports={
        
        basePath: "/data/images/",
        isDebug: false,
        
        _cache: {},
        
        _listeners: {},
        
        // pre-load a number of assets
        
        // is this wise?
        preload: function(images){
            while(images[0]) {
                this.get(images.shift());
            }
        },
        
        // set an image
        set: function(id, imageData) {
            this._cache[id]=imageData;
        },
        
        get: function(id, callback){
            // first check the local cache
            if(this._cache[id]) {
                //console.log("ImageBank :: Returning '"+id+"' from local cache");
                callback(this._cache[id]);
            } 
            // TODO - if the image is loading, defer
            else {
                var deffered = this._listeners[id];
                if(!deffered) {
                    deffered = this._listeners[id] = [callback];
                    this._load(id, function(data){
                        var value = this._cache[id] = this.isDebug ? data.url : data.bin;
                        this.onLoaded(id, value||data.url);
                    }.bind(this));
                } else {
                    deffered.push(callback);
                }
            }
        },
        
        _load: function(id, callback){
             $.get(this.basePath + id, callback);
        },
        
        onLoaded: function(id, data){
            var deffered = this._listeners[id];
            while(deffered[0]) {
                deffered.shift()(data);
            }
            this._listeners[id]=undefined;
            delete this._listeners[id];
        }
        
          // get the real model data and then re-generated the dom
//            $.get("/data/images/" + model, function(modelData) {
//                console.log(modelData);
//                this._model.content = modelData;
//                this.generateDom();
//            }.bind(this))
        
    };
    
    return exports;
    
});


