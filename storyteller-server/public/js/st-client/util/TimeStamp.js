/*
 * General utility class to print a timestamp on screen
 * 
 * Time stamp will show:
 * 
 * Day, Time, Zone
 * 
 * All rendered times can show different zones
 */

define(["util/domUtils", "moment"], function(dom, moment){
    
    var exports = {
        
        stamp: function(date, parent){
            var time = this.getFullDate(date);
            return dom.create({text: time, class:"timestamp", parent:parent});
        },
        
        time: function(date, parent){
            var time = this.getTime(date);
            return dom.create({text: time, class:"timestamp", parent:parent});
        },

        date: function(date, parent){
            var time = this.getDate(date);
            return dom.create({text: time, class:"timestamp", parent:parent});
        },
        
        getString: function(date){
            return this.getDate(date);
        },
        
        getFullDate: function(date){
            return moment(date).calendar();
        },
        
        getDate: function(date){
            //return moment(date).fromNow("d"); 
            return moment(date).format("dddd Do MMM [']YY");
        },
        
        getTime: function(date){
            return  moment(date).format("HH:mm");
        }
        
    };
    
    return exports;
    
});
