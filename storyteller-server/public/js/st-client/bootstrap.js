// Bootstrapping for the Storyteller loaded through Keystone

var load = function(storyJson){
    
  require(["socket/Session", "client/story/Story", "client/pageController", "client/app/widgets/Drawer", 
          "chat/chatLoader", "client/app/widgets/ResponseDialog", "client/app/widgets/Notifier",
            "client/gallery/Gallery"],
          function(Session, Story, PageController, Drawer, chat, ResponseDialog, Notifier, Gallery){
          
      // tmp
      var lid=0;
      window.generateLinkId=function(){
          return "@l#"+(lid++);
      };
      
      var story = new Story(storyJson);
      
      
      var editNode = $("#editControls")[0];
      if(editNode) {
  //      var office = [ {
  //          name: "Assets",
  //          contents: ["bransky-1", "bransky-2", "bransky-3", "branskys-arm", "sqwee-kee-klean-bottle"]
  //      }];
  //      new Drawer($("#editControls")[0], office, "Streak Free");

        if(storyJson.contentType!="Prose") {
            var characters = [{
                name: "Critters",
                contents: ["sad-rabbit", "troll", "centaur"]
            }, {
              name: "Ahella",
              contents: ["ahella-1", "ahella-2", "ahella-3"]
            },{
              name: "Geralt",
              contents: ["geralt", "geralt-2", "geralt-3", "geralt-portrait"]
            }];
            new Drawer($("#editControls")[0], characters, "Characters");
        }
      }
      else {
           $(".storyboard").addClass("noToolbar");
           $(".sidebar").addClass("noToolbar");
      }

      // set up a websocket connection
      var session = new Session(story.id);
      story.setSession(session);
      
      var storyboardNode = document.getElementsByClassName("storyboard")[0];
      var sidebarNode = document.getElementsByClassName("sidebar")[0];
      window.controller = new PageController(storyboardNode, sidebarNode, story, session, window.spectateMode);

      // Note: changes in how I'm handling player loading might enable me to drop this soon...
      var players = window.playerList;
      var playerNav = document.getElementById("playerUi");
      if (players) {
        for(var pid in players) {
            var p = players[pid];
            var options={
                storyteller: p.isStoryteller,
                name: p.name,
                state: p.state,
                // JC 21Sept14 Darned messs this is becoming - need a permissions system...
                isOnline: p.isOnline,
                isPrimary: p.isPrimary,
                isAdmin: p.isAdmin,
                isSpectator: p.isSpectator
            };
            
            controller.addPlayer(pid, options);
        }
      } else {
        if (playerNav) {
          controller.addPlayer("Storyteller", {storyteller: true});
        }
        controller.addPlayer("Player 1", {});
      }
      // make sure an active player is actually set (hack for spectate mode)
      if(!controller._activePlayer) {
          for(var p in controller._players) {
              var player = controller._players[p];
              if(!(player.isStoryteller||player.isStoryteller)) {
                  break;
              }
          }
          controller.setActivePlayer(controller._players[p]);
      }
      
      if (chat) {
        chat.initChat(session, controller, window.chatId);
      }
      
      var b = document.getElementById("responseButton");
      if (b) {
        var responseNote = new Notifier(b.children[1]);
        var responseDialog  = new ResponseDialog(controller, $("#editControls")[0], b, responseNote);
        session.on("player.pageComplete", function(evt){
          //var noteId = this.changeNotifier.register(this, undefined, "Contents Changed!", undefined);
          responseNote.register(evt.playerName, undefined, "Page Completed");
        });
        
      }
      
      window.edit=function(pageId){
          var isEdit = controller.toggleEdit(pageId);
          $("#editControls").css("visibility", isEdit?"visible":"hidden");
      };
      
      window.addText=function(){
          controller.addContent("text", "Enter Text Here...");
          //controller.refresh(); // redundant...
      };

    if(storyJson.contentType!="Prose") {
        var gallery;
        window.openGallery=function(){
                var onAccept=function(model){
                    if(model) {
                        model.size={ height: 30 };
                        model.type="picture";
                        controller.addContent(model);
                    }
                };
                if(!gallery)  {
                    gallery = new Gallery(undefined, {accept:onAccept});
                }
                gallery.open();
        };
    }
      
      var saveButton=document.getElementById("saveButton");
      if(saveButton) {
            session.subscribe("player.localChange", function(){
                saveButton.textContent="Save*";
            });
            window.save=function(){
                controller.save();
                saveButton.textContent="Save";
            };
            window.publishAll=function(){
                var player = controller._activePlayer;
                var pages = player._pages;
                for(var p in pages) {
                    var model = pages[p]._model;
                    if(model.status!=="shared") {
                        player.editPage(model.id, {status: "shared"});        
                    }
                }
                window.save();
            }
      }
      
      $("#storyTitle .title").click(function(){
          controller.editStory(this);
      });
      
      $("#editControls").css("visibility", controller.isEditMode()?"visible":"hidden");


  });
}

// window.storyJson will be written by the Jade template
load(window.storyJson);