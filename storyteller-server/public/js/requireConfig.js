require.config({
  baseUrl: '/js/st-client',
  // The shim config allows us to configure dependencies for
  // scripts that do not call define() to register a module
  shim: {
    'socketio': {
      exports: 'io'
    }
  },
  paths: {
    socketio: '/js/lib/socket.io/socket.io',
    'moment': '/js/lib/moment-2.7.0/moment',
    text: '/js/lib/require/text'
  }
});