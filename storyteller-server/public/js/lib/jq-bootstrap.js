// a little shim help from https://gist.github.com/guerrerocarlos/3651490
require.config({
  baseUrl: '/js/st-client',
  // The shim config allows us to configure dependencies for
  // scripts that do not call define() to register a module
  shim: {
    'socketio': {
      exports: 'io'
    }
  },
  paths: {
    jquery: "/js/lib/jquery-2.1.0/jquery",
    socketio: '/js/lib/socket.io/socket.io',
    'moment': '/js/lib/moment-2.7.0/moment',
    text: '/js/lib/require/text'
  }
});

//require(["jquery"], function($) {
//    
//   console.log($?"Jquery Loaded!":"Jquery Failed!");
//});

require(["jquery",
        "/js/lib/underscore-1.6.0/underscore.min.js",
        "/js/lib/jquery-ui-1.11.1/jquery-ui.js"
       ], function(jq){
        console.log("JQuery base bootstrapper loaded!");
        
        require(["/js/lib/jquery-tooltipster/js/jquery.tooltipster.min.js",
                 "/js/lib/amplify-1.1.2/amplify.js"], function(){
            
                 if(window.onLoaded) {
                window.onLoaded();
            }
            
            return jq;
        });
});