/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




define(["util/domUtils", "client/story/widgets/Picture"], function(dom, Picture){
    
    var Paginator = function(domNode, model, control){
        this.nodes={
            parent: domNode,
            root: dom.create({class:"paginator", parent: domNode})
        };
        this.model = model;
        this.control=control;
        this.init();
    };

    Paginator.prototype={

        init: function(){
            var nodes = this.nodes,
                root = nodes.root;

            nodes.previous = dom.create({class:"page-button", text:"Previous", parent:root, click:this.previous.bind(this)});
            nodes.next = dom.create({class:"page-button", text:"Next", parent:root, click:this.next.bind(this)});
            this.updateState();
        },

        hasNext: function(){
            var model = this.model;
            return model.total > model.end;
        },

        hasPrevious: function(){
            var model = this.model;
            return model.start > 0;
        },

        // only go next if there IS a next
        next: function(){
            if(this.hasNext()) {
                var model = this.model;
                var start = this.model.start;
                var size = this.model.size;

                model.start+=size;
                model.end+=size;
                this.render(model);
                return model;
            }
        },

         previous: function(){
            if(this.hasPrevious()) {
                var model = this.model;
                var start = this.model.start;
                var size = this.model.size;

                model.start-=size;
                model.end-=size;
                this.render(model);
                return model;
            }
        },

        render: function(newModel){
            var model=newModel||this.model;
            //this.updateState(model);
            this.control.render(model);
        },

        updateState: function(){
            var nodes = this.nodes;
            if(this.hasNext()){
                $(nodes.next).addClass("show");
            } else {
                $(nodes.next).removeClass("show");
            }

            if(this.hasPrevious()){
                $(nodes.previous).addClass("show");
            }else {
                $(nodes.previous).removeClass("show");
            }
        }
    };
    
    
    
    var Browser = function(domNode, source){
        this.nodes={
            parent: domNode,
            root: dom.create({parent: domNode, class: "image-browser"}),
            page: dom.create({parent: domNode, class: "page-control"}),
        };
        this._paginator = new Paginator(this.nodes.page, this.page, this);
        
        this._sourceUrl = source;
        this.init();
    };
    
    Browser.prototype={
        
        page: {
            current: 0, // the current page number
            start: 0,   // the first visible item
            end: 0 ,   // the last visible item
            total: 0,    // the total number of items
            size: 20    // the number of items per page
        },
        
        init: function(){
            this.page.end = this.page.start+this.page.size;
            
          this.loadData();   
        },
        
        loadData: function(){
            console.log("Browser :: loading images");
            $.get(this._sourceUrl, function(data){
                this._data = data.results;
                this.page.total = data.results.length;
                this.render();
            }.bind(this));
        },
        
        render: function(page){
            console.log("-> Rendering:");
            console.log(data);
            // get all the picture data
            var data = this._data;
            if(page) {
                for(var p in page) {
                    this.page[p]=page[p];
                }
            }

            var start = this.page.start,
                end = this.page.end;

            var root = this.nodes.root;
            root.innerHTML = "";
            var self=this;
            for(var i=start;i<end;i++) {
                var d = data[i];
                if(d) {
                    var wrapper = dom.create({class: "thumb-wrapper", parent:root});
                    var clickHandler=(function(node){
                        return function(){
                            console.log("select!");
                            self.select(node);
                        }.bind(self);
                    });
                    wrapper.onclick=clickHandler;
                    
                    // create a new Picture content for each picture
                    var model = wrapper.model = {
                      content: d.key,
                      ratio: d.ratio,
                      size: {
                          biggest: 150
                      }
                    };
                    var p = new Picture(wrapper, model);
                    if(d.name) {
                        dom.create({class:"title", parent: wrapper, text: d.name});
                    }
                    var creatorWrapper =  dom.create({class:"creator-wrapper", parent: wrapper});
                    if(d.creator) {
                        var name = d.creator.name;
                        dom.create({type: "span",class:"text", parent: creatorWrapper, text: "by"});
                        dom.create({type: "span",class:"author", parent: creatorWrapper, text: name.user||name.first});
                        if(d.createdAt) {
                            dom.create({type: "span",class:"text", parent: creatorWrapper, text:" at "});
                            dom.create({type: "span",class:"date", parent: creatorWrapper, text: d.createdAt});
                        }
                    } else {
                        dom.create({type: "span",class:"text", parent: creatorWrapper, html: "&nbsp;"});
                    }
                    p.render();
                }
            }
            if(this._paginator) {
                this._paginator.updateState(this.page);
            }
        },
        
        // return the selected MODEL
        getSelected: function(){
            if(this._selected) {
                return this._selected.model;
            }
        },
        
        select: function(node){
            if(this._selected) {
                this.deselect(this._selected);
            }
            $(node).addClass("selected");
            var model = node.model;
            this._selected={
                model: model,
                node: node
            };
            
            console.log("Selecting: ");
            console.log(model);
        },
        
        deselect: function(node){
            $(node).removeClass("selected");
            this._selected=undefined;
        }

        
    };
    
    return Browser;
    
});

