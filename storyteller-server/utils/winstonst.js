/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var keystone = require("keystone");
var winston = require("winston");

if(keystone.get("st mode")=="web") {
    winston.add(winston.transports.File, { filename: '../log/storyteller.log', timestamp:true, maxSize:100000000 });
    winston.info("Loading Winston with file Transport");
}
winston.transports.Console.colorize=true;

module.exports=winston;



