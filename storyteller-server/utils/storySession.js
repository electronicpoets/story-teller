var keystone = require('keystone'),
    express = keystone.express,
    Sockets = require('socket.io'),
    cookie = require('cookie');

var stories = keystone.list("Story").model,
    pages = keystone.list("Page").model,
    links = keystone.list("Link").model,
    users = keystone.list("User").model,
    middleware = require('../routes/middleware');

var analytics = require('../utils/analytics');
var winston = require('./winstonst.js');

// locals
var connections={};
var _socketId=1;
var getId=function(){
    return _socketId++;
};
var isWeb = keystone.get("st mode")=="web";
var isDebug=!isWeb;
//var isDebug=false;
//var log = GLOBAL.console.log;
//var console = {
//    log: function(){
//        if(isDebug) {
//            GLOBAL.console.log.apply(this, arguments);
//        }
//    }
//};


var notifications={};

var removeLinkListener = function(storyId, playerId, linkId) {
    stories.findById(storyId).exec(function(err, story){
        if(story && story.listeners) {
            var data=story.listeners[playerId];
            if(data) {
                data[linkId] = undefined;
                delete data[linkId];
            }
            if(Object.keys[data]==0) {
                story.listeners[playerId] = undefined;
                delete story.listeners[playerId];
            } else {
                story.listeners[playerId]=data;
            }

            var c = story.listeners;
            story.listeners=undefined;
            story.listeners=c;
            story.save();
        }
    });
};

var registerLinkListener = function(storyId, playerId, linkId) {
    stories.findById(storyId).exec(function(err, story){
        if(story) {
            var listeners = story.listeners;
            var clone = {};
            if(!listeners) {
                listeners=story.listeners={};
            } else {
                for(var p in listeners) {
                    clone[p]=listeners[p];
                }
            }
            var data=clone[playerId];
            if(!data) {
                data=clone[playerId]={};
            }
            data[linkId]=true;
            
            story.listeners=undefined;
            story.listeners=clone;// make sure the change is detected
            story.save(function(err, story){
                if(err) {
                    winston.error(err);
                }
            });
        } else {
            winston.error("*** registerLinkListener - couldn't find story "+storyId, err);
        }
    });
};

var notifyPlayer = function(storyId, playerId){
    if(!SessionManager.isPlayerOnline(storyId, playerId)) {
        winston.info("Sending player e-mail to inform that a page has been published");
        users.findById(playerId).exec(function(err, player) {
            if(player) {
                SessionManager._email("page-created", player.name.first, player.email);
            }
            else {
                winston.warn(" WARNING :: could not find user "+userId)
            }
        
        });
    } 
};

var notifyValidatedLink = function(storyId, playerId, linkId){
    winston.info(" ** Notifying player "+playerId+" that link "+linkId+" is valid") ;
    if(!SessionManager.isPlayerOnline(storyId, playerId)) {
        winston.info("Sending player e-mail to inform that a link has been validated");
        users.findById(playerId).exec(function(err, player) {
            if(player) {
                stories.findById(storyId).exec(function(err, story) {
                    var args={
                        player: player.name.first, 
                        story_name: story.title,
                        story_path: story.id,
                        subject: "Storyteller: Story "+story.title+" has been updated!",
                        host: keystone.get("st mode")=="web"?"http://www.electronicpoets.co.uk":"http://joe-pc:8080"
                    }
                    SessionManager._email("page-created", player.name.first, player.email, args);
                });
            }
            else {
                winston.warn(" WARNING :: could not find user "+userId);
            }
        });
    } 
//    else {
//        var evt = {
//            storyId: storyId,
//            playerId: playerId,
//            linkId: linkId
//        };
//        publishEvent(storyId, "story.link.validated", evt);
//    }
    removeLinkListener(storyId, playerId, linkId);
}

var updatePlayerCount = function(storyId){
    var cnx = connections[storyId];
    if (cnx) {
        var count = Object.keys(cnx).length;
        publishEvent(storyId, "players.count", { count: count });
    }
};

// Manually publish the event through each open socket?
// Or should I be using a broadcast event? I'll play with that
var publishEvent = function(storyId, eventName, data, exclude){
    var cnx = connections[storyId];
    for (var id in cnx) {
        var socket = cnx[id];
        if (!exclude || socket.id!=exclude) {
            socket.emit(eventName, data);
            //socket.broadcast.to("").emit(eventName, data)
        }
    }
}


var SessionManager = {
    
    // issue notification that a link has been valdiated
    // may cause e-mails to be sent to interested players
    validateLink: function(storyId, linkId){
        // lookup listeners for this story
        // find any players interested in this link id
        stories.findById(storyId).exec(function(err, story){
            if(story) {
                var data=story.listeners;
                if(data) {
                    for(var playerId in data) {
                        if(data[playerId][linkId]) {
                            notifyValidatedLink(storyId, playerId, linkId);
                        }
                    }
                }
                // publish a general announcement
                publishEvent(storyId, "story.link.validated", {
                   storyId: storyId,
                   playerId: playerId,
                   linkId: linkId
               });
            } else {
                winston.error("Error :: Couldn't find story "+storyId, err);
            }
        });
    },
    
    onPlayerJoinedStory: function(storyId, playerId){
        // if the storyteller is offline, send an e-mail
        stories.findById(storyId).populate("storyteller").exec(function(err, story){
            var st = story.storyteller;
            for(var i=0;i<st.length;i++) {
                var storyteller = st[i];
                if(!SessionManager.isPlayerOnline(storyId, storyteller._id)) {
                    console.log("Sending e-mail to notify new player has joined a game!");
                    var args={
                        player: storyteller.getName(), 
                        story_name: story.title,
                        story_path: story.id,
                        subject: "Storyteller: New Player has Joined "+story.title+"!",
                        host: keystone.get("st mode")=="web"?"http://www.electronicpoets.co.uk":"http://joe-pc:8080"
                    };
                    console.log(args);
                    SessionManager._email("player-joined", storyteller.getName(), storyteller.email, args);
                }
            }
        });
        
    },
    
    isPlayerOnline: function(storyId, playerId){
        var cnx = connections[storyId];
        if(cnx) {
            for(var socketId in cnx) {
                var user = cnx[socketId].user;
                if(user && user._id.toString() == playerId) {
                    return true;
                }
            }
        }
    },
    
    _email: function(template, to, address, data){
        data=data||{};
        data.subscriber=to;
        data.to=address;
        data.from= {
            name: 'Electronic Poets',
            email: 'scribe@electronicpoets.co.uk'
        };
        // also send vars
        new keystone.Email(template).send(data,function(err,data){
            winston.info("E-Mail sent to: "+address, err);
        });
    },
    
    start: function(){
        //var SessionSockets = require('session.socket.io');
        var io = Sockets.listen(keystone.httpServer);
    
        // io.set('authorization', function (handshake, accept) {
        //   var cookies = cookie.parse(handshake.headers.cookie);
        
        //    var sessionId = cookies['keystone.sid'];
        //    //console.log("Handshaking with "+sessionId);
            
        //     // if (connections[sessionId]) {
        //     //     accept('Error: Already signed in', false);
        //     // } else {
        //     //     accept(null, true);
        //     // }
        //     accept(null, true);
        // });
        
        var self=SessionManager;
        io.on('connection', function (socket) {
            var id;
            if (socket.handshake) {
                var cookies = cookie.parse(socket.handshake.headers.cookie);
                var sessionId = cookies['keystone.sid'];
                // the userId cookie isn't safe. Work some voodoo on keystone.ui instead
                var userId = cookies['keystone.uid'];
                if(userId) {
                    userId = userId.split(":")[1];
                }
                //var userId = cookies['userId'];
                id = socket._id = sessionId;

                // associate a user with the session request?
                users.findById(userId).exec(function(err, user) {
                    if(user) {
                        //console.log(" Found user "+user.name.first);
                        socket.user = user;
                    }
                    else {
                        //console.log(" WARNING :: could not find user "+userId)
                    }
                });

           } else {
                winston.warn("ALERT :: no handshake detected on socket!");
                id = getId();
           }
            self.listen(socket);
        });
  },

   listen: function(socket){
    var self=this;
    socket.on("register", function(evt){
       if (!this._registered) {
            this._registered=true;
            this.storyId = evt.storyId;
    
            if (!connections[evt.storyId]) {
                connections[evt.storyId] = {};
            }
            connections[evt.storyId][this._id] = this;
    
            self.connectSession(socket);
        } 
        // remove the connect handler?
        var user = this.user;
        if(user) {
            var playerData = {
                name: user.name.first,
                id: user._id.toString()
                // TODO - if the player is a storyteller, include notification
             };
         }

        socket.emit("connected", { player: playerData });
        publishEvent(evt.storyId, "player.connected", playerData);
        //self._email();
        //var publishEvent = function(storyId, eventName, data, exclude){
       // }
    });
  },
  
  // listen: function(socket){
  //   var self=this;


  //   socket.on("register", function(evt){
  //      if (!this._registered) {
  //           this._registered=true;
  //           console.log("Socket ("+this._id+") connecting to "+evt.storyId+"... ")
  //           this.storyId = evt.storyId;
    
  //           if (!connections[evt.storyId]) {
  //               connections[evt.storyId] = {};
  //           }
  //           connections[evt.storyId][this._id] = this;
    
  //           self.connectSession(socket);
  //           // remove the connect handler?
  //           socket.emit("connected");
  //       }
  //   });
  // },
  
  connectSession: function(socket){
   console.log("o> session ("+socket._id+") connecting.");
    updatePlayerCount(socket.storyId);
    
    var storyNotes=notifications[socket.storyId];
    if(!storyNotes) {
        storyNotes = notifications[socket.storyId] = {};
    }
    // reset all notifications for this player
    var userId = socket.user._id.toString();
    storyNotes[userId] = {};

//    socket.on("player.state", function(evt){
//        
//    });

    socket.on("player.page", function(evt, callback){
        // update the player's current page
        //console.log("> Updating player "+evt.playerName+" current page to "+evt.pageId);

        // first, load the appropriate story instance (can be optimised)
        stories.findById(this.storyId).exec(function(err, story){
            // second, update the story's state
            story.setCurrentPage(evt.playerId, evt.pageId); 
        });
        publishEvent(this.storyId, "player.page", { playerName: evt.playerName, playerId: evt.playerId, pageId:evt.pageId}, this._id);
    });
    
    socket.on("player.pageComplete", function(evt, callback){
        // update the player's state
        var storyId = this.storyId,
            linkId = evt.linkId,
            pageId = evt.pageId,
            playerId = evt.playerId;
            
        if(isWeb) {
            analytics.event(socket._id, "page", "completed", pageId);
        }
        socket.user.tickStats("pageCompleted");

        //console.log("> Updating player "+evt.playerName+" next page to "+evt.linkId);
        pages.findById(pageId).exec(function(err, page){
           if(!page) {
               winston.error(" *** Couldn't find page "+pageId, "player.pageComplete", err);
           } else {
                page.saveResponse(playerId, linkId, evt.message, evt.timestamp);
           }
        });
        var self=this;
       
       stories.findById(storyId).exec(function(err, story){
            // second, update the story's state
            story.setState(playerId, pageId, "next", linkId); 
            // the next page's previous page is the current page. Tricky.
            if(evt.nextPageId) {
                story.setState(playerId, evt.nextPageId, "previous", pageId);
            }
            
            publishEvent(storyId, "player.pageComplete", evt, this._id);
            var st = story.storyteller[0];
            if(!SessionManager.isPlayerOnline(storyId, st)) {
                users.findById(st).exec(function(err, st) {
                    var vars = {
                        storyteller: st.name.first,
                        player_name:evt.playerName||evt.playerId,
                        story_name: story.title,
                        story_path: story.id,
                        subject: "Storyteller: New Player Response in "+story.title,
                        host: keystone.get("st mode")=="web"?"http://www.electronicpoets.co.uk":"http://joe-pc:8080"
                    };
                    SessionManager._email("player-response", st.name.first, st.email, vars);
                });
            }
            story.onUpdated();
        });


    });
    
    // subscribe a player to page publishing notifications
    // if the player is offline, they'll be sent an email when the page is published
    socket.on("player.registerLinkListener", function(evt, callback){
        //console.log("Player subscribing to page-publication event:");
        //console.log(evt);
        registerLinkListener(evt.storyId, evt.playerId, evt.linkId);
    });
    socket.on("player.unregisterLinkListener", function(evt, callback){
        //console.log("Player unsubscribing to page-publication event:");
       // console.log(evt);
    });
    
    
    socket.on("disconnect", function(data){
        console.log("o> session ("+this._id+") disconnected.");
        //this._registered=false;
        var cnx = connections[this.storyId];
        
        if (cnx[this._id]) {
            cnx[this._id] = undefined;
            delete cnx[this._id];
        }
        
        var user = this.user;
        if(user) {
            var playerData = {
                name: user.name.first,
                id: user._id.toString()
             };
             
            publishEvent(this.storyId, "player.disconnected", playerData);
         }
        updatePlayerCount(this.storyId);
    });
    
    socket.on("story.changed", function(evt, callback){
        var playerId = evt.playerId;
        var deltas = evt.deltas;
        var socketId = this._id;
        var storyId = this.storyId;
        winston.info("socket for "+this.storyId+" detected change event!");
        if (evt.story) {
            // changes have been recorded to the story itself
            winston.info("Updating story details:");
            winston.info(evt.story);
            stories.findById(storyId).exec(function(err, story) {
                if (story) {
                    for (var change in evt.story) {
                        story[change] = evt.story[change];
                    }
                    story.onUpdated(function(updated){
                        publishEvent(storyId, "story.changed", evt, socketId);
                        if(callback) {
                            callback({id: updated.id});
                        }
                    });
                }
            });
        }
        else if (deltas) {
           // stories.findOne({ id: storyId}).populate("pages").exec(function(err, story){
                middleware.updateStory(storyId, deltas, function(newIds){
                    for (var id in deltas) {
                        var newId = newIds[id];
                        var item = deltas[id];
                        if (newId) {
                            deltas[newId] = item;
                            deltas[id] = undefined;
                            delete deltas[id];
                        }
                        // also check links & content
                        var nested = (item.contents||[]).concat(item.next||[]);
                        for (var i=0;i<nested.length;i++) {
                            var n = nested[i];
                            if (newIds[n.id]) {
                                //console.log(" -> Updating nested id from "+n.id+" to "+newId);
                                n.id = newIds[n.id];
                            }
                        }
                    }
                    if(callback) {
                        callback(newIds);
                    }
                    evt.deltas = deltas;
                    publishEvent(storyId, "story.changed", evt, socketId);
                });
                
            //});
        }
    });

    // evt: - content
    //      - timestamp (local)
    //      - author
     socket.on("chat.message", function(evt){
         var storyId = this.storyId;
         var socketId = this._id;
         publishEvent(storyId, "chat.message", evt, socketId);
         
         var storyNotes=notifications[socket.storyId];
         stories.findById(storyId).populate("storyteller players").exec(function(err, story){
            story.addChatMessage(evt);
           
            var users = story.storyteller.concat(story.players||[]);
                for(var i=0;i<users.length;i++) {
                    var user = users[i];
                    var userId = user._id.toString();
                    if(!storyNotes[userId]) {
                        storyNotes[userId]= {};
                    }
                    if(!SessionManager.isPlayerOnline(storyId, user._id) && !storyNotes[userId].chat) { 
                       storyNotes[userId].chat=true;
                       var vars = {
                           player_name:user.getName(),
                           story_name: story.title,
                           story_path: story.id,
                           subject: "Storyteller: New Chat Messages in "+story.title,
                           host: keystone.get("st mode")=="web"?"http://www.electronicpoets.co.uk":"http://joe-pc:8080"
                       };
                       SessionManager._email("new-chat", vars.player_name, user.email, vars);
                   }  
            }
        });
        
        
     });
    
  }
};

exports = module.exports = SessionManager;