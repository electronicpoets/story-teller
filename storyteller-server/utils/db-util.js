/*
 * util classes for reading/writing Storyteller objects
 * to the database.
 * 
 * No idea how to correctly expose and share this functionality...
 */
var keystone = require('keystone'),
        Story = keystone.list('Story'),
        Page = keystone.list('Page'),
        Content = keystone.list('Content'),
        Link = keystone.list('Link'),
        Images = keystone.list('Image');

var isDebug=keystone.get("st mode")!="web";
//var isDebug=false;
//var log = GLOBAL.console.log;
//var console = {
//    log: function(){
//        if(isDebug) {
//            GLOBAL.console.log.apply(this, arguments);
//        }
//    }
//};
var winston = require("../utils/winstonst");

exports = module.exports = {
    createPage: function(story, spec, idTracker, onComplete) {
        
        var page = new Page.model({
            title: spec.title
        });
        
        // NB: this will auto-save the page and story
        page.setStory(story, function() {
            var newModel;
            if (spec) {
                for (var prop in spec) {
                    var value = spec[prop];
                    if (prop == "contents") {
                        for (var i = 0; i < value.length; i++) {
                            newModel = exports.createContent(page, value[i]);
                            if (newModel && idTracker) {
                                idTracker[value[i].id] = newModel._id;
                            }
                        }
                    } else if (prop == "next") {
                        for (var i = 0; i < value.length; i++) {
                            var link = value[i];
                            if (idTracker) {
                                for (var p in link) {
                                    var v = link[p];
                                    if (idTracker[v]) {
                                        link[p] = idTracker[v];
                                    }
                                }
                            }
                           // console.log("- Creating new link ("+link.id+") for story "+story._id);
                            link.story=story;
                            newModel = exports.createLink(page, link.isNext, link);
                            if (newModel && idTracker) {
                                idTracker[link.id] = newModel._id;
                            }
                        }
                    }
                    else {
                        page[prop] = value;
                    }
                }
            }
            page.save(function(err, page){
                if(err) {
                    console.log(err);
                }
                if(onComplete) {
                    onComplete(page);
                }
            });
        });
    },
    createContent: function(page, spec, callback) {
        spec = spec || {};
        spec.page = page._id; // id, not _id ?

        var c = new Content.model(spec);
        // if the content is an image, increase its inserts stats after saving
        if(c.type=="picture") {
            Images.model.findOne({key:c.content}).exec(function(err, image){
                if(image) {
                    image.onInsert();
                }
            });
        }
        
        // JC this needs a serious look at: don't like chaining saves like this
        c.save(function(err, c){
            if(!err) {
                page.contents.push(c._id);
                var pid = page.id;
                page.save(function(err){
                    if(err) {
                        winston.error("Error saving page: "+pid, err);
                    } 
                    if(callback) {
                        callback(c);
                    }
                });
            } else {
                winston.err("Error saving content: ", err);
            }
        });
        
        // Note: Have to manually add the content to the page
        //page.contents.push(c._id);
        return c;
    },
    createLink: function(page, isNext, spec, callback) {
        var target;
        // JC 14Sept14 make sure we manually set the target, so that the link can self-validate
        if(spec.target) {
            target=spec.target;
            spec.target=undefined;
        }
        var link = new Link.model(spec);
        if (isNext) {
            page.next.push(link);
        } else {
            page.previous.push(link);
        }
        
        var handler=function(err, l){
            if(callback && l) {
                callback(l);
            }
        };
        if(target) {
            link.set_target(target, undefined, function(err, l){
                // nasty hacks - make sure the client sees the change
                if(link.isValid) {   
                    spec.isValid=true;
                }
                spec.target=l.target;
                handler(err, l);
            });
        } else {
            link.save(handler);
        }
        return link;

    },
    // Creates a new story instance
    // Note: Does NOT save the story...
    createStory: function(storyName, onComplete) {
        var story;
        if (storyName == "trolls") {
            story = this._createTrolls(onComplete);
        } else if (storyName == "rabbit") {
            story = this._createRabbit(onComplete);
        } else if(storyName == "Prose Test") {
            story = this._createTest(onComplete);
        }
    },
    _createText: function(onComplete) {
        var story = new Story.model({
            title: "Dungeon",
            description: "You are a in a dungeon",
            objective: "Get out!",
            maxPlayers: 2,
            type: "LiveStory",
            contentType:"Prose",
            pages: [],
            //isPublished: true,
            createdAt: new Date()
        });
        
         this.createPage(story, {status: "shared"}, function(){
             story.save(onComplete);
         });
    },
    _createTrolls: function(onComplete) {
        var trolls = new Story.model({
            title: "Trolls!",
            description: "A silly story about Trolls and Risotto",
            objective: "Find something to eat.",
            maxPlayers: 2,
            type: "LiveStory",
            pages: [],
            //isPublished: true,
            createdAt: new Date()
        });
        //trolls.save();

        var page = this.createPage(trolls, {background: "dark-forest", fadeBg: true, status: "shared"}, undefined, function(page){
            this.createLink(page, true, {story: trolls, title: "Check Inventory"});
            this.createLink(page, true, {story: trolls, title: "Attack"});
            this.createLink(page, true, {story: trolls, title: "Sneak Around"});
            this.createLink(page, true, {story: trolls, title: "Freestyle", freestyle: true});

            this.createContent(page, {
                type: "text",
                content: "You enter the clearing to see a troll standing before you.",
                position: {
                    horizontal: 50,
                    vertical: 20
                }
            });

            this.createContent(page, {
                type: "picture",
                content: "troll",
                position: {
                    horizontal: 50,
                    vertical: 60
                },
                size: {
                    height: 40
                }
            });
            page.save(function(){
              onComplete(trolls);  
            });
        }.bind(this));
    },
    
    _createRabbit: function(onComplete) {
        var rabbit = new Story.model({
            title: "Sad Rabbit",
            description: "A rabbit is on the beach. Why does it look so sad?",
            objective: "Cheer up the rabbit",
            type: "LiveStory",
            maxPlayers: 2,
            pages: [],
//            isPublished: true,
            createdAt: new Date()
        });

        this.createPage(rabbit, {background: "pebble-beach", status: "shared"}, undefined, function(page){
            this.createLink(page, true, {story: rabbit, title: "Look at the Rabbit"});
            this.createLink(page, true, {story: rabbit, title: "Approach the Rabbit"});
            this.createLink(page, true, {story: rabbit, title: "Shoot the Rabbit"});
            this.createLink(page, true, {story: rabbit, title: "Speak to the Rabbit", freestyle: true, prompt: "What would you like to say?"});

            this.createContent(page, {
                type: "text",
                content: "You see a rabbit on the beach before you. \nIt looks sad.",
                position: {
                    horizontal: "center",
                    vertical: "middle"
                }
            });

            this.createContent(page, {
                type: "picture",
                content: "sad-rabbit",
                position: {
                    horizontal: 30,
                    vertical: 75
                },
                size: {
                    height: 30
                }
            });
            
            page.save(function() {
                onComplete(rabbit);
            });
        }.bind(this));
    }

}