/* 
 * Wrapper library for analytics
 * With a little luck, this wil a) let me abstract implementations reasonably well,
 * and b) let me disable analytics in dev mode
 */

var keystone = require('keystone'),
    ua = require('universal-analytics');


var isWeb = keystone.get("st mode")=="web";
var key = keystone.get("ga key");

exports = module.exports = {
    
    visitors: {},
    
    // Initialise Google analytics
    init: function(){
        
    },
    
    // get the instance which tracks this user. Bind to a keystone
    // session id.
    getVisitor: function(sid){
        var visitor = this.visitors[sid];
        if(!visitor){
            visitor = this.visitors[sid] = ua(key);    
        }
        return visitor;
    },
    
    page: function(sid, path){
        if(isWeb) {
            var visitor = this.getVisitor(sid);
            visitor.pageview(path).send();
        }
        return path;
    },
    
    // analytics api
    event: function(sid, category, action, label){
        if(isWeb) {
            var visitor = this.getVisitor(sid);            
            visitor.event(category, action, label).send();
        }
        // else do nothing
    }
    
}


