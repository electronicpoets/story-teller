// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv')().load();

// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.
var CommandLine=require("node-commandline").CommandLine;

// Use the CommandLine package to introduce a smart command line parser
// The commands we support are:
//		-port: specify which port to run on
//		-mode [dev, demo, web]: configures permissions and behaviours depending on how the server is used
// This is way harder than it should be, but there we go...
var commandline=new CommandLine();

// convert the arguments into an array, excluding the node boiler-plate args
var args = [];
for(var i=2;i<process.argv.length;i++) {
	args.push(process.argv[i]);
}

// Define the arguments we expect
commandline.addArgument('port',{type: 'number'});
commandline.addArgument('model',{type: 'string'});

// and parse the arugments input to produce a model of the command line values
var cmds = commandline.parse.apply(commandline, args);

var port = cmds.port||'8080';

var mode = "dev";
var m = cmds.mode;
if(m && (m=="demo" || m=="web")) {
	mode = m;
}
keystone.set("st mode", mode);

console.info("\nStoryteller :: running in "+mode+" mode");

keystone.init({
	
	'name': 'Storyteller',
	'brand': 'Storyteller',
	'port': port,
	'mongo': 'mongodb://localhost:2156/storyteller',
        'sessionstore':'connect-mongo',
	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	
	'views': 'templates/views',
	'view engine': 'jade',
	
	'emails': 'templates/emails',
	
	'auto update': true,
	
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': 'kJRXPi">Wl61=6*]$_t"v1gtFvV}#sM<?+kcRNtddU9ZGlz7*]$_t"v1gtFvV}#sM<?+kcRNtddU9ZGlz7=lg?o/a>)1_W4R~*]$_t"v1gtFvV}#sM<?+kcRNtddU9ZGlz7',
	
        'mandrill api key': 'jjsyXVwONI17MI1iYhdbLQ',
        
	'signout redirect': '/',
	'signin redirect': '/'
});

// database set-up
//if(mode=="web") {
//    keystone.set("mongo", "mongodb://localhost/st-web");
//}

// Load your project's Models

keystone.import('models');

keystone.set("mailchimp key", '018c7f5337a7b26b88534fb7115bb61b-us8');

// Jc configure google analytics
keystone.set("ga key", "UA-53255510-1");

// JC configure EP Cloudinary account
keystone.set('cloudinary config', {
	cloud_name: "electronic-poets",
	api_key: "568771423934765",
	api_secret: "8xv-ndGHuJn7h8Oj90IwNPkuxL0"
});

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.

keystone.set('email locals', {
	logo_src: '/images/logo-email.gif',
	logo_width: 194,
	logo_height: 76,
	theme: {
		email_bg: '#f9f9f9',
		link_color: '#2697de',
		buttons: {
			color: '#fff',
			background_color: '#2697de',
			border_color: '#1a7cb7'
		}
	}
});

// load the version number
var pjson = require('./package.json');
keystone.set("st version", pjson.version);

// Setup replacement rules for emails, to automate the handling of differences
// between development a production.

// Be sure to update this rule to include your site's actual domain, and add
// other rules your email templates require.

keystone.set('email rules', [{
	find: '/images/',
	replace: (keystone.get('env') == 'production') ? 'http://www.your-server.com/images/' : 'http://localhost:3000/images/'
}, {
	find: '/keystone/',
	replace: (keystone.get('env') == 'production') ? 'http://www.your-server.com/keystone/' : 'http://localhost:3000/keystone/'
}]);

// Load your project's email test routes

keystone.set('email tests', require('./routes/emails'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'posts': ['posts', 'post-categories'],
	//'galleries': 'galleries',
	//'enquiries': 'enquiries',
	'users': 'users'
});


// JC If we're running in any kind of production environment, uglify all JS
//if (mode!="dev") {
//	var expressUglify = require('express-uglify');
//	keystone.app.use(expressUglify.middleware({ 
//	  src: 'public',
//	  logLevel: 'error'
//	}));
//}

// Start Keystone to connect to your database and initialise the web server
var session = require("./utils/storySession");
keystone.start({
	onHttpServerCreated: session.start
});

