
exports.isAttached = function(element) {
    var ancestor = element;
    while(ancestor.parentNode) {
        ancestor = ancestor.parentNode;
    }
    return !!ancestor.body;
};

exports.clear = function(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
};

exports.setText = function(element, text) {
    exports.clear(element);
    element.appendChild(document.createTextNode(text));
};

// Jc wrap handlers so we can remove them later
exports.handleEvent = function(element, name, handler) {
    var wrappedHandler = function(ev) {
        if (handler(ev) === false) {
            ev.preventDefault();
        }
    };
    element.addEventListener(name, wrappedHandler);
    return wrappedHandler;
};

exports.handleMouseEvent = function(element, name, handler) {
    return exports.handleEvent(element, name, function(ev) {
        var rect = element.getBoundingClientRect();
        return handler(ev, ev.clientX - rect.left, ev.clientY - rect.top);
    });
};

// JC remove event handles
exports.removeEvent = exports.removeMouseEvent = function(element, name, handler) {
    element.removeEventListener(name, handler);
};

exports.effectiveStyle = function(element, name) {
    return document.defaultView.getComputedStyle(element).getPropertyValue(name);
};