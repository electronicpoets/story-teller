var webmake = require('webmake');
var minify = require('node-minify');

webmake('src/carota-ep.js', { output: 'carota-ep.js' }, function(result) {
    if (!result) {
        console.log('All good');
    } else {
        console.log(result);
    }
});

new minify.minify({
    type: 'uglifyjs',
    fileIn: 'carota-ep.js',
    fileOut: 'carota-ep-min.js',
    callback: function(err, min){
        if (err) {
            console.log(err);
        }
    }
});
